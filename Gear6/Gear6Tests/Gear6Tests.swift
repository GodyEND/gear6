//
//  Gear6Tests.swift
//  Gear6Tests
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import XCTest
@testable import Gear6
import CoreData

class Gear6Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func getEmptyCategoryTest() {
        let category = Gear6GearElements.getCategory(id: -1)
        XCTAssertNil(category)
    }
    
    func addCategoryTest(name: String) -> Bool {
        Gear6MapperViewController.shared.viewState = .Category
        let prevCatCount = Gear6GearElements.categories.count
        Gear6MapperViewController.shared.addCategoryAction(name: name)
        let newCatCount = Gear6GearElements.categories.count
        var testableID: Int? = -1
        if (Gear6GearElements.reusableCategoryIDs.count == 0) {
            testableID = Gear6GearElements.categoryMaxID
        }
        let category = Gear6GearElements.getCategory(id: testableID!)
        XCTAssert(category?.name == name)
        return (newCatCount - prevCatCount == 1)
    }
    
    func addInvalidCategoryTest(name: String) {
        Gear6MapperViewController.shared.viewState = .Category
        let prevCatCount = Gear6GearElements.categories.count
        Gear6MapperViewController.shared.addCategoryAction(name: name)
        let newCatCount = Gear6GearElements.categories.count
        XCTAssert((newCatCount - prevCatCount) == 0)
       // let category = CoreDataManager<Category>.fetchDataWithName(name)
        //XCTAssert(category.count == 0)
    }
    
    func testForDuplicateCategory(name: String) -> Bool {
        Gear6MapperViewController.shared.viewState = .Category
        let prevCatCount = Gear6GearElements.categories.count
        Gear6MapperViewController.shared.addCategoryAction(name: name)
        var newCatCount = Gear6GearElements.categories.count
        XCTAssert(newCatCount - prevCatCount == 1)
       // let singleArray = CoreDataManager<Category>.fetchDataWithID(Gear6GearElements.categoryMaxID)
        //XCTAssert(singleArray.count == 1)
        Gear6MapperViewController.shared.addCategoryAction(name: name)
        newCatCount = Gear6GearElements.categories.count
        return (newCatCount - prevCatCount == 1)
        //let douplicateArray = CoreDataManager.fetchDataWithID(Gear6GearElements.categoryMaxID)
        //XCTAssert(douplicateArray.count == 1) */
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        self.getEmptyCategoryTest()
        // Test Search By ID
        XCTAssert(self.addCategoryTest(name: "first"))
        // Test for Duplicate
        XCTAssert(self.testForDuplicateCategory(name: "second"))
        // Test for Invalid Name
        self.addInvalidCategoryTest(name: "Value")
        self.addInvalidCategoryTest(name: "Text")
        self.addInvalidCategoryTest(name: "Image")
        self.addInvalidCategoryTest(name: "Category")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
