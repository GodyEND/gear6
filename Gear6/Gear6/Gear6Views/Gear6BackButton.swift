//
//  Gear6BackButton.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6BackButton: Gear6PropertyView {
    
    override func setupView() {
        let x = self.frame.origin.x
        let y = self.frame.origin.y
        let width = self.frame.size.width
        let height = self.frame.size.height
        
        let buttonImage = UIImageView(frame: CGRect(x: x+0.3*width, y: y+0.2*height, width: 0.6*width, height: 0.6*height))
        buttonImage.image = #imageLiteral(resourceName: "Gear6BackButton").withRenderingMode(.alwaysTemplate)
        buttonImage.tintColor = Gear6Defaults.primaryColor
        self.layer.addSublayer(buttonImage.layer)
        
        let buttonCoverImage = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
        buttonCoverImage.image = #imageLiteral(resourceName: "Gear6BackButtonCover").withRenderingMode(.alwaysTemplate)
        buttonCoverImage.tintColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.8)
        self.layer.addSublayer(buttonCoverImage.layer)
        
        let touchView = UIControl(frame: self.frame)
        touchView.addTarget(self, action: #selector(self.buttonPress), for: .touchUpInside)
        self.addSubview(touchView)
    }
    
    override func showAnimation(_ delay: TimeInterval = 0.0, completion:((Bool)->Void)?) {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0, delay: delay, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        })
    }
    
    override func clearAnimation(_ delay: TimeInterval = 0.0, completion: @escaping (Bool)->Void) {
        UIView.animate(withDuration: 1.0, delay: delay, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        })
    }
    
    var delegate: Gear6BackButtonDelegate!
}

@objc extension Gear6BackButton {
    func buttonPress() {
        self.delegate.backAction()
    }
}


// MARK: Delegtion Methods

protocol Gear6BackButtonDelegate {
    func backAction()
}
