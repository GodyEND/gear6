//
//  Gear6MainMenuButton.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6MainMenuButton: Gear6PropertyView {
    
    var title: String!
    var borderColor: UIColor!
    var buttonImage: UIImage!
    static var width:CGFloat = 104.0
    static var height:CGFloat = 104.0 / 0.875
    var pageID: Int!
    private var maskingView: UIView!
    
    init(x: CGFloat, y: CGFloat, title: String, image: UIImage, borderColor: UIColor) {
        super.init(frame: CGRect(x: x, y: y, width: Gear6MainMenuButton.width, height: Gear6MainMenuButton.height))
        self.title = title
        self.buttonImage = image
        self.borderColor = borderColor
        self.position = CGVector(dx: x, dy: y)
    }
    
    init(_ dictionary: Dictionary<String, Any>, position: CGVector) {
        super.init(frame: CGRect(x: position.dx, y: position.dy, width: Gear6MainMenuButton.width, height: Gear6MainMenuButton.height))
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: Gear6NavigationController.shared, action: #selector(Gear6NavigationController.mainButtonAction(sender:)))
        self.addGestureRecognizer(tapGestureRecognizer)
        
        
        self.isUserInteractionEnabled = true
        self.pageID = (dictionary["pageID"] as? Int)!
        self.title = (dictionary["title"] as? String)!
        let colorArray = (dictionary["borderColor"] as? [Int])!
        self.borderColor = UIColor(red: ColorFloat(colorArray[0]), green: ColorFloat(colorArray[1]), blue: ColorFloat(colorArray[2]), alpha: ColorFloat(colorArray[3]))
        self.position = position
        
        NetworkManager.getImage(url: (dictionary["buttonImage"] as? String)!, completion: { [unowned self] image in
            self.buttonImage = image
            DispatchQueue.main.async {
                self.finalizeView()
            }
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func finalizeView() {
        UIGraphicsBeginImageContext(CGSize(width:Gear6MainMenuButton.width, height: Gear6MainMenuButton.height))
        // Create Shape
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.frame.size.width/2, y: 0))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height/4))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height*3/4))
        path.addLine(to: CGPoint(x: self.frame.size.width/2, y: self.frame.size.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.size.height*3/4))
        path.addLine(to: CGPoint(x: 0, y: self.frame.size.height/4))
        path.close()
        // Draw Shape
        path.stroke()
        
        UIGraphicsEndImageContext()
        // Create ImageView Shape & BGPattern
        self.maskLayer = CAShapeLayer()
        self.maskLayer!.frame = self.bounds
        self.maskLayer!.path = path.cgPath
        self.layer.mask = self.maskLayer
        
        let bgPattern = CAShapeLayer()
        bgPattern.frame = self.bounds
        bgPattern.lineWidth = 3.0
        bgPattern.strokeColor = self.borderColor.cgColor
        bgPattern.fillColor = UIColor(patternImage: #imageLiteral(resourceName: "Gear6BasePatternBlack")).cgColor
        bgPattern.path = path.cgPath
        bgPattern.mask = self.maskLayer
        bgPattern.masksToBounds = true
        self.layer.addSublayer(bgPattern)
        
        // Create Image Layer
        let imageShapeLayer = CAShapeLayer()
        imageShapeLayer.frame = self.bounds
        imageShapeLayer.lineWidth = 3.0
        imageShapeLayer.strokeColor = self.borderColor.cgColor
        imageShapeLayer.path = path.cgPath
        imageShapeLayer.mask = self.maskLayer
        imageShapeLayer.masksToBounds = true
        imageShapeLayer.fillColor = UIColor.clear.cgColor
        if let image = self.buttonImage {
            imageShapeLayer.contents = image.cgImage
        }
        self.layer.addSublayer(imageShapeLayer)

        self.isReflective = true
        
        // Create Title Layer with MainButton Font
        let label = UILabel(frame: CGRect(x: 3, y: self.frame.size.height*2/5, width: self.frame.size.width-6, height: self.frame.size.height/2))
        label.text = self.title
        label.font = Gear6Defaults.primaryTitleFont
        label.textAlignment = .center
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.textColor = UIColor.white
        label.strokedText()
        self.addSubview(label)
    }
    
    override func showAnimation(_ delay: TimeInterval = 0.0, completion:((Bool)->Void)?) {
        UIView.animate(withDuration: 1.0, delay: delay, animations: {
            self.frame.origin = CGPoint(x: self.position.dx, y: self.position.dy)
        }, completion: { _ in
            completion!(true)
        })
    }
    override func clearAnimation(_ delay: TimeInterval = 0.0, completion:@escaping (Bool)->Void) {
        super.clearAnimation(completion: { _ in })
        UIView.animate(withDuration: 1.0, delay: delay, animations: {
            self.frame = CGRect(x: self.position.dx, y: self.position.dy + Screen.Bottom, width: Gear6MainMenuButton.width, height: Gear6MainMenuButton.height)
        }, completion: { _ in
            completion(true)
        })
    }
    
    override func standbyAnimation(_ delay: TimeInterval = 0.0) {
        super.standbyAnimation(delay)
    }
}
