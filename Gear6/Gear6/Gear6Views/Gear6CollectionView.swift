//
//  Gear6CollectionView.swift
//  Gear6
//
//  Created by Gody on 21/12/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6CollectionView: Gear6PropertyView, Gear6CollectionViewDelegate {
    
    var delegate: Gear6CollectionViewDelegate? = nil
    var dataSource: Gear6CollectionViewDataSource? = nil
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupView() {
        // Setup Collection View Layout
        // Set default cell to gear6Cells
        // Set default scroll indicator
        
        // Have to mimic all collectionview functionality myself.
        
        let scrollView = UIScrollView(frame: self.frame)
        scrollView.delegate = self.delegate as! UIScrollViewDelegate?;
        // todo: hide scroll view indicator
        // todo: calculate scroll content hegiht that fits with current frame width
        // todo: only scrollable vertically
        // scroll view frame is reduced by indicator width
        // custom indicator is always visible
        // custom indicator has dropshadow
        // custom indicator marks half way point if object number exeeds 2 * the height that fits on fully visible content.
        // more divisions pointer indicators are added as the factor of objects that fit into the collectionview increases
        // maximum indicator limit = 6 including edges therefor every marker indicates a 5th of the data
        
        self.addSubview(scrollView)
        self.updateCollection()
    }
    
    // if divident is updated/changed recall collection setup function to update content cell sizes
    //
    
    func updateCollection() {
        
        // Setup collection view dimensions from init
        // set divident for when collection needs to produce a new row of elements
        // set indexPath
        // set selector movement
        
        // set cell size dependant of width and divided
    }
    
    override func showAnimation(_ delay: TimeInterval = 0.0, completion:((Bool)->Void)?) {
        // For each row slide into from left
        completion!(true)
    }
    
    override func clearAnimation(_ delay: TimeInterval = 0.0, completion: @escaping (Bool)->Void) {
        // For each cell slide out to let
    }
}


// CollectionView Protocols
//extension Gear6CollectionView {
//    func cellForRow(indexPath:IndexPath) -> Gear6CollectionCell {
//        // is moved to implementor
//        return
//    }
//}
