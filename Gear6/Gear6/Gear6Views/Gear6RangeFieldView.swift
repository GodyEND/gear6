//
//  Gear6RangeFieldView.swift
//  Gear6
//
//  Created by Gody on 12/02/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import UIKit

class Gear6RangeFieldView : UIView {
    
    let minRangeField = UITextField()
    let maxRangeField = UITextField()
    var minValue: Int?
    var maxValue: Int?
    var delegate: UITextFieldDelegate? {
        didSet {
            self.minRangeField.delegate = self.delegate
            self.maxRangeField.delegate = self.delegate
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect(x: 75, y: Screen.TAB_VIEW_TOP-(Screen.TEXTFIELD_HEIGHT+10.0), width: Screen.TEXTFIELD_WIDTH, height: Screen.TEXTFIELD_HEIGHT))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    func setupView() {
        self.minRangeField.backgroundColor = UIColor.white
        self.minRangeField.keyboardType = .numberPad
        self.minRangeField.placeholder = Placeholder.MIN
        self.addSubview(self.minRangeField)
        
        self.maxRangeField.backgroundColor = UIColor.white
        self.maxRangeField.keyboardType = .numberPad
        self.maxRangeField.placeholder = Placeholder.MAX
        self.addSubview(self.maxRangeField)
        
        self.setupAutoLayoutConstraints()
        self.updateConstraints()
    }
    
    func setupAutoLayoutConstraints() {
        self.minRangeField.translatesAutoresizingMaskIntoConstraints = false
        self.maxRangeField.translatesAutoresizingMaskIntoConstraints = false
        
        self.leadingAnchor.constraint(equalTo: self.minRangeField.leadingAnchor, constant: -5.0).isActive = true
        self.trailingAnchor.constraint(equalTo: self.maxRangeField.trailingAnchor, constant: 5.0).isActive = true
        self.topAnchor.constraint(equalTo: self.minRangeField.topAnchor, constant: 5.0).isActive = true
        self.topAnchor.constraint(equalTo: self.maxRangeField.topAnchor, constant: 5.0).isActive = true
        self.bottomAnchor.constraint(equalTo: self.minRangeField.bottomAnchor, constant: 5.0).isActive = true
        self.bottomAnchor.constraint(equalTo: self.maxRangeField.bottomAnchor, constant: 5.0).isActive = true
        self.minRangeField.widthAnchor.constraint(equalToConstant: (self.frame.size.width-50.0)/2.0).isActive = true
        self.maxRangeField.widthAnchor.constraint(equalTo: self.minRangeField.widthAnchor).isActive = true
    }
    
    func reset() {
        self.minRangeField.text = ""
        self.maxRangeField.text = ""
        self.minValue = nil
        self.maxValue = nil
    }
    
    @discardableResult override func becomeFirstResponder() -> Bool {
        if self.minRangeField.becomeFirstResponder() {
            return true
        }
        else {
            return false
        }
    }
}
