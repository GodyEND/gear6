//
//  Gear6PropertyView.swift
//  Gear6
//
//  Created by Gody on 19/11/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6PropertyView: UIView, Gear6ViewAnimationProtocol {
    var propertyID: Int!
    var position: CGVector!
    var maskLayer: CAShapeLayer?
    
    private var effectView = UIView()
    
    private var reflectiveLayer = CAShapeLayer()
    private let reflectiveAnimation =  CABasicAnimation(keyPath: "position")
    private var reflectiveTimer: Timer?
    var isReflective: Bool = false // Reflective Surface Effect
    {
        didSet {
            self.updateAttachedEffect()
            //let animation = CABasicAnimation(keyPath: "position")
            self.reflectiveAnimation.fromValue = CGPoint(x: self.reflectiveLayer.frame.origin.x, y: self.reflectiveLayer.frame.origin.y)
            self.reflectiveAnimation.toValue = CGPoint(x: self.reflectiveLayer.frame.origin.x + self.reflectiveLayer.frame.size.width * 2.0, y: self.reflectiveLayer.frame.origin.y + self.reflectiveLayer.frame.size.height * 2.0)
            self.reflectiveAnimation.duration = 0.5
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
        
        self.effectView.frame = self.bounds
        self.effectView.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        
    }
    
    private func updateAttachedEffect() {
        if isReflective {
            // Check if View already Attached
            if let layers = self.effectView.layer.sublayers {
                guard (layers.filter { $0 == self.reflectiveLayer }).count == 0 else { return }
            }
            
            self.reflectiveLayer.frame = CGRect(x: self.bounds.origin.x-self.bounds.width, y: self.bounds.origin.y-self.bounds.height, width: self.bounds.size.width, height: self.bounds.size.height)
            self.reflectiveLayer.masksToBounds = true
            self.reflectiveLayer.contents  = #imageLiteral(resourceName: "Gear6BackButtonCover").cgImage
            self.effectView.layer.addSublayer(self.reflectiveLayer)
        }
        else {
            self.reflectiveLayer.removeFromSuperlayer()
            self.reflectiveLayer.removeAllAnimations()
        }
        
        if let _ = self.effectView.layer.sublayers {
            guard self.subviews.contains(self.effectView) == false else { return }
            self.effectView.layer.mask = self.maskLayer!
            self.addSubview(self.effectView)
        }
    }
    
    func gyroUpdate(source: CGSize) {
        // todo: hasShadow
        self.updateShadow(source: source)
    }
    
    func standbyAnimation(_ delay: TimeInterval = 0.0) {
        // User Interaction Effects update
        self.playStandbyEffects()
        // TODO: Shadow effect
        guard let gyroscope = Gear6BaseViewController.motionManager.gyroData else { return }
        self.gyroUpdate(source: CGSize(width: gyroscope.rotationRate.y, height: gyroscope.rotationRate.x))
    }
    
    func showAnimation(_ delay: TimeInterval = 0.0, completion: ((Bool)->Void)?) {
        // nothing
    }
    
    func clearAnimation(_ delay: TimeInterval = 0.0, completion: @escaping (Bool)->Void) {
        if let timer = self.reflectiveTimer {
            timer.invalidate()
        }
    }
    
    @objc func playStandbyEffects() {
        if Gear6NavigationController.shared.activeViewController.view.subviews.contains(self) {
            if self.isReflective {
                self.reflectiveLayer.add(self.reflectiveAnimation, forKey: "position")
                self.reflectiveTimer = Timer.scheduledTimer(timeInterval: Double.random(in: 4...6), target: self, selector: #selector(playStandbyEffects), userInfo: nil, repeats: false)
            }
        }
    }
}
