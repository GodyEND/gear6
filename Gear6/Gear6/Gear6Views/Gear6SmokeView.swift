//
//  Gear6SmokeView.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import SpriteKit

class Gear6SmokeView: SKView {
    
    var particles = [SKSpriteNode]()
    
    var cellVelocities = [[CGVector]]()
    var prevVelocities = [[CGVector]]()
    var gridLengthX: Int = 0
    var gridLengthY: Int = 0
    var cellNumber: Int = 0
    
    var sceneNode: SKScene?
    var touchEnabled: Bool = true
    private let viscocity: CGFloat = 0.001002
    private var n: Int = 0
    
    static let shared = Gear6SmokeView(frame: CGRect(x: 0, y:0, width: 800, height: 800), gridLength: 10)
    
    init(frame: CGRect, gridLength: Int) {
        super.init(frame: frame)
        
        self.alpha = 0.4
        let aspect = Double(self.frame.size.height / self.frame.size.width)
        gridLengthX = gridLength
        gridLengthY = Int(aspect * Double(gridLengthX))
        cellNumber = (gridLengthX-2) * (gridLengthY-2)
        sceneNode = SKScene(size: CGSize(width: gridLengthX, height: gridLengthY))
        n = gridLengthX-2
        
        for i in 0..<gridLengthX {
            cellVelocities.append([])
            prevVelocities.append([])
            for _ in 0..<gridLengthY {
                cellVelocities[i].append(CGVector(dx: 0.0, dy: 0.0))
                prevVelocities[i].append(CGVector(dx: 0.0, dy: 0.0))
            }
        }
        
        
        /// load particles
        let ptex = SKTexture(image: #imageLiteral(resourceName: "SmokeParticles"))
        for _ in 0..<cellNumber {
            let p = SKSpriteNode(texture: ptex, color: .gray, size: CGSize(width: Double(gridLength) * 1.3, height: Double(gridLength) * 1.3))
            let diceRoll = Int(arc4random_uniform(360))
            p.zRotation = CGFloat(Double(diceRoll) / 360.0 * Double.pi)
            p.alpha = 0.7
            //let physics = SKPhysicsBody(circleOfRadius: 6.5)
            //physics.allowsRotation = true
            //physics.velocity = CGVector(dx: 0.0, dy: 0.0)
            //p.physicsBody = physics
            particles.append(p)
        }
        
        // particle has pos x and pos y
        for i in 1...n {
            for j in 1...n {
                particles[(i-1)*n + j-1].position = CGPoint(x: i, y: j)
            }
        }
        
        // TODO: apply torque and rotate sprite
        
        // check vc and enable/disable interaction
        for index in 0..<cellNumber {
            sceneNode?.addChild(particles[index])
        }
    }
    
    func presentScene() {
        self.presentScene(sceneNode)
    }
    
    func updateScene() {
        let timer = Timer(timeInterval: 1/30, target: self, selector: #selector(Gear6SmokeView.update), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateGrid(dt: CGFloat) {
        diffuseGrid(dt: dt)
        swapGrid()
        projectGrid()
        advectGrid(dt: dt)
        swapGrid()
        projectGrid()
    }
    
    private func diffuseGrid(dt: CGFloat) {
        let a = dt * viscocity * CGFloat(gridLengthX * gridLengthY)
        
        for _ in 0..<20 {
            for i in 1...n {
                for j in 1...n {
                    prevVelocities[i][j].dx = (cellVelocities[i][j].dx + a*(prevVelocities[i-1][j].dx + prevVelocities[i+1][j].dx + prevVelocities[i][j-1].dx + prevVelocities[i][j+1].dx)) / (1.0+4.0*a)
                    prevVelocities[i][j].dy = (cellVelocities[i][j].dy + a*(prevVelocities[i-1][j].dy + prevVelocities[i+1][j].dy + prevVelocities[i][j-1].dy + prevVelocities[i][j+1].dy)) / (1.0+4.0*a)
                }
            }
            setGridBounds1(b0: n, b1: 2)
        }
    }
    
    private func advectGrid(dt: CGFloat) {
        var i0:Int = 0, j0:Int = 0, i1:Int = 0, j1:Int = 0
        var x:CGFloat = 0.0, y:CGFloat = 0.0, s0:CGFloat = 0.0, t0:CGFloat = 0.0, s1:CGFloat = 0.0, t1 :CGFloat = 0.0, dt0:CGFloat = 0.0
        
        dt0 = dt * CGFloat(n)
        
        for i in 1...n {
            for j in 1...n {
                x = CGFloat(i) - dt0*cellVelocities[i][j].dx
                x = CGFloat(j) - dt0*cellVelocities[i][j].dy
                
                if (x < 0.5) { x = 0.5 }
                if (x > CGFloat(n) + 0.5) { x = CGFloat(n) + 0.5 }
                i0 = Int(x)
                i1 = i0 + 1
                
                if (y < 0.5) { y = 0.5 }
                if (y > CGFloat(n) + 0.5) { y = CGFloat(n) + 0.5 }
                j0 = Int(y)
                j1 = j0 + 1
                
                s1 = x - CGFloat(i0)
                s0 = 1 - s1
                t1 = y - CGFloat(j0)
                t0 = 1 - t1
                
                prevVelocities[i][j].dx = s0*(t0*cellVelocities[i0][j0].dx + t1 * cellVelocities[i0][j1].dx) + s1*(t0*cellVelocities[i1][j0].dx + t1*cellVelocities[i1][j1].dy)
                prevVelocities[i][j].dy = s0*(t0*cellVelocities[i0][j0].dy + t1 * cellVelocities[i0][j1].dy) + s1*(t0*cellVelocities[i1][j0].dy + t1*cellVelocities[i1][j1].dy)
            }
        }
    }
    
    
    private func projectGrid() {
        var div = [[CGFloat]]()
        var p = [[CGFloat]]()
        let h = 1.0 / CGFloat(n)
        
        for i in 0..<gridLengthX {
            div.append([])
            p.append([])
            for j in 0..<gridLengthY {
                div[i].append(cellVelocities[i][j].dy)
                p[i].append(0.0)
            }
        }
        
        for i in 1...n {
            for j in 1...n {
                div[i][j] = -0.5 * h * (cellVelocities[i+1][j].dx - cellVelocities[i-1][j].dx + cellVelocities[i][j+1].dy - cellVelocities[i][j-1].dy)
            }
        }
        
        setGridBounds0(v: &div)
        setGridBounds0(v: &p)
        
        for _ in 0..<20 {
            for i in 1...n {
                for j in 1...n {
                    p[i][j] = (div[i][j] + p[i-1][j] + p[i+1][j] + p[i][j-1] + p[i][j+1]) / 4.0
                }
            }
            setGridBounds0(v: &p)
        }
        
        for i in 1...n {
            for j in 1...n {
                cellVelocities[i][j].dx -= 0.5*(p[i+1][j] - p[i-1][j]) / h
                cellVelocities[i][j].dy -= 0.5*(p[i][j+1] - p[i][j-1]) / h
            }
        }
        
        setGridBounds1(b0: 1, b1: 2)
    }
    
    private func swapGrid() {
        let temp = cellVelocities
        cellVelocities = prevVelocities
        prevVelocities = temp
    }
    
    
    
    private func setGridBounds0(v: inout [[CGFloat]]) {
        for i in 1...n {
            v[0][i] = v[1][i]
            v[n+1][i] = v[n][i]
            v[i][0] = v[i][1]
            v[i][n+1] = v[i][n]
        }
        
        v[0][0] = 0.5*(v[1][0] + v[0][1])
        v[0][n+1] = 0.5*(v[1][n+1] + v[0][n])
        v[n+1][0] = 0.5*(v[n][0] + v[n+1][1])
        v[n+1][n+1] = 0.5*(v[n][n+1] + v[n+1][n])
    }
    
    private func setGridBounds0() {
        for i in 1...n {
            prevVelocities[0][i] = prevVelocities[1][i]
            prevVelocities[n+1][i] = prevVelocities[n][i]
            prevVelocities[i][0] = prevVelocities[i][1]
            prevVelocities[i][n+1] = prevVelocities[i][n]
        }
        
        prevVelocities[0][0].dx = 0.5*(prevVelocities[1][0].dx + prevVelocities[0][1].dx)
        prevVelocities[0][0].dy = 0.5*(prevVelocities[1][0].dy + prevVelocities[0][1].dy)
        
        prevVelocities[0][n+1].dx = 0.5*(prevVelocities[1][n+1].dx + prevVelocities[0][n].dx)
        prevVelocities[0][n+1].dy = 0.5*(prevVelocities[1][n+1].dy + prevVelocities[0][n].dy)
        
        prevVelocities[n+1][0].dx = 0.5*(prevVelocities[n][0].dx + prevVelocities[n+1][1].dx)
        prevVelocities[n+1][0].dy = 0.5*(prevVelocities[n][0].dy + prevVelocities[n+1][1].dy)
        
        prevVelocities[n+1][n+1].dx = 0.5*(prevVelocities[n][n+1].dx + prevVelocities[n+1][n].dx)
        prevVelocities[n+1][n+1].dy = 0.5*(prevVelocities[n][n+1].dy + prevVelocities[n+1][n].dy)
    }
    
    private func setGridBounds1(b0: Int, b1: Int) {
        for i in 1...n {
            prevVelocities[0][i].dx = b0 == 1 ? -prevVelocities[1][i].dx : prevVelocities[1][i].dx
            prevVelocities[n+1][i].dx = b0 == 1 ? -prevVelocities[n][i].dx : prevVelocities[n][i].dx
            prevVelocities[i][0].dx = b0 == 2 ? -prevVelocities[i][1].dx : prevVelocities[i][1].dx
            prevVelocities[i][n+1].dx = b0 == 2 ? -prevVelocities[i][n].dx : prevVelocities[i][n].dx
            
            prevVelocities[0][i].dy = b1 == 1 ? -prevVelocities[1][i].dy : prevVelocities[1][i].dy
            prevVelocities[n+1][i].dy = b1 == 1 ? -prevVelocities[n][i].dy : prevVelocities[n][i].dy
            prevVelocities[i][0].dy = b1 == 2 ? -prevVelocities[i][1].dy : prevVelocities[i][1].dy
            prevVelocities[i][n+1].dy = b1 == 2 ? -prevVelocities[i][n].dy : prevVelocities[i][n].dy
        }
        
        prevVelocities[0][0].dx = 0.5*(prevVelocities[1][0].dx + prevVelocities[0][1].dx)
        prevVelocities[0][0].dy = 0.5*(prevVelocities[1][0].dy + prevVelocities[0][1].dy)
        
        prevVelocities[0][n+1].dx = 0.5*(prevVelocities[1][n+1].dx + prevVelocities[0][n].dx)
        prevVelocities[0][n+1].dy = 0.5*(prevVelocities[1][n+1].dy + prevVelocities[0][n].dy)
        
        prevVelocities[n+1][0].dx = 0.5*(prevVelocities[n][0].dx + prevVelocities[n+1][1].dx)
        prevVelocities[n+1][0].dy = 0.5*(prevVelocities[n][0].dy + prevVelocities[n+1][1].dy)
        
        prevVelocities[n+1][n+1].dx = 0.5*(prevVelocities[n][n+1].dx + prevVelocities[n+1][n].dx)
        prevVelocities[n+1][n+1].dy = 0.5*(prevVelocities[n][n+1].dy + prevVelocities[n+1][n].dy)
    }
    
    
    
    private func getEulerIntegration(particle: SKSpriteNode, time: CGFloat, velocity: CGVector, acceleration: CGVector) -> SKSpriteNode {
        var newVelocity = CGVector(dx:0, dy:0)
        newVelocity.dx = velocity.dx + time*acceleration.dx
        newVelocity.dy = velocity.dy + time*acceleration.dy
        particle.position.x += newVelocity.dx*time
        particle.position.y += newVelocity.dy*time
        return particle
    }
    
    private func integrateGrid(dt: CGFloat) {
        
        //var position = CGVector()
        
        // Find cell for partice
        
        for index in 0..<cellNumber {
            let velocity = cellVelocities[Int(particles[index].position.x)][Int(particles[index].position.y)]
            particles[index] = getEulerIntegration(particle: particles[index], time: CGFloat(dt), velocity: velocity, acceleration: CGVector(dx: 0.0, dy: 0.0))
        }
    }
}

@objc extension Gear6SmokeView {
    func update() {
        let dt: CGFloat = 1/30
        // update cursor pos
        updateGrid(dt: dt)
        integrateGrid(dt: dt)
    }
}
