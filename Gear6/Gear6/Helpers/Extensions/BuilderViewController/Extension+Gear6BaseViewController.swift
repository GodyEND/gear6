//
//  Extension+Gear6BaseViewController.swift
//  Gear6
//
//  Created by Gody on 30/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

// MARK: Setup Views

extension Gear6BaseViewController {
    func setupTouchGestures() {
        let tapToDismiss = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        self.view.addGestureRecognizer(tapToDismiss)
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        leftSwipeGesture.direction = .left
        self.view.addGestureRecognizer(leftSwipeGesture)
        let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        rightSwipeGesture.direction = .right
        self.view.addGestureRecognizer(rightSwipeGesture)
    }
    
    func setupDefaultViews() {
        self.tableView.frame = CGRect(x: Screen.TAB_VIEW_LEFT, y: Screen.TAB_VIEW_TOP, width: Screen.TAB_VIEW_WIDTH, height: Screen.TAB_VIEW_HEIGHT)
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        self.tableView.isHidden = true
        self.view.addSubview(self.tableView)
        
        self.pickerView.frame = CGRect(x: Screen.Width * 0.1, y: Screen.FIRST_ITEM_TOP+(3*Screen.TEXTFIELD_HEIGHT), width: Screen.Width * 0.8, height: Screen.Height * 0.5)
        self.pickerView.backgroundColor = UIColor.cyan
        self.pickerView.isHidden = true
        self.view.addSubview(self.pickerView)
        
        self.textField.frame = CGRect(x: 75, y: Screen.TAB_VIEW_TOP-(Screen.TEXTFIELD_HEIGHT+10.0), width: Screen.TEXTFIELD_WIDTH, height: Screen.TEXTFIELD_HEIGHT)
        self.textField.backgroundColor = UIColor.white
        self.textField.delegate = self
        self.textField.isHidden = true
        self.view.addSubview(self.textField)
    }
    
    func setupSmokeView() {
        //Gear6SmokeView.shared.updateScene()
        self.view.addSubview(Gear6SmokeView.shared)
        self.view.sendSubview(toBack: Gear6SmokeView.shared)
        Gear6SmokeView.shared.presentScene()
    }
    
    func setupScreenCover() {
        // Screen Cover
        let coverView = UIImageView()
        coverView.frame = CGRect(x:0, y: 0, width: Gear6Defaults.windowFrame.width, height: Gear6Defaults.windowFrame.height)
        coverView.image = #imageLiteral(resourceName: "Gear6CoverLayer")
        coverView.alpha = 0.5
        self.view.addSubview(coverView)
        
        // Laser Boarder
        let laserBorderLeft = UIImageView()
        laserBorderLeft.frame = CGRect(x:0, y: 0, width: 15, height: Gear6Defaults.windowFrame.height)
        laserBorderLeft.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        self.view.addSubview(laserBorderLeft)
        
        let laserBorderRight = UIImageView()
        laserBorderRight.frame = CGRect(x:Gear6Defaults.windowFrame.width - 15, y: 0, width: 15, height: Gear6Defaults.windowFrame.height)
        laserBorderRight.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        laserBorderRight.transform = laserBorderRight.transform.rotated(by: CGFloat(Double.pi))
        self.view.addSubview(laserBorderRight)
        
        let laserBorderBottom = UIImageView()
        laserBorderBottom.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        laserBorderBottom.transform = laserBorderBottom.transform.rotated(by: CGFloat(Double.pi * 3 / 2))
        laserBorderBottom.frame = CGRect(x:0, y: Gear6Defaults.windowFrame.height - 7, width: Gear6Defaults.windowFrame.width, height: 7)
        self.view.addSubview(laserBorderBottom)
        
        self.navigationItem.backBarButtonItem = nil
    }
}

@objc extension Gear6BaseViewController : Gear6ViewControllerProtocol {
    
    func setupView() {
        setupTouchGestures()
        //setupSmokeView()
        setupScreenCover()
        setupDefaultViews()
    }
    
    func dismissAction() {
        // overridable tap to dismiss
    }
    
    func swipeAction(_ sender: UISwipeGestureRecognizer) {
        // overidable swipe Gestures
    }
    
    func presentScreen(completion: @escaping (Bool) -> Void) {
        // overridable
    }
    
    func clearScreen(completion: ((Bool) -> Void)? = nil) {
        // overridable
    }
    
    func standbyAnimation() {
        // overridable
    }
}

extension Gear6BaseViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= Defaults.NAME_MAXIMUM_LENGTH
    }
}
