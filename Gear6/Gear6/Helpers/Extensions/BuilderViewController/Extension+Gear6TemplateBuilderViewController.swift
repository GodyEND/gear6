//
//  Extension+Gear6TemplateBuilderViewController.swift
//  Gear6
//
//  Created by Gody on 29/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

extension Gear6TemplateBuilderViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (self.viewState) {
        case .Main:
            if let template = Gear6Elements.getMainTemplate(catID: self.currentSelectedCategory) {
                return (template.data?.count)! + 1
            }
        case .Extension:
            if let template = Gear6Elements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                return (template.data?.count)! + 1
            }
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.TemplateBuilderView.rawValue, for: indexPath) as! Gear6TemplateViewCell
        cell.pickerDelegate = self
        
        if let template = (self.viewState == .Main) ? Gear6Elements.getMainTemplate(catID: self.currentSelectedCategory) : Gear6Elements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
            if (indexPath.row == (template.data?.allObjects.count)!) {
                cell.setupCell(dataType: nil)
            }
            else {
                // Display Existing Template from array
                let array = (template.data?.allObjects as! [DataBlock]).sorted{$0.id < $1.id}
                cell.setupCell(dataType:array[indexPath.row])
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard self.textField.isHidden, self.rangeField.isHidden, self.pickerView.isHidden else { return false }
        
        if (indexPath.row == Gear6Elements.getTemplate(id: self.activeTemplate)?.data?.count ?? 0) { return false }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            if let template = Gear6Elements.getTemplate(id: self.activeTemplate) {
                let dataArray = (template.data?.allObjects as! [DataBlock]).sorted(by: {$0.id < $1.id})
                Gear6Elements.removeDataBlock(tempID: Int(template.id), dataTempID: Int(dataArray[indexPath.row].id))
                tableView.reloadData()
            }
        }
    }
}

extension Gear6TemplateBuilderViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch (self.pickerState) {
        case .MainTemplate:
            guard row < Gear6Elements.categories.count else { return }
            let category = Gear6Elements.categories[row]
            self.selectedMainTemplField.text = category.name
            self.currentSelectedCategory = Int(category.id)
            self.activeTemplate = Int(category.template)
            self.viewState = .Main
            self.update()
            self.hidePicker()
        case .ExtensionTemplate:
            if (row == 0) {
                self.setDefaultGroup()
                if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                    self.activeTemplate = Int(category.template)
                    self.viewState = .Main
                }
            }
            else {
                if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    if (groupArray.count > 0) {
                        let group = groupArray[row-1]
                        self.currentSelectedGroup = Int(group.id)
                        self.selectedExtensionTemplField.text = group.name
                        self.activeTemplate = Int(group.template)
                        self.viewState = .Extension
                    }
                }
            }
            self.update()
            self.hidePicker()
        case .AddData, .EditData:
            // Update EditedDataType
            self.editDataBlock(pickerView: pickerView, row: row, component: component)
        case .Quantity:
            self.currentEditedDatatype.quantityType = Int16(row+1)
        case .Expansion:
            self.currentEditedDatatype.expansionType = Int16(row+1)
        default: break
        }
        
        pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch (self.pickerState) {
        case .AddData, .EditData:
            if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                let objArray = category.objects?.allObjects as! [Obj]
                let groupArray = category.subcategories?.allObjects as! [SubCategory]
                if (groupArray.count == 0 && objArray.count > 0) ||
                    (groupArray.count > 0 && objArray.count == 0) {
                    return 2
                }
                else if (groupArray.count > 0 && objArray.count > 0) {
                    return 3
                }
            }
            return 1
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        switch (self.pickerState) {
        case .MainTemplate:
            return Gear6Elements.categories.count
        case .ExtensionTemplate:
            return (Gear6Elements.getCategory(id: self.currentSelectedCategory)?.subcategories?.count)! + 1
        case .AddData, .EditData:
            switch (component) {
            case 1:
                if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    if self.currentEditedDatatype.groupID > 0 || (category.subcategories?.allObjects.count)! > 0 {
                        return (category.subcategories?.allObjects.count)! + 1
                    }
                    return (category.objects?.allObjects.count)! + 1
                }
            case 2:
                if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    return (category.objects?.allObjects as! [Obj]).count + 1
                }
            default: return Gear6Elements.categories.count + Gear6Elements.defaultCategories.count
            }
        case .Quantity:
            return Gear6MainDataBlockQuantityType.allCases.count-1
        case .Expansion:
            return Gear6GroupDataBlockExtensionType.allCases.count-1
        default: break
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch (self.pickerState) {
        case .MainTemplate:
            return Gear6Elements.categories[row].name
        case .ExtensionTemplate:
            if (row == 0) {
                return Placeholder.DEFAULT
            }
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                let array = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                guard array.count > 0 else {
                    return "--"
                }
                return array[row-1].name
            }
            return "--"
        case .AddData, .EditData:
            return self.titleForEditedDataBlock(pickerView:pickerView, row:row, component:component)
        case .Quantity:
            switch (row+1) {
            case Gear6MainDataBlockQuantityType.All.rawValue:
                return Gear6MainDataBlockQuantityType.All.stringValue
            case Gear6MainDataBlockQuantityType.Limit.rawValue:
                return Gear6MainDataBlockQuantityType.Limit.stringValue
            case Gear6MainDataBlockQuantityType.Multi.rawValue:
                return Gear6MainDataBlockQuantityType.Multi.stringValue
            case Gear6MainDataBlockQuantityType.Single.rawValue:
                return Gear6MainDataBlockQuantityType.Single.stringValue
            default: return "--"
            }
        case .Expansion:
            switch (row+1) {
            case Gear6GroupDataBlockExtensionType.Add.rawValue:
                return Gear6GroupDataBlockExtensionType.Add.stringValue
            case Gear6GroupDataBlockExtensionType.Remove.rawValue:
                return Gear6GroupDataBlockExtensionType.Remove.stringValue
            case Gear6GroupDataBlockExtensionType.Replace.rawValue:
                return Gear6GroupDataBlockExtensionType.Replace.stringValue
            default: return "--"
            }
        default: return "--"
        }
    }
}

extension Gear6TemplateBuilderViewController {
    func setDefaultGroup() {
        self.currentSelectedGroup = 0
        self.selectedExtensionTemplField.text = Placeholder.DEFAULT
        
        self.selectedExtensionTemplField.isHidden = false
        self.selectedExtensionTemplField.isUserInteractionEnabled = true
    }
    
    func editDataBlock(pickerView: UIPickerView, row: Int, component: Int) {
        guard Gear6Elements.categories.count > 0 else { return }
        
        if pickerView.selectedRow(inComponent: 0) < (Gear6Elements.categories.count) {
            self.currentEditedDatatype.categoryID = Gear6Elements.categories[pickerView.selectedRow(inComponent: 0)].id
        }
        else { // Set Default Category
            self.currentEditedDatatype.categoryID = Gear6Elements.defaultCategories[pickerView.selectedRow(inComponent: 0)-Gear6Elements.categories.count].id
            self.currentEditedDatatype.dataType = Gear6DataType.Content.rawValue
            if self.currentEditedDatatype.categoryID == Gear6ContentType.Range.rawValue {
                self.currentEditedDatatype.dataType = Gear6DataType.Range.rawValue
            }
            if let template = Gear6Elements.getTemplate(id: self.activeTemplate) {
                if template.isExtension {
                    self.currentEditedDatatype.dataType = Gear6DataType.Extension.rawValue
                }
            }
            self.currentEditedDatatype.groupID = 0
            self.currentEditedDatatype.objID = 0
            return
        }
        
        switch (pickerView.numberOfComponents) {
        case 2:
            // Update Selected Rows
            if (component == 0) {
                pickerView.reloadComponent(1)
                pickerView.selectRow(0, inComponent: 1, animated: true)
            }
            
            // Update DataType // Did Select User Category
            if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if (pickerView.selectedRow(inComponent: 1) > 0) {
                    let objArray = (category.objects?.allObjects as! [Obj]).sorted{$0.id < $1.id}
                    if objArray.count > 0 {
                        self.currentEditedDatatype.objID = objArray[pickerView.selectedRow(inComponent: 1)-1].id
                        self.currentEditedDatatype.groupID = 0
                        self.currentEditedDatatype.dataType = Gear6DataType.Object.rawValue
                    }
                    else {
                        let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                        guard groupArray.count > 0 else {
                            return
                        }
                        self.currentEditedDatatype.groupID = groupArray[pickerView.selectedRow(inComponent: 1)-1].id
                        self.currentEditedDatatype.objID = 0
                        self.currentEditedDatatype.dataType = Gear6DataType.Group.rawValue
                    }
                }
                else {
                    self.currentEditedDatatype.groupID = 0
                    self.currentEditedDatatype.objID = 0
                    self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                }
            }
        case 3:
            // Update Selected Rows
            if (component == 0) {
                pickerView.reloadComponent(1)
                pickerView.selectRow(0, inComponent: 1, animated: true)
                pickerView.reloadComponent(2)
                pickerView.selectRow(0, inComponent: 2, animated: true)
            }
            else if (component == 1) {
                pickerView.reloadComponent(2)
                pickerView.selectRow(0, inComponent: 2, animated: true)
            }
            else {
                pickerView.selectRow(0, inComponent: 1, animated: true)
            }
            
            // Update DataType
            
            if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if (pickerView.selectedRow(inComponent: 1) > 0) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    guard groupArray.count > 0 else {
                        return
                    }
                    self.currentEditedDatatype.groupID = groupArray[pickerView.selectedRow(inComponent: 1)-1].id
                    self.currentEditedDatatype.dataType = Gear6DataType.Group.rawValue
                    self.currentEditedDatatype.objID = 0
                }
                else {
                    if (pickerView.selectedRow(inComponent: 2) > 0) {
                        let objArray = (category.objects?.allObjects as! [Obj]).sorted{$0.id < $1.id}
                        guard objArray.count > 0 else { return }
                        self.currentEditedDatatype.objID = objArray[pickerView.selectedRow(inComponent: 2)-1].id
                        self.currentEditedDatatype.dataType = Gear6DataType.Object.rawValue
                    }
                    else {
                        self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                        self.currentEditedDatatype.groupID = 0
                        self.currentEditedDatatype.objID = 0
                    }
                }
            }
        default: self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
        }
        
        if let template = Gear6Elements.getTemplate(id: self.activeTemplate) {
            if template.isExtension {
                self.currentEditedDatatype.dataType = Gear6DataType.Extension.rawValue
            }
        }
    }
    
    func titleForEditedDataBlock(pickerView: UIPickerView, row: Int, component: Int) -> String? {
        switch (component) {
        case 1:
            if (row == 0) {
                return "--"
            }
            if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if ((category.subcategories?.allObjects.count)! > 0) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    guard groupArray.count > 0 else { return "--" }
                    return groupArray[row-1].name
                }
                else {
                    let objArray = (category.objects?.allObjects as! [Obj]).sorted{$0.id < $1.id}
                    guard objArray.count > 0 else { return "--" }
                    return objArray[row-1].name
                }
            }
        case 2:
            if (row == 0) {
                return "--"
            }
            if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                return ((category.objects?.allObjects as! [Obj]).sorted{$0.id < $1.id})[row-1].name
            }
        default:
            guard row < Gear6Elements.categories.count + Gear6Elements.defaultCategories.count else { return "--" }
            
            if (row < Gear6Elements.categories.count) {
                return Gear6Elements.categories[row].name
            }
            else {
                return Gear6Elements.defaultCategories[row-Gear6Elements.categories.count].name
            }
        }
        return "--"
    }
}

// MARK: TextFieldDelegate

extension Gear6TemplateBuilderViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.keyboardType == .numberPad {
            if textField === self.rangeField.minRangeField {
                self.rangeField.minValue = Int(textField.text ?? "0")
                self.rangeField.maxRangeField.becomeFirstResponder()
            }
            else if textField === self.rangeField.maxRangeField {
                if (self.rangeField.minRangeField.text?.count)! > 0 {
                    self.rangeField.minValue = Int(self.rangeField.minRangeField.text ?? "0")
                }
                self.rangeField.maxValue = Int(textField.text ?? "0")
            
                if let min = self.rangeField.minValue {
                    guard min < self.rangeField.maxValue! else { return false }
                    
                    self.rangeField.minRangeField.text = ""
                    self.rangeField.maxRangeField.text = ""
                    self.rangeField.maxRangeField.resignFirstResponder()
                    self.rangeField.isHidden = true
                }
                else {
                    return false
                }
            }
        }
        else {
            self.currentEditedDatatype.desc = textField.text ?? ""
            textField.text = ""
            textField.resignFirstResponder()
            textField.isHidden = true
        }
        self.dismissAction()
        
        return false
    }
}
