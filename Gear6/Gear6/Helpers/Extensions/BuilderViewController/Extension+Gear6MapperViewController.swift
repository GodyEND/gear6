//
//  Extension+Gear6MapperViewController.swift
//  Gear6
//
//  Created by Gody on 28/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

extension Gear6MapperViewController {
    func addCategoryAction(name: String) {
        // TODO: Cannot use Text, Image, Value as name
        // TODO: notify user
        // TODO: Test if issues occure if I do use these names
        guard name != "Value", name != "Text", name != "Image", name != "Category" else { return }

        switch (self.viewState) {
        case .Category:
            let result = Gear6Elements.categories.filter {$0.name == name}
            if (result.count == 0) {
                let newCategory = CoreDataManager<Category>.getNewObject()
                newCategory.name = name
                newCategory.id = Gear6Elements.getNewCategoryID()
                newCategory.template = Gear6Elements.getNewTemplateID()
                let newTemplate = CoreDataManager<Template>.getNewObject()
                newTemplate.id = newCategory.template
                Gear6Elements.templates.append(newTemplate)
                Gear6Elements.templates = Gear6Elements.templates.sorted{$0.id < $1.id}
                Gear6Elements.categories.append(newCategory)
                Gear6Elements.categories = Gear6Elements.categories.sorted{$0.id < $1.id}
                Gear6Elements.gear?.addToCategories(newCategory)
            }
            else {
                return // TODO: Notif Category Already Exists
            }
        case .Grouping:
            if let category = Gear6Elements.getCategory(id: self.currentCategory) {
                let array = category.subcategories?.allObjects as! [SubCategory]
                let result = array.filter {$0.name == name}
                if (result.count == 0) {
                    let newGroup = CoreDataManager<SubCategory>.getNewObject()
                    newGroup.name = name
                    newGroup.id = Gear6Elements.getNewGroupID()
                    newGroup.categoryID = category.id
                    newGroup.template = Gear6Elements.getNewTemplateID()
                    let newTemplate = CoreDataManager<Template>.getNewObject()
                    newTemplate.id = newGroup.template
                    newTemplate.isExtension = true
                    Gear6Elements.templates.append(newTemplate)
                    Gear6Elements.templates = Gear6Elements.templates.sorted{$0.id < $1.id}
                    category.addToSubcategories(newGroup)
                    Gear6Elements.groupings.append(newGroup)
                    Gear6Elements.groupings = Gear6Elements.groupings.sorted{$0.id < $1.id}
                }
                else {
                    return // TODO: Notif Group Already Exists
                }
            }
        case .EditCategory:
            if let category = Gear6Elements.getCategory(id: self.editID) {
                let result = Gear6Elements.categories.filter {$0.name == name}
                if (result.count == 0) {
                    category.name = name
                }
                else {
                    // TODO: Notif Category Already Exists
                }
            }
            self.viewState = .Category
        case .EditGroup:
            if let group = Gear6Elements.getGroup(catID: self.currentCategory, groupID: self.editID) {
                if let category = Gear6Elements.getCategory(id: self.currentCategory) {
                    let array = category.subcategories?.allObjects as! [SubCategory]
                    let result = array.filter {$0.name == name}
                    if (result.count == 0) {
                        group.name = name
                    }
                    else {
                        // TODO: Notif Group Already Exists
                    }
                }
            }
            self.viewState = .Grouping
        default: break
        }
        self.update()
    }
}

extension Gear6MapperViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (self.viewState) {
        case .Category:
            return Gear6Elements.categories.count
        case .Grouping:
            if let category = Gear6Elements.getCategory(id: self.currentCategory) {
                if let  groupArray = category.subcategories {
                    return groupArray.count
                }
            }
        default: break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.MapperView.rawValue, for: indexPath) as! Gear6MapperViewCell
        
        switch (self.viewState) {
        case .Category:
            cell.setupCell(category: Gear6Elements.categories[indexPath.row], group: nil, state: self.viewState)
        case .Grouping:
            if let category = Gear6Elements.getCategory(id: self.currentCategory) {
                let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                if (groupArray.count > 0) {
                    cell.setupCell(category: category, group: groupArray[indexPath.row], state: self.viewState)
                }
            }
        default: break
        }
        
        return cell
    }
}


// MARK: TextFieldDelegate

extension Gear6MapperViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch (self.viewState) {
        case .EditCategory, .EditGroup: break
        default: textField.text = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (!textField.text!.isEmpty) {
            self.addCategoryAction(name: textField.text!)
        }
        else {
            switch (self.viewState) {
            case .EditCategory:
                self.viewState = .Category
                textField.placeholder = Placeholder.ADD_CAT
            case .EditGroup:
                self.viewState = .Grouping
                textField.placeholder = Placeholder.ADD_GROUP
            default: break
            }
        }
        textField.text = ""
        textField.resignFirstResponder()
        
        return false
    }
}
