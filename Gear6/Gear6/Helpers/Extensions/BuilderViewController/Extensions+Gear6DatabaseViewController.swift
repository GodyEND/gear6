//
//  Extensions+Gear6DatabaseViewController.swift
//  Gear6
//
//  Created by Gody on 15/02/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import UIKit

extension Gear6DatabaseViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (self.viewState) {
        case .AddToCategory:
            guard self.getActiveObjects().count > 0 else { return 1 }
            return self.getActiveObjects().count
        case .AddToGroup:
            guard self.getActiveGroupObjects().count > 0 else { return 1 }
            return self.getActiveGroupObjects().count
        default: return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.DatabaseView.rawValue, for: indexPath) as! Gear6DatabaseViewCell
        cell.cellDelegate = self
        
        switch (self.viewState) {
        case .AddToCategory:
            guard self.getActiveObjects().count > 0 else { return cell }
            cell.setupCell(name: self.getActiveObjects()[indexPath.row].name!, id: Int(self.getActiveObjects()[indexPath.row].id))
        case .AddToGroup:
            guard self.getActiveGroupObjects().count > 0 else { return cell }
            cell.setupCell(name: self.getActiveGroupObjects()[indexPath.row].name!, id: Int(self.getActiveGroupObjects()[indexPath.row].id))
        default: break
        }
        
        return cell
    }
}

extension Gear6DatabaseViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch (self.pickerState) {
        case .Category:
            return Gear6Elements.categories.count
        case .Group:
            return (Gear6Elements.getCategory(id: self.currentSelectedCategory)?.subcategories?.allObjects.count ?? 0) + 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch (self.pickerState) {
        case .Category:
            Gear6Elements.categories.sort { $0.id < $1.id }
            self.currentSelectedCategory = Int(Gear6Elements.categories[row].id)
            self.activeCategoryField.text = Gear6Elements.categories[row].name
            self.viewState = .AddToCategory
        case .Group:
            if row == 0 { self.currentSelectedGroup = 0 }
            else {
                if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted { $0.id < $1.id }
                    self.currentSelectedGroup = Int(groupArray[row-1].id)
                    self.activeGroupField.text = groupArray[row-1].name
                    self.viewState = .AddToGroup
                }
            }
        }
        self.getActiveTemplate()
        self.update()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch (self.pickerState) {
        case .Category:
            Gear6Elements.categories.sort { $0.id < $1.id }
            return Gear6Elements.categories[row].name
        case .Group:
            if row == 0 { return Placeholder.DEFAULT }
            else {
                if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted { $0.id < $1.id }
                    return groupArray[row-1].name
                }
            }
        }
        return "--"
    }
}


// MARK: TextFieldDelegate

extension Gear6DatabaseViewController {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.text?.isEmpty)! {
            textField.resignFirstResponder()
            textField.isHidden = true
        }
        else {
            if textField == self.assetLoaderView.notationField {
                textField.text = ""
                textField.resignFirstResponder()
            }
            else {
                self.currentEditedObj.name = textField.text ?? ""
                textField.text = ""
                textField.resignFirstResponder()
                self.textField.isHidden = true
                self.endNewObjectAddition()
            }
        }
        self.deleteObjBtn.isHidden = true
        
        return false
    }
}

extension Gear6DatabaseViewController {
    func setDefaultGroup() {
        self.currentSelectedGroup = 0
        self.activeGroupField.text = Placeholder.DEFAULT
        
        self.activeGroupField.isHidden = false
        self.activeGroupField.isUserInteractionEnabled = true
    }
}
