//
//  Extensions+UIKit.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

extension UIColor {
    class func blend(color1: UIColor, color2: UIColor) -> UIColor {
        let half = CGFloat(0.5)
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return UIColor(red: CGFloat((r1+r2)*half), green: CGFloat((g1+g2)*half), blue: CGFloat((b1+b2)*half), alpha: CGFloat((a1+a2)*half))
    }
    
    class func multiply(color1: UIColor, color2: UIColor) -> UIColor {
        var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
        
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return UIColor(red: CGFloat(r1*r2) , green: CGFloat(g1*g2), blue: CGFloat(b1*b2), alpha: CGFloat(a1*a2))
    }
}


extension UILabel {
    func strokedText() {
        let strokeTextAttributes = NSMutableAttributedString(string: self.text!, attributes: [
            NSAttributedStringKey.strokeColor: UIColor.black,
            NSAttributedStringKey.foregroundColor: self.textColor,
            NSAttributedStringKey.strokeWidth: NSNumber(value: -6.0),
            NSAttributedStringKey.font: self.font
            ])
        
        self.attributedText = strokeTextAttributes
    }
}

extension String {
    func localized(bundle: Bundle = .main, tableName: String? = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
