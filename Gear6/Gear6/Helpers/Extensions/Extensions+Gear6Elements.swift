//
//  Extensions+Gear6Elements.swift
//  Gear6
//
//  Created by Gody on 07/02/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import Foundation

// MARK: Getters
extension Gear6Elements {
    private (set) static var categoryMaxID: Int = 4
    private (set) static var groupingMaxID: Int = 0
    private (set) static var objMaxID: Int = 0
    private (set) static var templateMaxID: Int = 0
    private (set) static var dataTempMaxID: Int = 0
    private (set) static var contentMaxID: Int = 0
    private (set) static var textMaxID: Int = 0
    private (set) static var valueMaxID: Int = 0
    private (set) static var imageMaxID: Int = 0
    
    private (set) static var reusableCategoryIDs: [Int] = []
    private (set) static var reusableGroupIDs: [Int] = []
    private (set) static var reusableObjIDs: [Int] = []
    private (set) static var reusableTemplateIDs: [Int] = []
    private (set) static var reusableDataBlockIDs: [Int] = []
    private (set) static var reusableContentIDs: [Int] = []
    private (set) static var reusableTextIDs: [Int] = []
    private (set) static var reusableValueIDs: [Int] = []
    private (set) static var reusableImageIDs: [Int] = []
    
    // Detect if Max should be reset to 0
    class func initializeReuseIDsFromDefaults() {
        Gear6Elements.categoryMaxID = (UserDefaults.standard.value(forKey: "categoryMaxID") as? Int) ?? 4
        Gear6Elements.groupingMaxID = (UserDefaults.standard.value(forKey: "groupingMaxID") as? Int) ?? 0
        Gear6Elements.objMaxID = (UserDefaults.standard.value(forKey: "objMaxID") as? Int) ?? 0
        Gear6Elements.templateMaxID = (UserDefaults.standard.value(forKey: "templateMaxID") as? Int) ?? 0
        Gear6Elements.dataTempMaxID = (UserDefaults.standard.value(forKey: "dataTempMaxID") as? Int) ?? 0
        Gear6Elements.contentMaxID = (UserDefaults.standard.value(forKey: "contentMaxID") as? Int) ?? 0
        Gear6Elements.textMaxID = (UserDefaults.standard.value(forKey: "textMaxID") as? Int) ?? 0
        Gear6Elements.valueMaxID = (UserDefaults.standard.value(forKey: "valueMaxID") as? Int) ?? 0
        Gear6Elements.imageMaxID = (UserDefaults.standard.value(forKey: "imageMaxID") as? Int) ?? 0
        Gear6Elements.reusableCategoryIDs = (UserDefaults.standard.value(forKey: "reusableCategoryIDs") as? [Int]) ?? []
        Gear6Elements.reusableGroupIDs = (UserDefaults.standard.value(forKey: "reusableGroupIDs") as? [Int]) ?? []
        Gear6Elements.reusableObjIDs = (UserDefaults.standard.value(forKey: "reusableObjIDs") as? [Int]) ?? []
        Gear6Elements.reusableTemplateIDs = (UserDefaults.standard.value(forKey: "reusableTemplateIDs") as? [Int]) ?? []
        Gear6Elements.reusableDataBlockIDs = (UserDefaults.standard.value(forKey: "reusableDataBlockIDs") as? [Int]) ?? []
        Gear6Elements.reusableContentIDs = (UserDefaults.standard.value(forKey: "reusableContentIDs") as? [Int]) ?? []
        Gear6Elements.reusableTextIDs = (UserDefaults.standard.value(forKey: "reusableTextIDs") as? [Int]) ?? []
        Gear6Elements.reusableValueIDs = (UserDefaults.standard.value(forKey: "reusableValueIDs") as? [Int]) ?? []
        Gear6Elements.reusableImageIDs = (UserDefaults.standard.value(forKey: "reusableImageIDs") as? [Int]) ?? []
    }
    
    class func saveReuseIDs() {
        if Gear6Elements.categoryMaxID - 4 == Gear6Elements.reusableCategoryIDs.count {
            Gear6Elements.categoryMaxID = 4
            Gear6Elements.reusableCategoryIDs = []
        }
        if Gear6Elements.groupingMaxID == Gear6Elements.reusableGroupIDs.count {
            Gear6Elements.groupingMaxID = 0
            Gear6Elements.reusableGroupIDs = []
        }
        if Gear6Elements.objMaxID == Gear6Elements.reusableObjIDs.count {
            Gear6Elements.objMaxID = 0
            Gear6Elements.reusableObjIDs = []
        }
        if Gear6Elements.templateMaxID == Gear6Elements.reusableTemplateIDs.count {
            Gear6Elements.templateMaxID = 0
            Gear6Elements.reusableTemplateIDs = []
        }
        if Gear6Elements.dataTempMaxID == Gear6Elements.reusableDataBlockIDs.count {
            Gear6Elements.dataTempMaxID = 0
            Gear6Elements.reusableDataBlockIDs = []
        }
        if Gear6Elements.contentMaxID == Gear6Elements.reusableContentIDs.count {
            Gear6Elements.contentMaxID = 0
            Gear6Elements.reusableContentIDs = []
        }
        if Gear6Elements.textMaxID == Gear6Elements.reusableTextIDs.count {
            Gear6Elements.textMaxID = 0
            Gear6Elements.reusableTextIDs = []
        }
        if Gear6Elements.valueMaxID == Gear6Elements.reusableValueIDs.count {
            Gear6Elements.valueMaxID = 0
            Gear6Elements.reusableValueIDs = []
        }
        if Gear6Elements.imageMaxID == Gear6Elements.reusableImageIDs.count {
            Gear6Elements.imageMaxID = 0
            Gear6Elements.reusableImageIDs = []
        }
        
        UserDefaults.standard.set(Gear6Elements.categoryMaxID, forKey: "categoryMaxID")
        UserDefaults.standard.set(Gear6Elements.groupingMaxID, forKey: "groupingMaxID")
        UserDefaults.standard.set(Gear6Elements.objMaxID, forKey: "objMaxID")
        UserDefaults.standard.set(Gear6Elements.templateMaxID, forKey: "templateMaxID")
        UserDefaults.standard.set(Gear6Elements.dataTempMaxID, forKey: "dataTempMaxID")
        UserDefaults.standard.set(Gear6Elements.contentMaxID, forKey: "contentMaxID")
        UserDefaults.standard.set(Gear6Elements.textMaxID, forKey: "textMaxID")
        UserDefaults.standard.set(Gear6Elements.valueMaxID, forKey: "valueMaxID")
        UserDefaults.standard.set(Gear6Elements.imageMaxID, forKey: "imageMaxID")
        UserDefaults.standard.set(Gear6Elements.reusableCategoryIDs, forKey: "reusableCategoryIDs")
        UserDefaults.standard.set(Gear6Elements.reusableGroupIDs, forKey: "reusableGroupIDs")
        UserDefaults.standard.set(Gear6Elements.reusableObjIDs, forKey: "reusableObjIDs")
        UserDefaults.standard.set(Gear6Elements.reusableTemplateIDs, forKey: "reusableTemplateIDs")
        UserDefaults.standard.set(Gear6Elements.reusableDataBlockIDs, forKey: "reusableDataBlockIDs")
        UserDefaults.standard.set(Gear6Elements.reusableContentIDs, forKey: "reusableContentIDs")
        UserDefaults.standard.set(Gear6Elements.reusableTextIDs, forKey: "reusableTextIDs")
        UserDefaults.standard.set(Gear6Elements.reusableValueIDs, forKey: "reusableValueIDs")
        UserDefaults.standard.set(Gear6Elements.reusableImageIDs, forKey: "reusableImageIDs")
    }
    
    class func getCategory(id: Int) -> Category? {
        return Gear6Elements.categories.first(where: {$0.id == id})
    }
    
    class func getGroup(catID: Int, groupID: Int) -> SubCategory? {
        if let category = Gear6Elements.getCategory(id: catID) {
            let array = category.subcategories?.allObjects as! [SubCategory]
            if let group = array.first(where: {$0.id == groupID}) {
                return group
            }
        }
        return nil
    }
    
    class func getObject(catID: Int, objID: Int) -> Obj? {
        if let category = Gear6Elements.getCategory(id: catID) {
            if let obj = ((category.objects?.allObjects as! [Obj]).first(where: {$0.id == objID })) {
                return obj
            }
        }
        return nil
    }
    
    class func getObject(groupID: Int, objID: Int) -> Obj? {
        if let group = Gear6Elements.groupings.first (where: { $0.id == groupID }) {
            if let obj = ((group.objects?.allObjects as! [Obj]).first(where: {$0.id == objID })) {
                return obj
            }
        }
        return nil
    }
    
    class func getTemplate(id: Int) -> Template? {
        if let template = Gear6Elements.templates.first(where: {$0.id == id}) {
            return template
        }
        return nil
    }
    
    class func getMainTemplate(catID: Int) -> Template? {
        if let category = Gear6Elements.getCategory(id: catID) {
            if let template = Gear6Elements.getTemplate(id: Int(category.template)) {
                return template
            }
        }
        return nil
    }
    
    class func getExtensionTemplate(catID: Int, groupID: Int) -> Template? {
        if let group = Gear6Elements.getGroup(catID: catID, groupID: groupID) {
            if let template = Gear6Elements.getTemplate(id: Int(group.template)) {
                return template
            }
        }
        return nil
    }
    
    class func getDataBlock(id: Int) -> DataBlock? {
        if let DataBlock = Gear6Elements.dataBlocks.first(where: {$0.id == id}) {
            return DataBlock
        }
        return nil
    }
    
    class func getContent(id: Int) -> Content? {
        if let content = Gear6Elements.contents.first(where: {$0.id == id}) {
            return content
        }
        return nil
    }
    
    class func getText(id: Int) -> Text? {
        if let text = Gear6Elements.texts.first(where: {$0.id == id}) {
            return text
        }
        return nil
    }
    
    class func getValue(id: Int) -> Value? {
        if let value = Gear6Elements.values.first(where: {$0.id == id}) {
            return value
        }
        return nil
    }
    
    class func getImage(id: Int) -> Image? {
        if let image = Gear6Elements.images.first(where: {$0.id == id}) {
            return image
        }
        return nil
    }
    
    
    
    // MARK: Get New IDs
    
    class func getNewCategoryID() -> Int32 {
        var newCatID = 0
        if (Gear6Elements.reusableCategoryIDs.count > 0) {
            newCatID = Gear6Elements.reusableCategoryIDs[0]
            Gear6Elements.reusableCategoryIDs = Gear6Elements.reusableCategoryIDs.filter {$0 != newCatID}
        }
        else {
            Gear6Elements.categoryMaxID += 1
            newCatID = Gear6Elements.categoryMaxID
        }
        return Int32(newCatID)
    }
    class func getNewGroupID() -> Int32 {
        var newGroupID = 0
        if (Gear6Elements.reusableGroupIDs.count > 0) {
            newGroupID = Gear6Elements.reusableGroupIDs[0]
            Gear6Elements.reusableGroupIDs = Gear6Elements.reusableGroupIDs.filter {$0 != newGroupID}
        }
        else {
            Gear6Elements.groupingMaxID += 1
            newGroupID = Gear6Elements.groupingMaxID
        }
        return Int32(newGroupID)
    }
    class func getNewObjectID() -> Int32 {
        var newObjectID = 0
        if Gear6Elements.reusableObjIDs.count > 0 {
            newObjectID = Gear6Elements.reusableObjIDs[0]
            Gear6Elements.reusableObjIDs = Gear6Elements.reusableObjIDs.filter { $0 != newObjectID }
        }
        else {
            Gear6Elements.objMaxID += 1
            newObjectID = Gear6Elements.objMaxID
        }
        return Int32(newObjectID)
    }
    class func getNewTemplateID() -> Int32 {
        var newTempID = 0
        if (Gear6Elements.reusableTemplateIDs.count > 0) {
            newTempID = Gear6Elements.reusableTemplateIDs[0]
            Gear6Elements.reusableTemplateIDs = Gear6Elements.reusableTemplateIDs.filter {$0 != newTempID}
        }
        else {
            Gear6Elements.templateMaxID += 1
            newTempID = Gear6Elements.templateMaxID
        }
        return Int32(newTempID)
    }
    class func getNewDataBlockID() -> Int32 {
        var newTempID = 0
        if (Gear6Elements.reusableDataBlockIDs.count > 0) {
            newTempID = Gear6Elements.reusableDataBlockIDs[0]
            Gear6Elements.reusableDataBlockIDs = Gear6Elements.reusableDataBlockIDs.filter {$0 != newTempID}
        }
        else {
            Gear6Elements.dataTempMaxID += 1
            newTempID = Gear6Elements.dataTempMaxID
        }
        return Int32(newTempID)
    }
    class func getNewContentID() -> Int32 {
        var newContentID = 0
        if (Gear6Elements.reusableContentIDs.count > 0) {
            newContentID = Gear6Elements.reusableContentIDs[0]
            Gear6Elements.reusableContentIDs = Gear6Elements.reusableContentIDs.filter {$0 != newContentID}
        }
        else {
            Gear6Elements.contentMaxID += 1
            newContentID = Gear6Elements.contentMaxID
        }
        return Int32(newContentID)
    }
    class func getNewTextID() -> Int32 {
        var newTextID = 0
        if (Gear6Elements.reusableTextIDs.count > 0) {
            newTextID = Gear6Elements.reusableTextIDs[0]
            Gear6Elements.reusableTextIDs = Gear6Elements.reusableTextIDs.filter {$0 != newTextID}
        }
        else {
            Gear6Elements.textMaxID += 1
            newTextID = Gear6Elements.textMaxID
        }
        return Int32(newTextID)
    }
    class func getNewValueID() -> Int32 {
        var newValueID = 0
        if (Gear6Elements.reusableValueIDs.count > 0) {
            newValueID = Gear6Elements.reusableValueIDs[0]
            Gear6Elements.reusableValueIDs = Gear6Elements.reusableValueIDs.filter {$0 != newValueID}
        }
        else {
            Gear6Elements.valueMaxID += 1
            newValueID = Gear6Elements.valueMaxID
        }
        return Int32(newValueID)
    }
    class func getNewImageID() -> Int32 {
        var newImageID = 0
        if (Gear6Elements.reusableImageIDs.count > 0) {
            newImageID = Gear6Elements.reusableImageIDs[0]
            Gear6Elements.reusableImageIDs = Gear6Elements.reusableImageIDs.filter {$0 != newImageID}
        }
        else {
            Gear6Elements.imageMaxID += 1
            newImageID = Gear6Elements.imageMaxID
        }
        return Int32(newImageID)
    }
    
    class func updateDataBlock(lhs: inout DataBlock, with rhs: DataBlockStruct) {
        lhs.id = rhs.id
        lhs.categoryID = rhs.categoryID
        lhs.groupID = rhs.groupID
        lhs.objID = rhs.objID
        lhs.contentID = rhs.contentID
        lhs.quantityType = rhs.quantityType
        lhs.expansionType = rhs.expansionType
        lhs.dataType = rhs.dataType
        lhs.desc = rhs.desc
    }
    class func updateDataBlockStruct(lhs: inout DataBlockStruct, with rhs: DataBlock) {
        lhs.id = rhs.id
        lhs.categoryID = rhs.categoryID
        lhs.groupID = rhs.groupID
        lhs.objID = rhs.objID
        lhs.contentID = rhs.contentID
        lhs.quantityType = rhs.quantityType
        lhs.expansionType = rhs.expansionType
        lhs.dataType = rhs.dataType
        lhs.desc = rhs.desc ?? ""
    }
}


// MARK: Remove

extension Gear6Elements {
    
    class func removeCategory(id: Int) {
        guard id > Gear6ContentType.allCases.count - 1 else { return }
        
        if let category = Gear6Elements.getCategory(id: id) {
            // Clear Template
            if let template = Gear6Elements.getTemplate(id: Int(category.template)) {
                for dataTemp in (template.data?.allObjects as! [DataBlock]) {
                    Gear6Elements.removeDataBlock(tempID: Int(template.id), dataTempID: Int(dataTemp.id))
                }
                Gear6Elements.templates = Gear6Elements.templates.filter {$0.id != category.template}
                Gear6Elements.reusableTemplateIDs.append(Int(template.id))
            }
            
            
            // Clear SubCategories
            for group in (category.subcategories?.allObjects as! [SubCategory]) {
                if let template = Gear6Elements.getTemplate(id: Int(group.template)) {
                    for dataTemp in (template.data?.allObjects as! [DataBlock]) {
                        Gear6Elements.removeDataBlock(tempID: Int(template.id), dataTempID: Int(dataTemp.id))
                    }
                    Gear6Elements.templates = Gear6Elements.templates.filter {$0.id != group.template}
                    Gear6Elements.reusableTemplateIDs.append(Int(template.id))
                }
                Gear6Elements.groupings = Gear6Elements.groupings.filter {$0.id != group.id}
                category.removeFromSubcategories(group)
                Gear6Elements.reusableGroupIDs.append(Int(group.id)) 
            }
            
            // TODO: remove quickview, page & objects
            Gear6Elements.categories = Gear6Elements.categories.filter {$0.id != id}.sorted{$0.id < $1.id}
            Gear6Elements.gear?.removeFromCategories(category)
            Gear6Elements.reusableCategoryIDs.append(id)
        }
    }
    
    class func removeSubCategory(catID: Int, groupID:Int) {
        if let category = Gear6Elements.getCategory(id: catID) {
            if let group = Gear6Elements.getGroup(catID: catID, groupID: groupID) {
                if let template = Gear6Elements.getTemplate(id: Int(group.template)) {
                    for dataTemp in (template.data?.allObjects as! [DataBlock]) {
                        Gear6Elements.removeDataBlock(tempID: Int(template.id), dataTempID: Int(dataTemp.id))
                    }
                    Gear6Elements.templates = Gear6Elements.templates.filter {$0.id != group.template}.sorted{$0.id < $1.id}
                    Gear6Elements.reusableTemplateIDs.append(Int(template.id))
                }
                
                Gear6Elements.groupings = Gear6Elements.groupings.filter {$0.id != groupID}.sorted{$0.id < $1.id}
                category.removeFromSubcategories(group)
                Gear6Elements.reusableGroupIDs.append(Int(group.id))
            }
        }
    }
    
    class func removeObject(catID: Int, objID: Int) {
        if let category = Gear6Elements.getCategory(id: catID) {
            if let obj = Gear6Elements.getObject(catID: catID, objID: objID) {
                for subCat in (obj.subcategories?.allObjects as! [SubCategory]) {
                    if (subCat.objects?.allObjects as! [Obj]).contains(obj) {
                        subCat.removeFromObjects(obj)
                    }
                }
                // TODO: remove content
                // TODO: remove data
                // TODO: remove image & icon
                Gear6Elements.objects = Gear6Elements.objects.filter { $0.id != obj.id }.sorted { $0.id < $1.id }
                category.removeFromObjects(obj)
                Gear6Elements.reusableObjIDs.append(objID)
            }
        }
    }
    class func removeObject(groupID: Int, objID: Int) {
        if let obj = Gear6Elements.getObject(groupID: groupID, objID: objID) {
            if (obj.subcategories?.allObjects.count)! > 1 {
                if let group = Gear6Elements.getGroup(catID: Int(obj.category!.id), groupID: groupID) {
                    // TODO: remove Expa content
                    obj.removeFromSubcategories(group)
                }
            }
            else {
                Gear6Elements.removeObject(catID: Int(obj.category!.id), objID: objID)
            }
        }
    }
    
    class func removeDataBlock(tempID: Int, dataTempID: Int) {
        if let template = Gear6Elements.getTemplate(id: tempID) {
            if let DataBlock = Gear6Elements.getDataBlock(id: dataTempID) {
                switch DataBlock.categoryID {
                case Int32(Gear6ContentType.Text.rawValue):
                    Gear6Elements.removeText(id: Int(DataBlock.contentID))
                case Int32(Gear6ContentType.Value.rawValue), Int32(Gear6ContentType.Range.rawValue):
                    Gear6Elements.removeValue(id: Int(DataBlock.contentID))
                case Int32(Gear6ContentType.Image.rawValue):
                    Gear6Elements.removeImage(id: Int(DataBlock.contentID))
                default: break
                }
                template.removeFromData(DataBlock)
                Gear6Elements.dataBlocks = Gear6Elements.dataBlocks.filter {$0.id != dataTempID}
                Gear6Elements.reusableDataBlockIDs.append(dataTempID)
            }
        }
    }
    
    class func removeContent(id: Int) {
        if let content = Gear6Elements.getContent(id: id) {
            for text in content.texts?.allObjects as! [Text] {
                Gear6Elements.removeText(id: Int(text.id))
            }
            for value in content.values?.allObjects as! [Value] {
                Gear6Elements.removeValue(id: Int(value.id))
            }
            for image in content.images?.allObjects as! [Image] {
                Gear6Elements.removeImage(id: Int(image.id))
            }
            Gear6Elements.contents = Gear6Elements.contents.filter { $0.id != id }
            Gear6Elements.reusableContentIDs.append(id)
        }
    }
    
    class func removeText(id: Int) {
        Gear6Elements.texts = Gear6Elements.texts.filter { $0.id != id }
        Gear6Elements.reusableTextIDs.append(id)
    }
    
    class func removeValue(id: Int) {
        Gear6Elements.values = Gear6Elements.values.filter { $0.id != id }
        Gear6Elements.reusableValueIDs.append(id)
    }
    
    class func removeImage(id: Int) {
        Gear6Elements.images = Gear6Elements.images.filter { $0.id != id }
        Gear6Elements.reusableValueIDs.append(id)
    }
    
    
    // MARK: Save to CoreData
    class func saveToCoreData() {
        // Get Stored Cat IDs
        var catIDList:[Int] = []
        let catArray = CoreDataManager<Category>.fetchData()
        for cat in catArray {
            catIDList.append(Int(cat.id))
        }
        // Remove Defaults
        var defIDList:[Int] = []
        for cat in Gear6Elements.defaultCategories {
            defIDList.append(Int(cat.id))
        }
        catIDList = catIDList.filter { !defIDList.contains($0) }
        
        // Update CoreData Items If not found then add to CoreData
        for category in Gear6Elements.categories {
            if let foundItem = CoreDataManager<Category>.fetchDataWithID(Int(category.id)).first(where: {$0.id == category.id}) {
                // Test Save of category
                catIDList = catIDList.filter { $0 != Int(foundItem.id) }
            }
        }
        
        // Delete remaining fetched Categories
        let catsToDelete = CoreDataManager<Category>.fetchDataWithIDs(catIDList)
        for category in catsToDelete {
            CoreDataManager.delete(category)
        }
        
        // Get Stored Cat IDs
        var tempArrayIDList:[Int] = []
        let tempArray = CoreDataManager<Template>.fetchData()
        for temp in tempArray {
            tempArrayIDList.append(Int(temp.id))
        }
        
        // Update Template Items
        for template in Gear6Elements.templates {
            if let foundItem = CoreDataManager<Template>.fetchDataWithID(Int(template.id)).first(where: {$0.id == template.id}) {
                tempArrayIDList = tempArrayIDList.filter { $0 != Int(foundItem.id) }
            }
        }
        
        // Delete remaining fetched Templates
        let tempsToDelete = CoreDataManager<Template>.fetchDataWithIDs(tempArrayIDList)
        for template in tempsToDelete {
            CoreDataManager.delete(template)
        }
        
        CoreDataManager.saveData()
        Gear6Elements.saveReuseIDs()
    }
}




extension Gear6Elements {
    class func getCategoryID(objID: Int) -> Int {
        // Categories: 1=Pokemon, 2=Item, 3=Location, 4=Nature
        // Groups: 1=Base, 2=Stage1, 3=Stage2, 4=Electric, 5=Fire, 20=Ball, 101=Town, 102=Route
        // Objects: 24=Pikachu, 401=Jolly, 201=LuxBall, 301=Alabasta, 202=EVUp1Item
        switch objID {
        case 24, 25:
            return 1
        case 201, 202:
            return 2
        case 301, 302:
            return 3
        case 401:
            return 4
        default: return 0
        }
    }
    class func getGroupIDs(objID: Int) -> [Int] {
        
        switch objID {
        case 24, 25:
            return [1,4]
        case 201:
            return [20]
        case 301:
            return [101]
        default:
            return []
        }
    }
    class func getStaticProperties(objID: Int) -> [(Int,Int)] {
        switch objID {
        case 24:
            return [(26, 3), (27, 8)]
        default:
            return []
        }
    }
}
