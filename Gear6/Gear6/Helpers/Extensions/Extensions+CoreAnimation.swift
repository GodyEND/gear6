//
//  Extensions+CoreAnimation.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

extension CALayer {
    func addBorder(color: UIColor) {
        self.borderWidth = 3.0
        self.borderColor = color.cgColor
    }
    
    func addShadow() {
        self.shadowOpacity = 0.9
        self.shadowRadius = 3.0
        self.shadowOffset = CGSize(width: 2, height: 3)
        // TODO: Possibly Shift shadow with layer center position
    }
    
    class func addTopDownFadeMask(frame: CGRect) -> CALayer {
        let maskLayer = CALayer()
        maskLayer.frame = frame
        maskLayer.contents = #imageLiteral(resourceName: "Gear6navBarMask").cgImage
        return maskLayer
    }
    
}
