//
//  Appearance.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

func setAppearance(window: UIWindow? = nil) {
    setNavigationBarAppearance()
}

private func setNavigationBarAppearance() {
    
    // Navigation Bar
    // Set Buttons & Title Font
    let navTitleFont = Gear6Defaults.primaryTitleFont
    
    // remove:
    let shadow = NSShadow()
    shadow.shadowColor = UIColor.black.withAlphaComponent(0.5)
    shadow.shadowOffset = CGSize(width: 2, height: 2)
    shadow.shadowBlurRadius = 5.0
    // end remove
    
    UINavigationBar.appearance().tintColor = UIColor.black
    UINavigationBar.appearance().titleTextAttributes = [
        NSAttributedStringKey.font : navTitleFont!,
        NSAttributedStringKey.foregroundColor : Gear6Defaults.primaryColor
        //NSShadowAttributeName : shadow,
    ]
    
    // Set navigation background to nil image for transparency
    UINavigationBar.appearance().shadowImage = UIImage()
    UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    UINavigationBar.appearance().tintColor = Gear6Defaults.primaryColor
    UINavigationBar.appearance().isTranslucent = true
}

