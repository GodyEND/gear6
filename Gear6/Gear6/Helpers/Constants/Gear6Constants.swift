//
//  Gear6Constants.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6Colors {
    static let black = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    static let white = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let red = UIColor(hue: 0, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let green = UIColor(hue: 118/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let blue = UIColor(hue: 224/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let cyan = UIColor(hue: 172/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let magenta = UIColor(hue: 288/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let yellow = UIColor(hue: 62/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    static let orange = UIColor(hue: 34/359, saturation: 54/99, brightness: 75/99, alpha: 1.0)
    
    // TODO: Color Function
    // TODO: Inverse Function
    static let redInverse = UIColor(hue: 352/359, saturation: 61/99, brightness: 65/99, alpha: 1.0)
}

func ColorFloat(_ value: Int) -> CGFloat {
    return CGFloat(value) / 255.0
}

class Gear6Pattern {
    static let base = UIImage(named: "Gear6BasePattern")
    static let baseBlack = UIImage(named: "Gear6BasePatternBlack")
}

class Gear6Defaults {
    static let primaryColor = Gear6Colors.redInverse
    static let primaryFont = UIFont(name: "Eurostile Extended", size: 16)
    static let primaryFontBold = UIFont(name: "EurostileExtended-Black", size: 16)
    static let primaryTitleFont = UIFont(name: "EurostileExtended-Black", size: 20)
    static var gear6Mode = Gear6Mode.User
    static let windowFrame = UIScreen.main.bounds
}

class Gear6Elements {
    static var gear: Gear? = nil
    static var categories: [Category] = []
    static var groupings: [SubCategory] = []
    static var templates: [Template] = []
    static var dataBlocks: [DataBlock] = []
    static var contents: [Content] = []
    static var texts: [Text] = []
    static var values: [Value] = []
    static var images: [Image] = []
    static var defaultCategories: [Category] = []
    
    static var infoPages: [Page] = [] // Generated Section Layout = List / Collection / Gear6Property if Value
    //static var infoViews: [Space] = [] // todo: better  definition
    static var buildTemplates: [Page] = []
    static var objects: [Obj] = []
    //static var guideGears: [GuideGear] = []
    static var properties: [Property] = []
    //static var formulas: [Formula] = []
    static var mainMenuButtons: [Gear6MainMenuButton] = []
    static var resources: [Image] = []
    // TODO: Properties user builder mode / user view mode
}

enum Gear6Mode {
    case Builder // Gear Builder Mode
    case User
}

enum Gear6DataType : Int16 {
    case None = 0
    case Category
    case Group
    case Extension
    case Object
    case Content
    case Range
}
enum Gear6MainDataBlockQuantityType : Int, CaseIterable {
    case None
    case Single
    case Multi
    case Limit
    case All
    
    var stringValue: String {
        return String("\(self)").localized(bundle: .main, tableName: nil)
    }
}

enum Gear6GroupDataBlockExtensionType : Int, CaseIterable {
    case None
    case Add
    case Remove
    case Replace
    
    var stringValue: String {
        return String("\(self)").localized(bundle: .main, tableName: nil)
    }
}

enum Gear6ContentType : Int, CaseIterable {
    case None
    case Text
    case Value
    case Range
    case Image
    
    var stringValue: String {
        return String("\(self)").localized(bundle: .main, tableName: nil)
    }
}

enum Gear6ValueRangeType {
    case Static // Objects all have the same range
    case Varying // Objects can have varying ranges
}

struct DataBlockStruct { // Mimics Managed Object
    var id: Int32 = -1
    var categoryID: Int32 = 0
    var groupID: Int32 = 0
    var objID: Int32 = 0
    var contentID: Int32 = 0
    var expansionType: Int16 = 0
    var quantityType: Int16 = 0
    var dataType: Int16 = 0
    var desc: String = ""
    
    mutating func reset() {
        self.id = -1
        self.categoryID = 0
        self.groupID = 0
        self.objID = 0
        self.contentID = 0
        self.quantityType = 0
        self.expansionType = 0
        self.dataType = 0
        self.desc = ""
    }
}

struct ObjectStruct {
    var id: Int32 = -1
    var categoryID: Int32 = 0
    var name: String = ""
    var iconURL: String = ""
    var imgURL: String = ""
//    var subCategories: [Int] = []
//    var content: [Int] = []
    
    mutating func reset() {
        self.id = -1
        self.categoryID = 0
        self.name = ""
        self.iconURL = ""
        self.imgURL = ""
//        self.subCategories = []
//        self.content = []
    }
}
