//
//  Constants.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import CoreData

// CoreDataSharedContext
class CoreData {
    class func sharedContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.persistentContainer.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        return appDelegate.persistentContainer.viewContext
    }
}

class Screen  {
    static let Top = 0.0
    static let Bottom = UIScreen.main.bounds.size.height
    static let Left = 0.0
    static let Right = UIScreen.main.bounds.size.width
    
    static let Origin = UIScreen.main.bounds.origin
    
    static let Width = UIScreen.main.bounds.width
    static let Height = UIScreen.main.bounds.height
    static let HALF_WIDTH = UIScreen.main.bounds.width / 2.0
    static let HALF_HEIGHT = UIScreen.main.bounds.height / 2.0
    static let QUARTER_WIDTH = UIScreen.main.bounds.width / 4.0
    static let QUARTER_HEIGHT = UIScreen.main.bounds.height / 4.0
    
    static let TAB_VIEW_LEFT = CGFloat(30.0)
    static let TAB_VIEW_TOP = UIScreen.main.bounds.height * 0.35
    static let TAB_VIEW_WIDTH = UIScreen.main.bounds.width - 60.0
    static let TAB_VIEW_HEIGHT = UIScreen.main.bounds.height * 0.65 - 30
    
    static let FIRST_ITEM_TOP = CGFloat(140.0)
    static let TEXTFIELD_WIDTH = UIScreen.main.bounds.width - 150.0
    static let TEXTFIELD_HEIGHT = CGFloat(50.0)
}

class Defaults {
    static let NAME_MAXIMUM_LENGTH = 16
}

enum Cell : String {
    case MapperView
    case TemplateBuilderView
    case DatabaseView
}

class Placeholder {
    static let ADD_GEAR = "Add Gear".localized(bundle: .main, tableName: nil)
    static let ADD_CAT = "Add Category".localized(bundle: .main, tableName: nil)
    static let ADD_GROUP = "Add Group".localized(bundle: .main, tableName: nil)
    static let ADD_DATA = "Add Data".localized(bundle: .main, tableName: nil)
    static let EDIT_CAT = "Edit Category".localized(bundle: .main, tableName: nil)
    static let EDIT_GROUP = "Edit Group".localized(bundle: .main, tableName: nil)
    static let ENT_OBJ_NAME = "Enter Object Name".localized(bundle: .main, tableName: nil)
    static let DESCRIPTION = "Description".localized(bundle: .main, tableName: nil)
    static let DEFAULT = "Default".localized(bundle: .main, tableName: nil)
    static let MIN = "Min".localized(bundle: .main, tableName: nil)
    static let MAX = "Max".localized(bundle: .main, tableName: nil)
    static let NONE = "None".localized(bundle: .main, tableName: nil)
}

enum Gear6View : Int {
    case Hub
    case BuilderMenu       // Build Selection Screen / Page Creator
    case LibraryMenu        // Library Selector
    case Main               // Builder / User Mode
    
    case Mapper             // Builder // Add Categories and Groups
    case TemplateBuilder    // Builder // Add Build Templates // Add Custom Group Build Templates // Add QuickView Templates
    case Database           // Builder // Add Object Names // Add image files to Objects / Add Existing Data to Objects
    case UserBuilder        // Builder // Create user properties used for the Assistant Tool / Market
    case ToolBuilder        // Builder Add new Tool // Uses Database of Collection Objects values to perform calculations
    case AssistantBuilder   // Builder // Create Assistant tool to optimize user build
    // Info Page Arranger
    
    case BuildCreator       // User // Add new Collection Object
    case BuildPage          // User // View Build
    case Library            // User View Object Info
    case Collection         // User // Present User Collection // +Feature Show missing
    case Assistant          // User // Help achieving Build
    case Market             // User // Trade Builds with other users / Arrange Times
    case Page               // User // Custom Object Page // Information Page
    case Exit
    
    var stringValue: String {
        return String("\(self)").localized(bundle: .main, tableName: nil)
    }
}


enum MapperViewState {
    case None
    case Category
    case Grouping
    case Resource
    case EditCategory
    case EditGroup
}
enum TemplateBuilderState {
    case Main // Set Main Template to active
    case Extension // Set Extension Template to active
}

enum TemplateBuilderPickerState {
    case MainTemplate
    case ExtensionTemplate
    case AddData
    case EditData
    case EditDesc
    case Content
    case Quantity
    case Expansion
}

enum DatabaseViewState {
    case AddToCategory
    case AddToGroup
    case EditObject
    case EditAssets
    case EditData
}

enum DatabasePickerState {
    case Category
    case Group
}
