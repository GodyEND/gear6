//
//  Protocols+Gear6.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import CoreMotion

protocol Gear6ViewAnimationProtocol {
    func standbyAnimation(_ delay: TimeInterval)
    func showAnimation(_ delay: TimeInterval, completion:((Bool)->Void)?)
    func clearAnimation(_ delay: TimeInterval, completion: @escaping (Bool)->Void)
}

extension Gear6ViewAnimationProtocol where Self : Gear6PropertyView {
    func updateShadow(source: CGSize) {
        let screenWidth = (self.window?.frame.size.width)! * 0.5
        let screenHeight = (self.window?.frame.size.height)! * 0.5
        let shadowDisplacementMAX = self.frame.size.width * 0.5
        let sourceDisplacementMAX:CGFloat = sqrt(screenWidth * screenWidth +
            screenHeight * screenHeight)
        let maxDistance = sourceDisplacementMAX * 2.0
        
        let sourcePosition =
            CGSize(width: screenWidth + sourceDisplacementMAX * (source.width / CGFloat(Double.pi)),
                  height: screenHeight + sourceDisplacementMAX * (source.height / CGFloat(Double.pi)))
        
        let distX:CGFloat = self.center.x - sourcePosition.width
        let distY:CGFloat = self.center.y - sourcePosition.height
        let distance:CGFloat = sqrt(distX*distX + distY*distY)
        
        let shadowStrength = 1.0 - (distance / maxDistance)
        let shadowDisplacementX = shadowDisplacementMAX * (distX / maxDistance)
        let shadowDisplacementY = shadowDisplacementMAX * (distY / maxDistance)
        
        self.layer.shadowOffset = CGSize(width: shadowDisplacementX, height: shadowDisplacementY)
        self.layer.shadowOpacity = Float(shadowStrength)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.opacity = 0.8
    }
    
    func standbyAnimation() {
        // This is an empty implementation to allow optional use
    }
}

protocol Gear6ViewControllerProtocol {
    func setupView()
    func presentScreen(completion: @escaping (Bool) -> Void)
    func clearScreen(completion: ((Bool) -> Void)?)
}

protocol Gear6CollectionViewDelegate: class {
    
}

protocol Gear6CollectionViewDataSource: class {
    func cellForRow() -> Gear6CollectionCell
}

protocol TempCellDelegate {
    func addNewDataBlock()
    func editCategory()
    func descPickerPress()
    func rangePickerPress()
    func quantityPickerPress()
    func extTypePickerPress()
}

protocol DatabaseCellDelegate {
    func addNewObject()
    func editObject()
    func editIcon()
    func editData()
}
