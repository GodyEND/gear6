//
//  AssistantClasses.swift
//  Gear6
//
//  Created by Gody on 02/11/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

struct Guide {
    var id: Int?
    var name: String?
    var tasks: [Task]?
    var taskProcesses: [TaskProcess]?
    
    var goal: CollectableObject?                // Collectable Object obtained after completing this guide
    var session: CollectableObject?             // Modifiable Collectable Object used to create goal
    var milestoneObjects: [CollectableObject]?  // Current Target Collectable Object
    var arsenal: [CollectableObject]?           // User Collectable Objects that can be used to complete guid in less steps
    var requirements: [CollectableObject]?      // Missing Collectable Objects that are needed to complete this guide
    
    var goalPath: Path?                         // Sequence of tasks performed to obtain goal
    var currentPath: Path?                      // Currently completed tasks
    
    var currentProgress: Int?                   // Percentage of guide completion
    // 1 Get Input Requirements & add to pending
    // 2 Match to Startpoint if possible and remove from pending
    // 3 Repeat until pending is empty
    // 4 Couple Method
    // 5 set expected output
}

struct Task {
    
}

struct TaskProcess {
    
}

func guideGenerator() {
    
}

func pathGenerator() {
    
}




// Gear6Objects
struct CollectableObject {
    var fields: [CollectableField]!
    var objectType: ObjectType!
    
    init(objectID: Int) {
        // if object ID = 0
        self.objectType = ObjectType()
        // else
        self.objectType = Assistant.getObjectType(id: objectID)
        
        self.fields = []
    }
}

struct CollectableField { // Can be Random or Set
    var id: Int?
    var objectType: ObjectType!
    var value: Int?
    var requirement: FieldRequirement? // Any, Equal, Less , Greater, GreaterEqual, LessEqual
}



//struct CollectableProperty {
//    var id: Int?
//    var objectType: ObjectType!
//    var value: Int?
//
//    init(id: Int? = nil, objectID: Int, value: Int? = nil) {
//        self.objectType = Assistant.getObjectType(id: objectID)
//    }
//}

struct ObjectType: Equatable {
    var categoryID: Int?
    var groupIDs: [Int]? // use groups as filter tags
    var objectID: Int?
}

struct Path {
    var id: Int?
    var path: [PathNode]?
}

class PathNode {
    var id: Int?
    var method: Task?
    var parent: PathNode?
    var favChild: PathNode?
    var optionalChild: [PathNode]?
    init () {
        
    }
}


enum TaskExecutionType {
    case addition
    case subtraction
    case division
    case multiplication
    case setProperty
    case removeProperty
    case replaceProperty
    case selectProperty
    case randomizeProperty
}

enum FieldRequirement {
    case AnyValue
    case Equal
    case Less
    case Greater
    case LessEqual
    case GreaterEqual
}

