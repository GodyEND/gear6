//
//  Gear6DatabaseView+AssetLoader.swift
//  Gear6
//
//  Created by Gody on 22/02/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import UIKit

class Gear6DatabaseAssetLoader : UIView {
    private let objectTitleLabel = UILabel()
    private let infoLabel = UILabel() // Displays: Will look for <ObjectName>
    private let iconView = UIImageView() // On tap will upload image file // 48 x 48px
    private let mainImageView = UIImageView() // On tap will upload image file
    let notationField = UITextField() // On tap will select filepath <ObjectName>Notation
    private let batchUploadSwitch = UISwitch()
    private let iconPicker = UIImagePickerController()
    private let imagePicker = UIImagePickerController()
    
    var isBatchUpload = false
    var iconURL: String = ""
    var imageURL: String = ""
    var objectTitle: String = "" {
        didSet {
            self.objectTitleLabel.text = self.objectTitle
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect(x: Screen.Width * 0.1, y: Screen.FIRST_ITEM_TOP+(3*Screen.TEXTFIELD_HEIGHT), width: Screen.Width * 0.8, height: 248.2))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    private func setupView() {
        self.prepareForReuse()
        self.addSubview(self.objectTitleLabel)
        self.addSubview(self.infoLabel)
        self.addSubview(self.notationField)
        self.addSubview(self.batchUploadSwitch)
        
        self.iconView.isUserInteractionEnabled = true
        self.mainImageView.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.cyan
        self.notationField.backgroundColor = UIColor.white
        
        let iconTapGesture = UITapGestureRecognizer(target: self, action: #selector(iconTap))
        self.iconView.addGestureRecognizer(iconTapGesture)
        self.iconView.backgroundColor = UIColor.orange
        self.addSubview(self.iconView)
        
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(mainImageTap))
        self.mainImageView.addGestureRecognizer(imageTapGesture)
        self.mainImageView.backgroundColor = UIColor.green
        self.addSubview(self.mainImageView)
        
        self.iconPicker.delegate = self
        self.iconPicker.allowsEditing = false
        self.iconPicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        
        self.objectTitleLabel.textAlignment = .center
        self.infoLabel.textAlignment = .center
        self.iconView.contentMode = .scaleAspectFit
        self.mainImageView.contentMode = .scaleAspectFit
        
        // TODO: Unhide when plans for usage
        self.infoLabel.isHidden = true
        self.notationField.isHidden = true
        self.setupAutoLayoutConstraints()
    }
    
    func prepareForReuse() {
        self.objectTitle = ""
        self.infoLabel.text = "Will look for <ObjectName> in FilePath" // TODO: Should FilePath be set or search entire device?
        self.iconView.image = nil // TODO: Placeholder Image
        self.mainImageView.image = nil
        self.notationField.text = ""
        self.isBatchUpload = false // UserDefaults if nil then false
        self.iconURL = ""
        self.imageURL = ""
    }
    
    private func setupAutoLayoutConstraints() {
        self.objectTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.mainImageView.translatesAutoresizingMaskIntoConstraints = false
        self.iconView.translatesAutoresizingMaskIntoConstraints = false
        self.infoLabel.translatesAutoresizingMaskIntoConstraints = false
        self.notationField.translatesAutoresizingMaskIntoConstraints = false
        self.batchUploadSwitch.translatesAutoresizingMaskIntoConstraints = false
        
        self.objectTitleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5.0).isActive = true
        self.objectTitleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5.0).isActive = true
        self.objectTitleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        self.objectTitleLabel.heightAnchor.constraint(equalToConstant: Screen.TEXTFIELD_HEIGHT).isActive = true
        
        self.mainImageView.topAnchor.constraint(equalTo: self.objectTitleLabel.bottomAnchor, constant: 0.0).isActive = true
        self.mainImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5.0).isActive = true
        let width = self.frame.size.width * 2.0 / 3.0
        self.mainImageView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.mainImageView.heightAnchor.constraint(equalToConstant: width * 9.0 / 16.0).isActive = true
        
        self.iconView.topAnchor.constraint(equalTo: self.objectTitleLabel.bottomAnchor, constant: 0.0).isActive = true
        self.iconView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        self.iconView.widthAnchor.constraint(equalToConstant: 48.0).isActive = true
        self.iconView.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.batchUploadSwitch.topAnchor.constraint(equalTo: self.iconView.bottomAnchor, constant: 5.0).isActive = true
        self.batchUploadSwitch.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        self.batchUploadSwitch.widthAnchor.constraint(equalToConstant: 48.0).isActive = true
        self.batchUploadSwitch.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        
        self.infoLabel.topAnchor.constraint(equalTo: self.mainImageView.bottomAnchor, constant: 0.0).isActive = true
        self.infoLabel.topAnchor.constraint(equalTo: self.batchUploadSwitch.bottomAnchor, constant: 0.0).isActive = true
        self.infoLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5.0).isActive = true
        self.infoLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        self.infoLabel.heightAnchor.constraint(equalToConstant: Screen.TEXTFIELD_HEIGHT).isActive = true
        
        self.notationField.topAnchor.constraint(equalTo: self.infoLabel.bottomAnchor, constant: 0.0).isActive = true
        self.notationField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5.0).isActive = true
        self.notationField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5.0).isActive = true
        self.notationField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5.0).isActive = true
        self.notationField.heightAnchor.constraint(equalToConstant: Screen.TEXTFIELD_HEIGHT).isActive = true
        self.updateConstraints()
    }
}

@objc extension Gear6DatabaseAssetLoader {
    private func iconTap() {
        Gear6NavigationController.shared.present(iconPicker, animated: true, completion:  nil)
    }
    private func mainImageTap() {
        Gear6NavigationController.shared.present(imagePicker, animated: true, completion:  nil)
    }
}

extension Gear6DatabaseAssetLoader: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let fileManager = FileManager.default
        let paths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        var path = paths[0]
        path.appendPathComponent("gear6")
        path.appendPathComponent("builder")
        path.appendPathComponent("mainImages")
        if let url = info[UIImagePickerControllerImageURL] as? URL {
            if url.pathComponents.contains(self.objectTitle) { // && USERDEFAULTS Iconloading has no images of component notation show alert}
            }
        }
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if (picker == self.iconPicker) {
                // TODO UserDefaults store locally
                if !fileManager.fileExists(atPath: path.path) {
                    do {
                    try fileManager.createDirectory(atPath: path.path, withIntermediateDirectories: true, attributes: nil)
                    }
                    catch {
                        print("Failed to create Directory at: "+path.path)
                    }
                }
                path.appendPathComponent(self.objectTitle+"_ICON")
                path.appendPathExtension("png")

                // Save Image Reference
                let newImage = self.getScaledImage(image: image, rect: CGRect(x: 0.0, y: 0.0, width: 48.0, height: 48.0))
                let data = UIImagePNGRepresentation(newImage)!
                do {
                    try data.write(to: path, options: .atomic)
                }
                catch {
                    print("Failed to write to path \(path)")
                }
                self.iconView.image = newImage
            }
            else {
                path.appendPathComponent(self.objectTitle+"_MAIN")
                path.appendPathExtension("png")

                let newImage = self.getScaledImage(image: image, rect: CGRect(x: 0.0, y: 0.0, width: 500.0, height: 500.0))
                let data = UIImagePNGRepresentation(newImage)!
                do {
                    try data.write(to: path, options: .atomic)
                }
                catch {
                    print("Failed to write to path \(path)")
                }
                self.mainImageView.image = newImage
            }
        }
        Gear6NavigationController.shared.dismiss(animated: true, completion: nil)
    }
    
    func getScaledImage(image: UIImage, rect: CGRect) -> UIImage {
        let cgImage = image.cgImage!
        let context = CGContext(data: nil, width: Int(rect.size.width), height: Int(rect.size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: cgImage.bytesPerRow, space: cgImage.colorSpace!, bitmapInfo: cgImage.bitmapInfo.rawValue)
        context?.interpolationQuality = .high
        context?.draw(cgImage, in: rect)
        return context!.makeImage().flatMap { UIImage(cgImage: $0) }!
    }
}
