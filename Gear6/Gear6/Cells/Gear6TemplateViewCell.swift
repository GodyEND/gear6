//
//  Gear6TemplateViewCell.swift
//  Gear6
//
//  Created by Gody on 29/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6TemplateViewCell: UITableViewCell {
    
    var dataBtn = UIButton()
    var descBtn = UIButton() // Image / Value / Text Description Name
    var rangeBtn = UIButton() // ValueRange desc
    var quantityBtn = UIButton()
    var extTypeBtn = UIButton() // Used for Group Template
    // Data Storage
    var dataTempStorage = DataBlockStruct()
    // Delegation
    var pickerDelegate: TempCellDelegate!
    
    // Dynamic Width Constraints
    var dataBtnWidth: CGFloat = 0.0
    var descBtnWidth: CGFloat = 0.0
    var rangeBtnWidth: CGFloat = 0.0
    var quantityBtnWidth: CGFloat = 0.0
    var extTypeBtnWidth: CGFloat = 0.0
    var dataConstraint: NSLayoutConstraint!
    var descConstraint: NSLayoutConstraint!
    var rangeConstraint: NSLayoutConstraint!
    var quantConstraint: NSLayoutConstraint!
    var extConstraint: NSLayoutConstraint!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.dataBtn.addTarget(self, action: #selector(dataAction), for: .touchUpInside)
        self.dataBtn.backgroundColor = UIColor.lightGray
        self.contentView.addSubview(self.dataBtn)
        
        self.descBtn.addTarget(self, action: #selector(descAction), for: .touchUpInside)
        self.descBtn.backgroundColor = UIColor.red
        self.contentView.addSubview(self.descBtn)
        
        self.rangeBtn.addTarget(self, action: #selector(rangeAction), for: .touchUpInside)
        self.rangeBtn.backgroundColor = UIColor.green
        self.contentView.addSubview(self.rangeBtn)
       
        self.quantityBtn.addTarget(self, action: #selector(quantityAction), for: .touchUpInside)
        self.quantityBtn.backgroundColor = UIColor.blue
        self.contentView.addSubview(self.quantityBtn)
        
        self.extTypeBtn.addTarget(self, action: #selector(extTypeAction), for: .touchUpInside)
        self.extTypeBtn.backgroundColor = UIColor.darkGray
        self.contentView.addSubview(self.extTypeBtn)
        
        self.setupAutoLayoutConstraints()
        self.prepareForReuse()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    func setupCell(dataType: DataBlock?) {
        
        var quantityString = Placeholder.NONE
        var extensionString = Placeholder.NONE
        var categoryString = Placeholder.ADD_DATA
        var descString = ""
        var rangeString = ""
        
        if let source = dataType {
            Gear6Elements.updateDataBlockStruct(lhs: &self.dataTempStorage, with: source)
            
            switch (Int(source.quantityType)) {
            case Gear6MainDataBlockQuantityType.Single.rawValue:
                quantityString = Gear6MainDataBlockQuantityType.Single.stringValue
            case Gear6MainDataBlockQuantityType.Limit.rawValue:
                quantityString = Gear6MainDataBlockQuantityType.Limit.stringValue
            case Gear6MainDataBlockQuantityType.Multi.rawValue:
                quantityString = Gear6MainDataBlockQuantityType.Multi.stringValue
            case Gear6MainDataBlockQuantityType.All.rawValue:
                quantityString = Gear6MainDataBlockQuantityType.All.stringValue
            default: break
            }
            
            switch (Int(source.expansionType)) {
            case Gear6GroupDataBlockExtensionType.Add.rawValue:
                extensionString = Gear6GroupDataBlockExtensionType.Add.stringValue
            case Gear6GroupDataBlockExtensionType.Remove.rawValue:
                extensionString = Gear6GroupDataBlockExtensionType.Remove.stringValue
            case Gear6GroupDataBlockExtensionType.Replace.rawValue:
                extensionString = Gear6GroupDataBlockExtensionType.Replace.stringValue
            default: break
            }
            
            if (source.objID > 0) {
                categoryString = (Gear6Elements.getCategory(id: Int(source.categoryID))?.name)!
                descString = (Gear6Elements.getObject(catID: Int(source.categoryID), objID: Int(source.objID))?.name)!
            }
            else if (source.groupID > 0) {
                // TODO: half font size
                categoryString = (Gear6Elements.getCategory(id: Int(source.categoryID))?.name)!
                categoryString.append(": ")
                categoryString.append((Gear6Elements.getGroup(catID: Int(source.categoryID), groupID: Int(source.groupID))?.name)!)
            }
            else if (source.categoryID > Gear6ContentType.allCases.count-1) {
                categoryString = (Gear6Elements.getCategory(id: Int(source.categoryID))?.name)!
            }
            else if (source.categoryID < Gear6ContentType.allCases.count) {
                switch (Int(source.categoryID)) {
                case Gear6ContentType.Text.rawValue:
                    categoryString = Gear6ContentType.Text.stringValue
                    if let text = Gear6Elements.getText(id: Int(source.contentID)) {
                        descString = text.desc ?? ""
                    }
                case Gear6ContentType.Value.rawValue:
                    categoryString = Gear6ContentType.Value.stringValue
                    if let value = Gear6Elements.getValue(id: Int(source.contentID)) {
                        rangeString = String(value.value)
                        descString = value.desc ?? ""
                    }
                case Gear6ContentType.Range.rawValue:
                    categoryString = Gear6ContentType.Range.stringValue
                    if let value = Gear6Elements.getValue(id: Int(source.contentID)) {
                        rangeString = "\(value.value) - \(value.maxValue)"
                        descString = value.desc ?? ""
                    }
                case Gear6ContentType.Image.rawValue:
                    categoryString = Gear6ContentType.Image.stringValue
                    if let image = Gear6Elements.getImage(id: Int(source.contentID)) {
                        descString = image.desc ?? ""
                    }
                default: break
                }
            }
            
            // Show Buttons
            self.dataBtn.isHidden = false
            self.quantityBtn.isHidden = false
            self.descBtn.isUserInteractionEnabled = true
            switch (source.dataType) {
            case Gear6DataType.Content.rawValue:
                self.descBtn.isHidden = false
            case Gear6DataType.Range.rawValue:
                self.descBtn.isHidden = false
                self.rangeBtn.isHidden = false
            case Gear6DataType.Extension.rawValue:
                if source.categoryID < Gear6ContentType.allCases.count {
                    self.descBtn.isHidden = false
                    if source.categoryID == Gear6ContentType.Range.rawValue {
                        self.rangeBtn.isHidden = false
                    }
                }
                self.extTypeBtn.isHidden = false
            case Gear6DataType.Object.rawValue:
                self.descBtn.isHidden = false
                self.descBtn.isUserInteractionEnabled = false
            default: break
            }
        }
        else {
            self.dataBtn.isHidden = false
        }
        
        self.dataBtn.setTitle(categoryString, for: .normal)
        self.descBtn.setTitle(descString, for: .normal)
        self.rangeBtn.setTitle(rangeString, for: .normal)
        self.quantityBtn.setTitle(quantityString, for: .normal)
        self.extTypeBtn.setTitle(extensionString, for: .normal)
        
        // Calculate Autolayout Width Constraints
        self.quantityBtnWidth = (self.quantityBtn.isHidden) ? 0.0 : 64.0
        self.extTypeBtnWidth = (self.extTypeBtn.isHidden) ? 0.0 : 80.0
        
        if (self.descBtn.isHidden == false || self.rangeBtn.isHidden == false) {
            if let font = self.descBtn.titleLabel?.font {
                self.dataBtnWidth = categoryString.size(withAttributes: [.font: font]).width + 20.0
                
                if self.rangeBtn.isHidden == false {
                    self.rangeBtnWidth = rangeString.size(withAttributes: [.font: font]).width
                    let maxDescWidth = Screen.TAB_VIEW_WIDTH - (self.dataBtnWidth + self.quantityBtnWidth + self.extTypeBtnWidth) - 50.0
                    if (self.rangeBtnWidth > 64.0 && self.rangeBtnWidth < maxDescWidth) {
                        self.rangeBtnWidth += 20
                    }
                    else if self.rangeBtnWidth > maxDescWidth {
                        self.rangeBtnWidth = maxDescWidth
                    }
                    else {
                        self.rangeBtnWidth = 64.0
                    }
                }
                if self.descBtn.isHidden == false {
                    if self.rangeBtn.isHidden {
                        self.rangeBtnWidth = 0.0
                    }
                    if self.rangeBtn.isHidden && self.extTypeBtn.isHidden {
                        self.dataBtnWidth = (Screen.TAB_VIEW_WIDTH - 30.0 - self.quantityBtnWidth) / 2.0
                        self.descBtnWidth = dataBtnWidth
                    }
                    else {
                        let edgeWidth = self.dataBtnWidth + self.quantityBtnWidth + self.extTypeBtnWidth
                        self.descBtnWidth = Screen.TAB_VIEW_WIDTH - edgeWidth - self.rangeBtnWidth - 30.0
                        if self.descBtnWidth < 64.0 {
                            self.descBtnWidth = 64.0 // TODO: Fix Button Layout
                        }
                    }
                }
            }
        }
        else {
            self.descBtnWidth = 0.0
            self.rangeBtnWidth = 0.0
            self.dataBtnWidth = Screen.TAB_VIEW_WIDTH - 30.0 - self.quantityBtnWidth - self.extTypeBtnWidth
        }
        
        // Update Btn Widths
        self.dataConstraint.constant = self.dataBtnWidth
        self.descConstraint.constant = self.descBtnWidth
        self.rangeConstraint.constant = self.rangeBtnWidth
        self.quantConstraint.constant = self.quantityBtnWidth
        self.extConstraint.constant = self.extTypeBtnWidth
        self.updateConstraints()
    }
    
    override func prepareForReuse() {
        self.dataBtn.setTitle(Placeholder.ADD_DATA, for: .normal)
        self.dataBtn.titleLabel!.textAlignment = .center
        self.dataBtn.isHidden = true
        self.descBtn.setTitle("", for: .normal)
        self.descBtn.isHidden = true
        self.descBtn.isUserInteractionEnabled = false
        self.quantityBtn.setTitle("", for: .normal)
        self.quantityBtn.isHidden = true
        self.rangeBtn.setTitle("", for: .normal)
        self.rangeBtn.isHidden = true
        self.extTypeBtn.setTitle("", for: .normal)
        self.extTypeBtn.isHidden = true
        self.dataTempStorage.reset()
    }
}

@objc extension Gear6TemplateViewCell {
    
    func dataAction() {
        if (self.dataTempStorage.objID > 0) ||
            (self.dataTempStorage.groupID > 0) ||
            (self.dataTempStorage.categoryID > 0) {
            Gear6TemplateBuilderViewController.shared.currentEditedDatatype.id = Int32(self.dataTempStorage.id)
            self.pickerDelegate.editCategory()
        }
        else {
            self.pickerDelegate.addNewDataBlock()
        }
    }
    
    func descAction() {
        Gear6TemplateBuilderViewController.shared.currentEditedDatatype.id = Int32(self.dataTempStorage.id)
        self.pickerDelegate.descPickerPress()
    }
    
    func rangeAction() {
        Gear6TemplateBuilderViewController.shared.currentEditedDatatype.id = Int32(self.dataTempStorage.id)
        self.pickerDelegate.rangePickerPress()
    }
    
    func quantityAction() {
        Gear6TemplateBuilderViewController.shared.currentEditedDatatype.id = Int32(self.dataTempStorage.id)
        self.pickerDelegate.quantityPickerPress()
    }
    
    func extTypeAction() {
        Gear6TemplateBuilderViewController.shared.currentEditedDatatype.id = Int32(self.dataTempStorage.id)
        self.pickerDelegate.extTypePickerPress()
    }
}

extension Gear6TemplateViewCell {
    func setupAutoLayoutConstraints() {
        self.dataBtn.translatesAutoresizingMaskIntoConstraints = false
        self.descBtn.translatesAutoresizingMaskIntoConstraints = false
        self.rangeBtn.translatesAutoresizingMaskIntoConstraints = false
        self.quantityBtn.translatesAutoresizingMaskIntoConstraints = false
        self.extTypeBtn.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.leadingAnchor.constraint(equalTo: self.dataBtn.leadingAnchor, constant: -15.0).isActive = true
        self.dataBtn.trailingAnchor.constraint(equalTo: self.descBtn.leadingAnchor, constant: 0.0).isActive = true
        self.descBtn.trailingAnchor.constraint(equalTo: self.rangeBtn.leadingAnchor, constant: 0.0).isActive = true
        self.rangeBtn.trailingAnchor.constraint(equalTo: self.quantityBtn.leadingAnchor, constant: 0.0).isActive = true
        self.quantityBtn.trailingAnchor.constraint(equalTo: self.extTypeBtn.leadingAnchor, constant: 0.0).isActive = true
        self.contentView.trailingAnchor.constraint(equalTo: self.extTypeBtn.trailingAnchor, constant: 15.0).isActive = true

        self.dataConstraint = self.dataBtn.widthAnchor.constraint(equalToConstant: self.dataBtnWidth)
        self.descConstraint = self.descBtn.widthAnchor.constraint(equalToConstant: self.descBtnWidth)
        self.rangeConstraint = self.rangeBtn.widthAnchor.constraint(equalToConstant: self.rangeBtnWidth)
        self.quantConstraint = self.quantityBtn.widthAnchor.constraint(equalToConstant: self.quantityBtnWidth)
        self.extConstraint = self.extTypeBtn.widthAnchor.constraint(equalToConstant: self.extTypeBtnWidth)
        self.dataBtn.addConstraint(self.dataConstraint)
        self.descBtn.addConstraint(self.descConstraint)
        self.rangeBtn.addConstraint(self.rangeConstraint)
        self.quantityBtn.addConstraint(self.quantConstraint)
        self.extTypeBtn.addConstraint(self.extConstraint)
        
        self.contentView.topAnchor.constraint(equalTo: self.dataBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.descBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.rangeBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.quantityBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.extTypeBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.dataBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.descBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.rangeBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.quantityBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.extTypeBtn.bottomAnchor, constant: 0.0).isActive = true
    }
}
