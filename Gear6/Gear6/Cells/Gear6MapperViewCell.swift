//
//  Gear6MapperViewCell.swift
//  Gear6
//
//  Created by Gody on 28/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6MapperViewCell : UITableViewCell {
    
    var titleLabel = UILabel()
    var editTitleBtn = UIButton()
    var groupBtn = UIButton()
    var resourceBtn = UIButton()
    var deleteBtn = UIButton()
    var actionID: Int = 0
    var cellType = MapperViewState.None
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let width = Screen.TAB_VIEW_WIDTH
        self.titleLabel.frame = CGRect(x: 0.25 * width, y: 0, width: 0.5 * width, height: self.frame.size.height)
        self.titleLabel.textAlignment = .center
        self.contentView.addSubview(self.titleLabel)
        
        self.editTitleBtn.frame = self.titleLabel.frame
        self.editTitleBtn.addTarget(self, action: #selector(editTitleAction), for: .touchUpInside)
        self.contentView.addSubview(self.editTitleBtn)
        
        self.groupBtn.backgroundColor = UIColor.blue
        self.groupBtn.frame = CGRect(x: 0.05 * width, y: 0, width: 0.2 * width, height: self.frame.size.height)
        self.groupBtn.addTarget(self, action: #selector(addGroupAction), for: .touchUpInside)
        self.contentView.addSubview(self.groupBtn)
        
        self.resourceBtn.backgroundColor = UIColor.blue
        self.resourceBtn.frame = groupBtn.frame
        self.resourceBtn.addTarget(self, action: #selector(addResourcesAction), for: .touchUpInside)
        self.contentView.addSubview(self.resourceBtn)
        
        self.deleteBtn.backgroundColor = UIColor.red
        // set btn colour
        // set btn icon
        self.deleteBtn.frame = CGRect(x: 0.75 * width, y: 0, width: 0.2 * width, height: self.frame.size.height)
        deleteBtn.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        self.contentView.addSubview(self.deleteBtn)
        
        self.prepareForReuse()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    func setupCell(category: Category?, group: SubCategory?, state: MapperViewState) {
        self.cellType = state
        switch (state) {
        case .Category:
            self.titleLabel.text = category!.name
            self.actionID = Int(category!.id)
            self.groupBtn.isHidden = false
            self.groupBtn.isUserInteractionEnabled = true
        case .Grouping:
            self.titleLabel.text = group!.name
            self.actionID = Int(group!.id)
            self.resourceBtn.isHidden = false
            self.resourceBtn.isUserInteractionEnabled = true
        default: break
        }
    }
    
    override func prepareForReuse() {
        self.titleLabel.text = ""
        self.groupBtn.isHidden = true
        self.groupBtn.isUserInteractionEnabled = false
        self.resourceBtn.isHidden = true
        self.resourceBtn.isUserInteractionEnabled = false
        self.actionID = 0
        self.cellType = .None
    }
}

@objc extension Gear6MapperViewCell {
    
    func addGroupAction() {
        Gear6MapperViewController.shared.viewState = MapperViewState.Grouping
        Gear6MapperViewController.shared.currentCategory = actionID
        Gear6MapperViewController.shared.update()
    }
    
    func addResourcesAction() {
        Gear6MapperViewController.shared.viewState = MapperViewState.Resource
        Gear6MapperViewController.shared.currentGroup = actionID
        Gear6MapperViewController.shared.update()
    }
    
    func editTitleAction() {
        switch (self.cellType) {
        case .Category:
            Gear6MapperViewController.shared.viewState = MapperViewState.EditCategory
            Gear6MapperViewController.shared.editID = actionID
            Gear6MapperViewController.shared.update()
        case .Grouping:
            Gear6MapperViewController.shared.viewState = MapperViewState.EditGroup
            Gear6MapperViewController.shared.editID = actionID
            Gear6MapperViewController.shared.update()
        default: break
        }
    }
    
    func deleteAction() {
        switch (self.cellType) {
        case .Category:
            Gear6Elements.removeCategory(id:actionID)
        case .Grouping:
            Gear6Elements.removeSubCategory(catID: Gear6MapperViewController.shared.currentCategory, groupID: actionID)
        default: break
        }
        
        switch (Gear6MapperViewController.shared.viewState) {
        case .EditCategory: Gear6MapperViewController.shared.viewState = MapperViewState.Category
        case .EditGroup: Gear6MapperViewController.shared.viewState = MapperViewState.Grouping
        default: break
        }
        Gear6MapperViewController.shared.update()
    }
}
