//
//  Gear6DatabaseViewCell.swift
//  Gear6
//
//  Created by Gody on 17/02/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import UIKit

class Gear6DatabaseViewCell: UITableViewCell {
    
    var objBtn = UIButton()
    var iconBtn = UIButton()
    var dataBtn = UIButton()
    var actionID:Int = 0
    var cellDelegate: DatabaseCellDelegate!
    
    // Width Constraints
    var objBtnWidth: CGFloat = 0.0
    var iconBtnWidth: CGFloat = 0.0
    var dataBtnWidth: CGFloat = 0.0
    var objConstraint: NSLayoutConstraint!
    var iconConstraint: NSLayoutConstraint!
    var dataConstraint: NSLayoutConstraint!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.objBtn.addTarget(self, action: #selector(editObjAction), for: .touchUpInside)
        self.objBtn.backgroundColor = UIColor.green
        self.contentView.addSubview(self.objBtn)
        self.iconBtn.addTarget(self, action: #selector(editIconAction), for: .touchUpInside)
        self.iconBtn.backgroundColor = UIColor.yellow
        self.contentView.addSubview(self.iconBtn)
        self.dataBtn.addTarget(self, action: #selector(editDataAction), for: .touchUpInside)
        self.dataBtn.backgroundColor = UIColor.orange
        self.contentView.addSubview(self.dataBtn)
        
        self.setupAutoLayoutConstraints()
        self.updateConstraints()
        self.prepareForReuse()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    func setupCell(name: String, id: Int) {
        self.objBtn.setTitle(name, for: .normal)
        self.actionID = id
    }
    
    override func prepareForReuse() {
        self.objBtn.setTitle("", for: .normal)
        self.objBtn.titleLabel!.textAlignment = .center
        self.iconBtn.setTitle("image", for: .normal)
        self.iconBtn.titleLabel!.textAlignment = .center
        self.dataBtn.setTitle("data", for: .normal)
        self.dataBtn.titleLabel!.textAlignment = .center
    }
    
    func setupAutoLayoutConstraints() {
        self.objBtnWidth = (Screen.TAB_VIEW_WIDTH - 30.0 - 64.0) * 2 / 3
        self.dataBtnWidth = (Screen.TAB_VIEW_WIDTH - 30.0 - 64.0) / 3
        self.iconBtnWidth = 64.0
        
        self.objBtn.translatesAutoresizingMaskIntoConstraints = false
        self.iconBtn.translatesAutoresizingMaskIntoConstraints = false
        self.dataBtn.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.leadingAnchor.constraint(equalTo: self.objBtn.leadingAnchor, constant: -15).isActive = true
        self.contentView.trailingAnchor.constraint(equalTo:self.dataBtn.trailingAnchor, constant: 15).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.objBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.objBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.iconBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.iconBtn.bottomAnchor, constant: 0.0).isActive = true
        self.contentView.topAnchor.constraint(equalTo: self.dataBtn.topAnchor, constant: 0.0).isActive = true
        self.contentView.bottomAnchor.constraint(equalTo: self.dataBtn.bottomAnchor, constant: 0.0).isActive = true
        self.objBtn.trailingAnchor.constraint(equalTo: self.iconBtn.leadingAnchor, constant: 0.0).isActive = true
        self.iconBtn.trailingAnchor.constraint(equalTo: self.dataBtn.leadingAnchor, constant: 0.0).isActive = true
        
        self.objConstraint = self.objBtn.widthAnchor.constraint(equalToConstant: self.objBtnWidth)
        self.objBtn.addConstraint(self.objConstraint)
        self.iconConstraint = self.iconBtn.widthAnchor.constraint(equalToConstant: self.iconBtnWidth)
        self.iconBtn.addConstraint(self.iconConstraint)
        self.dataConstraint = self.dataBtn.widthAnchor.constraint(equalToConstant: self.dataBtnWidth)
        self.dataBtn.addConstraint(self.dataConstraint)
    }
}

@objc extension Gear6DatabaseViewCell {
    func editObjAction() {
        Gear6DatabaseViewController.shared.currentEditedObj.reset()
        Gear6DatabaseViewController.shared.currentEditedObj.id = Int32(self.actionID)
        self.cellDelegate.editObject()
    }
    func editIconAction() {
        Gear6DatabaseViewController.shared.currentEditedObj.reset()
        Gear6DatabaseViewController.shared.currentEditedObj.id = Int32(self.actionID)
        self.cellDelegate.editIcon()
    }
    func editDataAction() {
        Gear6DatabaseViewController.shared.currentEditedObj.reset()
        Gear6DatabaseViewController.shared.currentEditedObj.id = Int32(self.actionID)
        self.cellDelegate.editData()
    }
}
