//
//  NetworkManager.swift
//  Gear6
//
//  Created by Gody on 04/11/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import AVFoundation

// MARK: - HTTP Method enumeration
public enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case PATCH
    case DELETE
}

// MARK: - MIME Media Type enumeration
public enum HTTPMediaType: String {
    case JSON = "application/json"
}

// MARK - HTTP Request Header Fields Enum
enum RequestHeader: String {
    case Authorization = "Authorization"
    case ContentType = "Content-Type"
    //case IfModifiedSince = "If-Modified-Since"
}

public enum HTTPRequestType: String {
    case Default
    case JSON
    case Image
    case Audio
}


class NetworkManager : NSObject {
    static var cachedImages = Dictionary<String, UIImage>() // TODO: load from phone storage
    static var cachedAudio = Dictionary<String, UIImage>()
    
    class func get(url: String, postData: Dictionary<String, AnyObject>?, completion: @escaping ([String : Any]?) -> ()) {
        NetworkManager.getRequest(url: url, requestType: HTTPRequestType.Default, postData: postData, completion: { json in
            completion(json as! [String : Any]?)
        })
    }
    
    class func getImage(url: String, completion: @escaping (UIImage?) -> ()) {
        if let image = cachedImages[url] {
            completion(image)
        }
        else {
            NetworkManager.getRequest(url: url, requestType: HTTPRequestType.Image, postData: nil, completion: { image in
                if (image != nil) {
                    cachedImages[url] = image as! UIImage?
                    completion(image as? UIImage)
                }
            })
        }
    }
    
    private class func getRequest(url: String, requestType: HTTPRequestType, postData: Dictionary<String, Any>?, completion: @escaping (Any?) -> ()) {
        var method = HTTPMethod.GET.rawValue
        
        if ((postData) != nil) {
            method = HTTPMethod.POST.rawValue
        }
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = method
        URLSession.shared.dataTask(with: request, completionHandler: {(data:Data?, response: URLResponse?, error: Error?) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                switch (httpResponse.statusCode) {
                case 200:
                    switch (requestType) {
                    case .Image:
                        completion(NetworkManager.getImageFromData(data))
                    case .Audio:
                        //completion(self.getAudioData(data: data))
                        break
                    case .JSON:
                        completion(self.getJSONFromData(data))
                    default:
                        completion(data)
                    }
                default:
                    print("connection error: \(String(describing: error?.localizedDescription))")
                }
            }
            else {
                print("Error fetching data: \(String(describing: error?.localizedDescription))")
            }
        }).resume()
    }
    
    // Data Converters
    
    private class func getJSONFromData(_ data: Data?) -> ([String : Any]?) {
        if let data = data {
            do {
                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                return (jsonDictionary as? [String : Any])
            } catch let error as NSError {
                print("error processing json data: \(error.localizedDescription)")
            }
        }
        return nil
    }
    
    private class func getImageFromData(_ data: Data?) -> (UIImage?) {
        if let data = data {
            let image = UIImage(data: data)!
            return image
        }
        return nil
    }
}
