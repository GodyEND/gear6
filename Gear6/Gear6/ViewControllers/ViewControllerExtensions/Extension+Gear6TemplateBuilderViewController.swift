//
//  Extension+Gear6TemplateBuilderViewController.swift
//  Gear6
//
//  Created by Gody on 29/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

extension Gear6TemplateBuilderViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (self.viewState) {
        case .Main:
            if let template = Gear6GearElements.getMainTemplate(catID: self.currentSelectedCategory) {
                return (template.data?.count)! + 1
            }
            break
        case .Extension:
            if let template = Gear6GearElements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                return (template.data?.count)! + 1
            }
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.TemplateBuilderView.rawValue, for: indexPath) as! Gear6TemplateViewCell
        cell.pickerDelegate = self
        
        if let template = (self.viewState == .Main) ? Gear6GearElements.getMainTemplate(catID: self.currentSelectedCategory) : Gear6GearElements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
            if (indexPath.row == (template.data?.allObjects.count)!) {
                cell.setupCell(dataType: nil)
            }
            else {
                // Display Existing Template from array
                let array = (template.data?.allObjects as! [DataTemplate]).sorted{$0.id < $1.id}
                cell.setupCell(dataType:array[indexPath.row])
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard self.textField.isHidden, self.pickerView.isHidden else {
            return false
        }
        
        if (indexPath.row == Gear6GearElements.getTemplate(id: self.activeTemplate)?.data?.count ?? 0) {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            if let template = CoreDataManager<Template>.fetchDataWithID(self.activeTemplate).first(where:{$0.id == self.activeTemplate}) {
                let dataArray = (template.data?.allObjects as! [DataTemplate]).sorted(by: {$0.id < $1.id})
                let index = (self.viewState == .Main) ? indexPath.row : indexPath.row - 1
                Gear6GearElements.removeDataTemplate(tempID: Int(template.id), dataTempID: Int(dataArray[index].id))
                CoreDataManager.saveData()
                tableView.reloadData()
            }
        }
    }
}

extension Gear6TemplateBuilderViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch (self.pickerState) {
        case .MainTemplate:
            guard row < Gear6GearElements.categories.count else {
                return
            }
            let category = Gear6GearElements.categories[row]
            self.selectedMainTemplField.text = category.name
            self.currentSelectedCategory = Int(category.id)
            self.activeTemplate = Int(category.template)
            self.viewState = .Main
            self.update()
            self.hidePicker()
            break
        case .ExtensionTemplate:
            if (row == 0) {
                self.setDefaultGroup()
                if let category = Gear6GearElements.getCategory(id: self.currentSelectedCategory) {
                    self.activeTemplate = Int(category.template)
                    self.viewState = .Main
                }
            }
            else {
                if let category = Gear6GearElements.getCategory(id: self.currentSelectedCategory) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    if (groupArray.count > 0) {
                        let group = groupArray[row-1]
                        self.currentSelectedGroup = Int(group.id)
                        self.selectedExtensionTemplField.text = group.name
                        self.activeTemplate = Int(group.template)
                        self.viewState = .Extension
                    }
                }
            }
            self.update()
            self.hidePicker()
            break
        case .AddData, .EditData:
            // Update EditedDataType
            self.editDataTemplate(pickerView: pickerView, row: row, component: component)
            break
        case .Quantity:
            self.currentEditedDatatype.quantityType = Int16(row+1)
            break
        default:
            break
        }
        
        pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch (self.pickerState) {
        case .AddData, .EditData:
            if let category = Gear6GearElements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                let objArray = category.objects?.allObjects as! [Object]
                let groupArray = category.subcategories?.allObjects as! [SubCategory]
                if (groupArray.count == 0 && objArray.count > 0) ||
                    (groupArray.count > 0 && objArray.count == 0) {
                    return 2
                }
                else if (groupArray.count > 0 && objArray.count > 0) {
                    return 3
                }
            }
            return 1
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        switch (self.pickerState) {
        case .MainTemplate:
            return Gear6GearElements.categories.count
        case .ExtensionTemplate:
            return (Gear6GearElements.getCategory(id: self.currentSelectedCategory)?.subcategories?.count)! + 1
        case .AddData, .EditData:
            switch (component) {
            case 1:
                if let category = Gear6GearElements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    if self.currentEditedDatatype.groupID > 0 || (category.subcategories?.allObjects.count)! > 0 {
                        return (category.subcategories?.allObjects.count)! + 1
                    }
                    return (category.objects?.allObjects.count)! + 1
                }
                break
            case 2:
                if let group = Gear6GearElements.getGroup(catID: Int(self.currentEditedDatatype.categoryID), groupID: Int(self.currentEditedDatatype.groupID)) {
                    return (group.objects?.allObjects.count)! + 1
                }
                break
            default:
                return Gear6GearElements.categories.count
            }
            break
        case .Quantity:
            return Gear6MainSourceTemplateQuantityType.allCases.count-1
        case .Expansion:
            return Gear6GroupSourceTemplateDataExtensionType.allCases.count-1
        default:
            break
        }
        return 1 // todo test
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch (self.pickerState) {
        case .MainTemplate:
            return Gear6GearElements.categories[row].name
        case .ExtensionTemplate:
            if (row == 0) {
                return "Default" // Todo: translatable
            }
            if let category = Gear6GearElements.getCategory(id: self.currentSelectedCategory) {
                let array = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                guard array.count > 0 else {
                    return "--"
                }
                return array[row-1].name
            }
            return "--"
        case .AddData, .EditData:
            return self.titleForEditedDataTemplate(pickerView:pickerView, row:row, component:component)
        case .Quantity:
            switch (row+1) {
            case Gear6MainSourceTemplateQuantityType.All.rawValue:
                return Gear6MainSourceTemplateQuantityType.All.stringValue
            case Gear6MainSourceTemplateQuantityType.Limit.rawValue:
                return Gear6MainSourceTemplateQuantityType.Limit.stringValue
            case Gear6MainSourceTemplateQuantityType.Multi.rawValue:
                return Gear6MainSourceTemplateQuantityType.Multi.stringValue
            case Gear6MainSourceTemplateQuantityType.Single.rawValue:
                return Gear6MainSourceTemplateQuantityType.Single.stringValue
            default:
                return "--"
            }
        case .Expansion:
            switch (row+1) {
            case Gear6GroupSourceTemplateDataExtensionType.Add.rawValue:
                return Gear6GroupSourceTemplateDataExtensionType.Add.stringValue
            case Gear6GroupSourceTemplateDataExtensionType.Remove.rawValue:
                return Gear6GroupSourceTemplateDataExtensionType.Remove.stringValue
            case Gear6GroupSourceTemplateDataExtensionType.Replace.rawValue:
                return Gear6GroupSourceTemplateDataExtensionType.Replace.stringValue
            default:
                return "--"
            }
        default:
            return "--"
        }
    }
}

extension Gear6TemplateBuilderViewController {
    func setDefaultGroup() {
        self.currentSelectedGroup = 0
        self.selectedExtensionTemplField.text = "Default"
        
        self.selectedExtensionTemplField.isHidden = false
        self.selectedExtensionTemplField.isUserInteractionEnabled = true
    }
    
    func editDataTemplate(pickerView: UIPickerView, row: Int, component: Int) {
        guard Gear6GearElements.categories.count > 0 else {
            return
        }
        self.currentEditedDatatype.categoryID = Gear6GearElements.categories[pickerView.selectedRow(inComponent: 0)].id
        switch (pickerView.numberOfComponents) {
        case 2:
            // Update Selected Rows
            if (component == 0) {
                pickerView.reloadComponent(1)
                pickerView.selectRow(0, inComponent: 1, animated: true)
            }
            
            // Update DataType
            if let category = Gear6GearElements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if (pickerView.selectedRow(inComponent: 1) > 0) {
                    if ((category.subcategories?.allObjects.count)! > 0) {
                        let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                        guard groupArray.count > 0 else {
                            return
                        }
                        self.currentEditedDatatype.groupID = groupArray[pickerView.selectedRow(inComponent: 1)-1].id
                        self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                    }
                    else {
                        let objArray = (category.objects?.allObjects as! [Object]).sorted{$0.id < $1.id}
                        guard objArray.count > 0 else {
                            return
                        }
                        self.currentEditedDatatype.objID = objArray[pickerView.selectedRow(inComponent: 1)-1].id
                        self.currentEditedDatatype.dataType = Gear6DataType.Object.rawValue
                    }
                }
                else {
                    self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                    self.currentEditedDatatype.groupID = 0
                    self.currentEditedDatatype.objID = 0
                }
            }
            break
        case 3:
            // Update Selected Rows
            if (component == 0) {
                pickerView.reloadComponent(1)
                pickerView.selectRow(0, inComponent: 1, animated: true)
                pickerView.reloadComponent(2)
                pickerView.selectRow(0, inComponent: 2, animated: true)
            }
            else if (component == 1) {
                pickerView.reloadComponent(2)
                pickerView.selectRow(0, inComponent: 2, animated: true)
            }
            
            // Update DataType
            
            if let category = Gear6GearElements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if (pickerView.selectedRow(inComponent: 1) > 0) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    guard groupArray.count > 0 else {
                        return
                    }
                    self.currentEditedDatatype.groupID = groupArray[pickerView.selectedRow(inComponent: 1)-1].id
                    
                    if (pickerView.selectedRow(inComponent: 2) > 0) {
                        if let group = Gear6GearElements.getGroup(catID: Int(self.currentEditedDatatype.categoryID), groupID: Int(self.currentEditedDatatype.groupID)) {
                            let objArray = (group.objects?.allObjects as! [Object]).sorted{$0.id < $1.id}
                            guard objArray.count > 0 else {
                                return
                            }
                            self.currentEditedDatatype.objID = objArray[pickerView.selectedRow(inComponent: 2)-1].id
                            self.currentEditedDatatype.dataType = Gear6DataType.Object.rawValue
                        }
                    }
                    else {
                        self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                        self.currentEditedDatatype.objID = 0
                    }
                }
                else {
                    self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
                    self.currentEditedDatatype.groupID = 0
                    self.currentEditedDatatype.objID = 0
                }
            }
            break
        default:
            self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
            break
        }
    }
    
    func titleForEditedDataTemplate(pickerView: UIPickerView, row: Int, component: Int) -> String? {
        switch (component) {
        case 1:
            if (row == 0) {
                return "--"
            }
            if let category = Gear6GearElements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                if ((category.subcategories?.allObjects.count)! > 0) {
                    let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                    guard groupArray.count > 0 else {
                        return "--"
                    }
                    return groupArray[row-1].name
                }
                else {
                    let objArray = (category.objects?.allObjects as! [Object]).sorted{$0.id < $1.id}
                    guard objArray.count > 0 else {
                        return "--"
                    }
                    return objArray[row-1].name
                }
            }
            break
        case 2:
            if (row == 0) {
                return "--"
            }
            if let group = Gear6GearElements.getGroup(catID: Int(self.currentEditedDatatype.categoryID), groupID: Int(self.currentEditedDatatype.groupID)) {
                let objArray = (group.objects?.allObjects as! [Object]).sorted{$0.id < $1.id}
                guard objArray.count > 0 else {
                    return "--"
                }
                return objArray[row-1].name
            }
        default:
            guard row < Gear6GearElements.categories.count else {
                return "--"
            }
            return Gear6GearElements.categories[row].name
        }
        return "--"
    }
}

// MARK: TextFieldDelegate

extension Gear6TemplateBuilderViewController {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.currentEditedDatatype.desc = textField.text
        textField.text = ""
        textField.resignFirstResponder()
        textField.isHidden = true
        self.dismissAction()
        
        return false
    }
}
