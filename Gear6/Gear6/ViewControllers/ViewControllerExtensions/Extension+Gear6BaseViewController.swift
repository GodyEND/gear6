//
//  Extension+Gear6BaseViewController.swift
//  Gear6
//
//  Created by Gody on 30/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

// MARK: Setup Views

extension Gear6BaseViewController {
    func setupSmokeView() {
        //Gear6SmokeView.shared.updateScene()
        self.view.addSubview(Gear6SmokeView.shared)
        self.view.sendSubview(toBack: Gear6SmokeView.shared)
        Gear6SmokeView.shared.presentScene()
    }
    
    func setupScreenCover() {
        // Screen Cover
        let coverView = UIImageView()
        coverView.frame = CGRect(x:0, y: 0, width: Gear6Defaults.windowFrame.width, height: Gear6Defaults.windowFrame.height)
        coverView.image = #imageLiteral(resourceName: "Gear6CoverLayer")
        coverView.alpha = 0.5
        self.view.addSubview(coverView)
        
        // Laser Boarder
        let laserBorderLeft = UIImageView()
        laserBorderLeft.frame = CGRect(x:0, y: 0, width: 15, height: Gear6Defaults.windowFrame.height)
        laserBorderLeft.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        self.view.addSubview(laserBorderLeft)
        
        let laserBorderRight = UIImageView()
        laserBorderRight.frame = CGRect(x:Gear6Defaults.windowFrame.width - 15, y: 0, width: 15, height: Gear6Defaults.windowFrame.height)
        laserBorderRight.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        laserBorderRight.transform = laserBorderRight.transform.rotated(by: CGFloat(Double.pi))
        self.view.addSubview(laserBorderRight)
        
        let laserBorderBottom = UIImageView()
        laserBorderBottom.image = #imageLiteral(resourceName: "Gear6LaserBorder")
        laserBorderBottom.transform = laserBorderBottom.transform.rotated(by: CGFloat(Double.pi * 3 / 2))
        laserBorderBottom.frame = CGRect(x:0, y: Gear6Defaults.windowFrame.height - 7, width: Gear6Defaults.windowFrame.width, height: 7)
        self.view.addSubview(laserBorderBottom)
        
        self.navigationItem.backBarButtonItem = nil
    }
}

@objc extension Gear6BaseViewController : Gear6ViewControllerProtocol {
    
    func setupView() {
        //setupSmokeView()
        setupScreenCover()
    }
    
    func presentScreen() {
        // overridable
    }
    
    func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        // overridable
    }
}

extension Gear6BaseViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= Defaults.NAME_MAXIMUM_LENGTH
    }
}
