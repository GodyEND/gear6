//
//  Gear6ToolBuilderViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6ToolBuilderViewController: Gear6BaseViewController {
    
    static let shared = Gear6ToolBuilderViewController()
    
    override func setupView() {
        super.setupView()
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}
