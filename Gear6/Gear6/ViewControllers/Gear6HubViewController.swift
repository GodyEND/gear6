//
//  Gear6HubViewController.swift
//  Gear6
//
//  Created by Gody on 03/12/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

// The hub is where the user will access existing libraries

// HubBtns
// View Favourites / Offline Libraries
// Create new Gear6
// Edit Gear6 the user has permission to edit

// Offline use will hide the trade feature

// HubBtns

class Gear6HubViewController: Gear6BaseViewController {
    
    static let shared = Gear6HubViewController()
    
    override func setupView() {
        super.setupView()
        // Btn to buildermenu
        // btn to librarymenu
        // username textfield for login
        // password textfield hidd chars to login on success display success notification
        // alt mehtods, login with facebook, twitter, reddit, chrome, steam
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}
