//
//  Gear6DatabaseViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6DatabaseViewController: Gear6BaseViewController {
    
    static let shared = Gear6DatabaseViewController()
    var viewState = DatabaseViewState.AddToCategory
    public private (set) var pickerState = DatabasePickerState.Category
    
    let addObjBtn = UIButton()
    let deleteObjBtn = UIButton()
    let activeCategoryField = UILabel()
    let activeGroupField = UILabel()
    let assetLoaderView = Gear6DatabaseAssetLoader()
    
    var currentSelectedCategory: Int = 0
    var currentSelectedGroup: Int = -1 // Unselected
    var currentEditedObj = ObjectStruct()
    var activeTemplate: Int = 0
    
    override func setupView() {
        super.setupView()
        
        // TODO: longpress scroll table view up/down
        
        self.activeCategoryField.frame = CGRect(x: Screen.Width * 0.2, y: Screen.FIRST_ITEM_TOP, width: Screen.Width * 0.6, height: Screen.TEXTFIELD_HEIGHT)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showMainPicker))
        self.activeCategoryField.addGestureRecognizer(gesture)
        self.activeCategoryField.textAlignment = .center
        self.activeCategoryField.backgroundColor = UIColor.purple
        if (Gear6Elements.categories.count > 0) {
            self.activeCategoryField.text = Gear6Elements.categories[0].name
            self.currentSelectedCategory = Int(Gear6Elements.categories[0].id)
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                if (category.subcategories?.allObjects.count)! > 0 {
                    self.currentSelectedGroup = 0
                }
            }
            self.activeTemplate = Int(Gear6Elements.categories[0].template)
        }
        self.activeCategoryField.isUserInteractionEnabled = true
        self.view.addSubview(self.activeCategoryField)
        
        self.activeGroupField.frame = CGRect(x: Screen.Width * 0.2, y: Screen.FIRST_ITEM_TOP+Screen.TEXTFIELD_HEIGHT, width: Screen.Width * 0.6, height: Screen.TEXTFIELD_HEIGHT)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(showExtensionPicker))
        self.activeGroupField.addGestureRecognizer(gesture2)
        self.activeGroupField.backgroundColor = UIColor.orange
        self.activeGroupField.isUserInteractionEnabled = false
        self.activeGroupField.isHidden = true
        self.activeGroupField.textAlignment = .center
        self.view.addSubview(self.activeGroupField)
        
        self.addObjBtn.frame = CGRect(x: 15, y: Screen.TAB_VIEW_TOP-(Screen.TEXTFIELD_HEIGHT+10), width: 50, height: Screen.TEXTFIELD_HEIGHT)
        self.addObjBtn.backgroundColor = UIColor.blue
        self.addObjBtn.addTarget(self, action: #selector(addNewObject), for: .touchUpInside)
        self.view.addSubview(self.addObjBtn)
        
        self.deleteObjBtn.frame = CGRect(x: 75+Screen.TEXTFIELD_WIDTH, y: Screen.TAB_VIEW_TOP-(Screen.TEXTFIELD_HEIGHT+10), width: 50, height: Screen.TEXTFIELD_HEIGHT)
        self.deleteObjBtn.backgroundColor = UIColor.red
        self.deleteObjBtn.addTarget(self, action: #selector(removeObject), for: .touchUpInside)
        self.deleteObjBtn.isHidden = true
        self.view.addSubview(self.deleteObjBtn)
        
        self.tableView.register(Gear6DatabaseViewCell.self, forCellReuseIdentifier: Cell.DatabaseView.rawValue)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = false
        
        self.textField.placeholder = Placeholder.ENT_OBJ_NAME
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.view.bringSubview(toFront: self.pickerView)
        
        self.assetLoaderView.notationField.delegate = self
        self.assetLoaderView.isHidden = true
        self.view.addSubview(self.assetLoaderView)
        
        self.currentEditedObj.reset()
        self.update()
    }
    
    func update() {
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            if ((category.subcategories?.count)! > 0) {
                if self.currentSelectedGroup == 0 {
                    self.setDefaultGroup()
                }
                else {
                    self.activeGroupField.isHidden = false
                    self.activeGroupField.isUserInteractionEnabled = true
                }
            }
            else {
                self.currentSelectedGroup = 0
                self.activeGroupField.isHidden = true
                self.activeGroupField.isUserInteractionEnabled = false
            }
        }
        
        switch (self.viewState) {
        case .AddToCategory:
            if getActiveObjects().count < 1 { self.tableView.isHidden = true }
            else { self.tableView.isHidden = false }
        case .AddToGroup:
            if getActiveGroupObjects().count < 1 { self.tableView.isHidden = true }
            else { self.tableView.isHidden = false }
        default: break
        }
        if (self.viewState == .AddToCategory || self.viewState == .AddToGroup) {
            self.tableView.reloadData()
        }
        self.pickerView.reloadAllComponents()
    }
    
    func getActiveTemplate() {
        if self.currentSelectedGroup > 0 {
            if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                self.activeTemplate = Int(group.template)
                self.viewState = .AddToGroup
            }
        }
        else {
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                self.activeTemplate = Int(category.template)
                self.viewState = .AddToCategory
            }
        }
    }
    func getActiveDataBlocks() -> [DataBlock] {
        if let template = Gear6Elements.getTemplate(id: self.activeTemplate) {
            if let blocks = template.data?.allObjects as? [DataBlock] {
                return blocks
            }
        }
        return []
    }
    func getActiveObjects() -> [Obj] {
        
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            if let objects = (category.objects?.allObjects as? [Obj]) {
                return objects.sorted(by: {$0.id<$1.id})
            }
        }
        return []
    }
    func getActiveGroupObjects() -> [Obj] {
        guard self.currentSelectedGroup > 0 else {
            return []
        }
        if let category = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
            if let objects = category.objects?.allObjects as? [Obj] {
                return objects.sorted(by: {$0.id<$1.id})
            }
        }
        return []
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        self.pickerState = .Category
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            if (category.subcategories?.allObjects.count)! > 0 {
                self.currentSelectedGroup = 0
            }
        }
        self.update()
        completion(true)
        //self.setActiveTemplate()
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}


@objc extension Gear6DatabaseViewController: DatabaseCellDelegate {
    
    func showPicker() {
        guard self.textField.isHidden, self.assetLoaderView.isHidden else { return }
        self.pickerView.isHidden = false
        self.pickerView.isUserInteractionEnabled = true
        self.pickerView.reloadAllComponents()
    }
    
    func showMainPicker() {
        self.pickerState = .Category
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            let index = Gear6Elements.categories.index(of: category)
            self.pickerView.selectRow(index!, inComponent: 0, animated: false)
        }
        self.showPicker()
    }
    func showExtensionPicker() {
        self.pickerState = .Group
        if self.currentSelectedGroup == 0 {
            self.pickerView.selectRow(0, inComponent: 0, animated: false)
        }
        if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted(by: {$0.id < $1.id})
                let index = groupArray.index(of: group)!
                self.pickerView.selectRow(index+1, inComponent: 0, animated: false)
            }
        }
        self.showPicker()
    }
    
    func hidePicker() {
        self.pickerView.isHidden = true
        self.pickerView.isUserInteractionEnabled = false
        
        self.tableView.reloadData()
        let currentDataCount = (self.currentSelectedGroup > 0) ? Gear6Elements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup)?.data?.count : Gear6Elements.getMainTemplate(catID: self.currentSelectedCategory)?.data?.count
        
        // Scroll to Bottom
        let indexPath = IndexPath(item: currentDataCount!, section: 0)
        if (self.tableView.visibleCells.count < currentDataCount!+1) && self.tableView.visibleCells.count > Int(self.tableView.frame.size.height / (self.tableView.cellForRow(at: IndexPath(item: 0, section: 0))?.frame.size.height)!) {
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    func addNewObject() {
        guard self.currentSelectedCategory > 0, self.pickerView.isHidden, self.assetLoaderView.isHidden else { return }
        self.textField.text = ""
        self.textField.isHidden = false
        self.textField.becomeFirstResponder()
    }
    func removeObject() {
        guard self.currentSelectedCategory > 0 else { return }
        self.textField.text = ""
        self.textField.isHidden = true
        self.textField.resignFirstResponder()
        self.deleteObjBtn.isHidden = true
        
        if self.currentSelectedGroup > 0 {
            Gear6Elements.removeObject(groupID: self.currentSelectedGroup, objID: Int(self.currentEditedObj.id))
            self.viewState = .AddToGroup
        }
        else {
            Gear6Elements.removeObject(catID: self.currentSelectedCategory, objID: Int(self.currentEditedObj.id))
            self.viewState = .AddToCategory
        }
        self.currentEditedObj.reset()
        self.update()
    }
    
    func endNewObjectAddition() {
        switch self.viewState {
        case .AddToCategory:
            let objects = self.getActiveObjects()
            guard (objects.filter { $0.name == self.currentEditedObj.name }).count == 0 else {
                return // TODO: Notif Duplicate exists // TODO: Only Display notif if editedObject is pointing to a different object
            }
            let newObject = CoreDataManager<Obj>.getNewObject()
            newObject.name = self.currentEditedObj.name
            newObject.id = Gear6Elements.getNewObjectID()
            
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                category.addToObjects(newObject)
                Gear6Elements.objects.append(newObject)
            }
        case .AddToGroup:
            let objects = self.getActiveGroupObjects()
            guard (objects.filter { $0.name == self.currentEditedObj.name }).count == 0 else {
                return // TODO: Notif Duplicate exists
            }
            // Check if obj exists in category / if true then refer to the category obj
            let catObjects = self.getActiveObjects().filter { $0.name == self.currentEditedObj.name }
            if catObjects.count > 0 {
                let newObject = catObjects.first(where: { $0.name == self.currentEditedObj.name})!
                newObject.addToSubcategories(Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup)!)
            }
            else {
                let newObject = CoreDataManager<Obj>.getNewObject()
                newObject.name = self.currentEditedObj.name
                newObject.id = Gear6Elements.getNewObjectID()
                if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                    category.addToObjects(newObject)
                    if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                        group.addToObjects(newObject)
                    }
                    Gear6Elements.objects.append(newObject)
                    // TODO: on remove object if subcats 1 also remove object from category else only from group
                }
            }
        case .EditObject:
            // Check for duplicate
            let objects = self.getActiveObjects()
            guard (objects.filter { $0.name == self.currentEditedObj.name }).count == 0 else {
                return // TODO: Notif Duplicate exists
            }
            if let object = Gear6Elements.getObject(catID: self.currentSelectedCategory, objID: Int(self.currentEditedObj.id)) {
                object.name = self.currentEditedObj.name
            }
            self.getActiveTemplate() // Update View State
        default: break
        }
        self.currentEditedObj.reset()
        self.update()
    }
    
    func editObject() {
        // TODO: obj depends on App State
        guard let obj = Gear6Elements.getObject(catID: self.currentSelectedCategory, objID: Int(self.currentEditedObj.id)) else { return }
        self.viewState = .EditObject
        
        self.currentEditedObj.name = obj.name!
        self.textField.isHidden = false
        self.textField.text = self.currentEditedObj.name
        self.textField.becomeFirstResponder()
        self.deleteObjBtn.isHidden = false
    }
    func editIcon() {
        // Show assetsview
        // set curr edit obj id
        // pre populate image views if images exist
        guard self.textField.isHidden, self.pickerView.isHidden else { return }
        guard let obj = Gear6Elements.getObject(catID: self.currentSelectedCategory, objID: Int(self.currentEditedObj.id)) else { return }
        self.viewState = .EditAssets
        self.assetLoaderView.prepareForReuse()
        self.assetLoaderView.objectTitle = obj.name!
        self.assetLoaderView.isHidden = false
    }
    func editData() {
        // Disable if TemplateData is 0
        guard let object = Gear6Elements.getObject(catID: Int(self.currentEditedObj.categoryID), objID: Int(self.currentEditedObj.id)) else { return }
        guard (object.data?.count)! > 0 else { return }
        
        Gear6DatabaseDetailViewController.shared.selectedObject = object
        Gear6NavigationController.shared.pushViewController(Gear6DatabaseDetailViewController.shared, animated: true)
        
    }
    
    override func dismissAction() {
        self.view.endEditing(true)
        self.textField.isHidden = true
        self.deleteObjBtn.isHidden = true
        self.assetLoaderView.isHidden = true
        
        self.getActiveTemplate()
        
        guard self.pickerView.isHidden == false else { return }
        self.hidePicker()
    }
    
    override func swipeAction(_ sender: UISwipeGestureRecognizer) {
        guard self.textField.isHidden, self.pickerView.isHidden else { return }
        
        if sender.direction == .left {
            self.pickerState = .Category
            self.pickerView.reloadAllComponents()
            if self.currentSelectedCategory != Int(Gear6Elements.categories[0].id) {
                // Get current Cat index
                let currentIndex = Gear6Elements.categories.firstIndex(of: Gear6Elements.getCategory(id: self.currentSelectedCategory)!)!
                let index = Gear6Elements.categories.startIndex.distance(to: currentIndex) - 1
                self.currentSelectedCategory = Int(Gear6Elements.categories[index].id)
                self.pickerView.selectRow(index, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: index, inComponent: 0)
            }
            else {
                self.currentSelectedCategory = Int(Gear6Elements.categories.last!.id)
                self.pickerView.selectRow((Gear6Elements.categories.count-1), inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: (Gear6Elements.categories.count-1), inComponent: 0)
            }
            self.currentSelectedGroup = 0
            self.update()
        }
        else if sender.direction == .right {
            self.pickerState = .Category
            self.pickerView.reloadAllComponents()
            if self.currentSelectedCategory != Int(Gear6Elements.categories.last!.id) {
                // Get current Cat index
                let currentIndex = Gear6Elements.categories.firstIndex(of: Gear6Elements.getCategory(id: self.currentSelectedCategory)!)!
                let index = Gear6Elements.categories.startIndex.distance(to: currentIndex) + 1
                self.currentSelectedCategory = Int(Gear6Elements.categories[index].id)
                self.pickerView.selectRow(index, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: index, inComponent: 0)
            }
            else {
                self.currentSelectedCategory = Int(Gear6Elements.categories.first!.id)
                self.pickerView.selectRow(0, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 0)
            }
            self.currentSelectedGroup = 0
            self.update()
        }
    }
}
