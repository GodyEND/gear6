//
//  Gear6NavigationController.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6NavigationController: UINavigationController {
    
    static let shared = Gear6NavigationController(rootViewController: Gear6MainViewController.shared)
    var navigationStack:[Gear6View] = [Gear6View.Main]
    let backButton: Gear6BackButton = Gear6BackButton(frame: CGRect(x: 0, y: 0, width: 34, height: 22))
    var backItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    var activeViewController: Gear6BaseViewController {
        return self.getViewController(view: self.navigationStack.last!)
    }
    
    private func pushToViewController(pageID: Int) {
        // Add new to stack
        let view = Gear6View(rawValue: pageID)
        
        self.activeViewController.clearScreen(completion: { [unowned self] success in
            if(success) {
                self.navigationStack.append(view!)
                self.activeViewController.title = view?.stringValue.uppercased()
                if (view != Gear6View.Main) {
                    if (self.activeViewController.navigationItem.leftBarButtonItem != self.backItem) {
                        self.activeViewController.navigationItem.setLeftBarButton(self.backItem, animated: false)
                    }
                }
                self.clearScreen(true)
            }
        })
    }
    
    private func pushToPreviousViewController() {
        // Remove top from stack
        self.activeViewController.clearScreen(completion: { [unowned self] success in
            if (success) {
                self.navigationStack.removeLast()
                self.activeViewController.title = self.navigationStack.last?.stringValue.uppercased()
                if (self.activeViewController.title == Gear6View.Main.stringValue.uppercased()) {
                    self.activeViewController.navigationItem.leftBarButtonItem = nil
                }
                self.clearScreen(false)
            }
        })
    }
    
    private func getViewController<T:Gear6BaseViewController>(view: Gear6View) -> T {
        switch (view) {
        case .Hub:
            return Gear6HubViewController.shared as! T
        case .BuilderMenu:
            return Gear6BuilderMenuViewController.shared as! T
        case .LibraryMenu:
            return Gear6LibraryMenuViewController.shared as! T
        case .Main:
            return Gear6MainViewController.shared as! T
        case .Mapper:
            return Gear6MapperViewController.shared as! T
        case .TemplateBuilder:
            return Gear6TemplateBuilderViewController.shared as! T
        case .Database:
            return Gear6DatabaseViewController.shared as! T
        case .UserBuilder:
            return Gear6UserBuilderViewController.shared as! T
        case .ToolBuilder:
            return Gear6ToolBuilderViewController.shared as! T
        case .AssistantBuilder:
            return Gear6AssistantBuilderViewController.shared as! T
            
        case .BuildCreator:
            return Gear6BuildCreatorViewController.shared as! T
        case .BuildPage:
            return Gear6BuildPageViewController.shared as! T
        case .Library:
            return Gear6LibraryViewController.shared as! T
        case .Collection:
            return Gear6CollectionViewController.shared as! T
        case .Assistant:
            return Gear6AssistantViewController.shared as! T
        case .Market:
            return Gear6MarketViewController.shared as! T
        case .Page:
            return Gear6PageViewController.shared as! T
            // TODO: Exit Route
        //case .Page: // TODO:
          //  return Gear6PageViewController.page[pageID] as! T
            //return page with id from PageVC Stack
        default:
            return Gear6BaseViewController() as! T
        }
    }
    
    class func getActiveGear6View() -> Gear6View {
        return Gear6NavigationController.shared.navigationStack.last!
    }
    
    func customPageAction(pageID: Int) {
        Gear6NavigationController.shared.pushToViewController(pageID: Gear6View.Page.rawValue)
    }
}

extension Gear6NavigationController: Gear6BackButtonDelegate {
    func setupView() {
        
        self.navigationBar.frame = CGRect(x: 0.0, y: 0.0, width: Gear6Defaults.windowFrame.width, height: 50.0)
        
        // Set NavigationBar Transparency
        self.navigationBar.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor(patternImage: Gear6Pattern.base!)
        
        // todo: shadow
        
        // Set NavigationBar Pattern
        let patternLayer = CALayer()
        patternLayer.contents = Gear6Pattern.base!
        self.navigationBar.layer.addSublayer(patternLayer)
        
        // NavigationBar Accent
        let navBarAccentLayer = UIImageView(frame: CGRect(x: 0.0, y: 42.0, width: 300.0, height: 8.0))
        navBarAccentLayer.image = #imageLiteral(resourceName: "Gear6NavBarAccent").withRenderingMode(.alwaysTemplate)
        navBarAccentLayer.tintColor = Gear6Defaults.primaryColor
        self.navigationBar.layer.addSublayer(navBarAccentLayer.layer)
        
        // Set Navigation Bar Alpha Layer
        self.navigationBar.layer.mask = CALayer.addTopDownFadeMask(frame: CGRect(x: 0, y: 0, width: Gear6Defaults.windowFrame.width, height: 68))
        
        self.activeViewController.title = Gear6View.Main.stringValue.uppercased()
        
        // Delegation
        self.backButton.delegate = self
        
        self.backItem = UIBarButtonItem(customView: self.backButton)
    }
    
    func presentScreen() {
        self.activeViewController.presentScreen(completion: { _ in
            self.activeViewController.standbyAnimation()
        })
        
        self.backButton.showAnimation(completion: nil)
    }
    
    func clearScreen(_ isPush:Bool) {
        if (isPush) {
            self.pushViewController(self.activeViewController, animated: false)
        }
        else {
            self.popViewController(animated: false)
        }
    }
    
    // Delegated Methods
    func backAction() {
        if (Gear6NavigationController.getActiveGear6View() == Gear6View.Mapper) {
            switch (Gear6MapperViewController.shared.viewState) {
            case .Category, .EditCategory:
                Gear6NavigationController.shared.pushToPreviousViewController()
            case .Grouping, .EditGroup:
                Gear6MapperViewController.shared.viewState = .Category
            case .Resource:
                Gear6MapperViewController.shared.viewState = .Grouping
            default: break
            }
            Gear6MapperViewController.shared.update()
        }
        else if Gear6NavigationController.getActiveGear6View() == Gear6View.TemplateBuilder {
            // Valudation Check
            guard Gear6TemplateBuilderViewController.shared.validation() else { return }
            Gear6NavigationController.shared.pushToPreviousViewController()
        }
        else {
            Gear6NavigationController.shared.pushToPreviousViewController()
            // TODO: Single Ice Cube Drop in Glass Sound Effect
        }
    }
}

@objc extension Gear6NavigationController {
    func mainButtonAction(sender:UITapGestureRecognizer) {
        if let mainButton = sender.view as? Gear6MainMenuButton {
            switch mainButton.pageID {
            case Gear6View.Mapper.rawValue:
                Gear6MapperViewController.shared.viewState = .Category
            case Gear6View.TemplateBuilder.rawValue:
                guard Gear6Elements.categories.count > 0 else { return }
                if (Gear6TemplateBuilderViewController.shared.currentSelectedCategory == 0) {
                    Gear6TemplateBuilderViewController.shared.currentSelectedCategory = Int(Gear6Elements.categories[0].id)
                }
                Gear6TemplateBuilderViewController.shared.update()
            case Gear6View.Database.rawValue:
                guard Gear6Elements.categories.count > 0 else { return }
            default: break
            }
            Gear6NavigationController.shared.pushToViewController(pageID: mainButton.pageID)
        }
    }
}
