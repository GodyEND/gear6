//
//  Gear6AssistantBuilderViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit
import CoreData

class Assistant {
    func findPath() {
        
    }
    
    func checkRequiredMethods() {
        
    }
    
    func validatePath() {
        
    }
    
    func calcDifficulty() {
        
    }
    
    class func getObjectType(id: Int) -> ObjectType { // Object with ID
        return ObjectType()
    }
}


//
//struct Method {
//    var inputs: [CollectableObject]?
//    var output: CollectableObject?
//    var componentID: Int?
//    var methodName: String?
//    var execOrder: Int?
//    var affectedProperties: [Int]?
//    var expectedOutput: CollectableObject?
//    var steps: [Step]?
//}
//
//struct Step {
//    //var target [Input1, Input2, execFuncProp1, execFuncProp2]
//    var result: CollectableObject?
//    var execOrder: Int?
//    var execFunctions: [ExecFunc]
//
//    var parentMethod: Int?
//    var inputs: [Int]?
//}
//
//struct ExecFunc {
//    var type: ExecFuncType?
//    var value: Int?
//    var propertyOptions: [CollectableProperty]?
//}
//



// Assistant View Controller

class Gear6AssistantBuilderViewController: Gear6BaseViewController {
    
    static let shared = Gear6AssistantBuilderViewController()
    
    let assistant = Assistant()
    
    override func setupView() {
        super.setupView()
        
        // Categories: 1=Pokemon, 2=Item, 3=Location, 4=Nature, 5=Ability, 6=Attack, 7=IV, 8=Gender, 9=Form, 10=EV, 11=Level
        // Groups: 1=Base, 2=Stage1, 3=Stage2, 4=Electric, 5=Fire,
                //200=Ball, 301=Town, 302=Route, 901=Shiny
        // Objects: 2400=Pikachu, 401=Jolly, 201=LuxBall, 303=Alabasta,
        
        
        // Set Goal // Pikachu, LuxBall, Jolly, Level 8
        let itself = CollectableProperty(id: 1, objectID: 2400)
        let luxball = CollectableProperty(id: 4, objectID: 201)
        let jolly = CollectableProperty(id: 2, objectID: 401)
        let level = CollectableProperty(id: 22, objectID: 0, value: 1)
        
        var pikachu = CollectableObject(objectID: 2400)
        pikachu.properties.append(itself)
        pikachu.properties.append(luxball)
        pikachu.properties.append(jolly)
        pikachu.properties.append(level)
        
        
        // TODO:
        var guide: Guide = Guide()
        guide.id = 1 // Auto Generated
        guide.name = "Pokemon Guide" // Set by Builder / Output Type
        
        guide.goal = pikachu // Set by User
        guide.traceBackObject = pikachu // Set by goal
        guide.modifiableObject = CollectableObject(objectID: 0)
        
        guide.requiredMethods = [] // Set by Goal Properties
        
        
        // Pokemon propertyID
        // 0=class, 1=object, 2=nature, 3=ability, 4=ball, 5=helditem, 6=move1, 7=move2, 8=move3, 9=move4
        // 10=iv1, 11=iv2, 12=iv3, 13=iv4, 14=iv5, 15=iv6, 16=ev1, 17=ev2, 18=ev3, 19=ev4, 20=ev5, 21=ev6
        // 22=level, 23=gender, 24=form
        
        var catchMethod = Method()
        catchMethod.componentID = 1 // Auto Generated
        catchMethod.methodName = "Catch" // Set by builder
        catchMethod.affectedProperties = [1,2,3,10,11,12,13,14,15,16,22,23,24] // Set by builder
        
        var setPokemon = ExecFunc()
        setPokemon.type = .setProperty
        let step1 = Step(result: nil, execOrder: 1, execFunctions: [setPokemon], parentMethod: 1, inputs: nil)
        catchMethod.steps = [step1]
        
        var levelupMethod = Method()
        levelupMethod.componentID = 2
        levelupMethod.methodName = "Level Up"
        levelupMethod.affectedProperties = [1, 22]
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}
