//
//  Gear6TemplateBuilderViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6TemplateBuilderViewController: Gear6BaseViewController {
    
    static let shared = Gear6TemplateBuilderViewController()
    var viewState = TemplateBuilderState.Main
    public private (set) var pickerState = TemplateBuilderPickerState.MainTemplate
    
    let rangeField = Gear6RangeFieldView()
    
    let selectedMainTemplField = UILabel()
    let selectedExtensionTemplField = UILabel()
    var activeTemplate: Int = 0
    var currentSelectedCategory: Int = 0
    var currentSelectedGroup: Int = -1 // Unselected
    var currentEditedDatatype = DataBlockStruct()
    
    override func setupView() {
        super.setupView()
        
        self.selectedMainTemplField.frame = CGRect(x: Screen.Width * 0.2, y: Screen.FIRST_ITEM_TOP, width: Screen.Width * 0.6, height: Screen.TEXTFIELD_HEIGHT)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showMainPicker))
        self.selectedMainTemplField.addGestureRecognizer(gesture)
        self.selectedMainTemplField.textAlignment = .center
        self.selectedMainTemplField.backgroundColor = UIColor.purple
        if (Gear6Elements.categories.count > 0) {
            self.selectedMainTemplField.text = Gear6Elements.categories[0].name
            self.currentSelectedCategory = Int(Gear6Elements.categories[0].id)
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                if (category.subcategories?.allObjects.count)! > 0 {
                    self.currentSelectedGroup = 0
                }
            }
            self.activeTemplate = Int(Gear6Elements.categories[0].template)
            self.pickerState = .MainTemplate
            self.viewState = .Main
        }
        self.selectedMainTemplField.isUserInteractionEnabled = true
        self.view.addSubview(self.selectedMainTemplField)
        
        self.selectedExtensionTemplField.frame = CGRect(x: Screen.Width * 0.2, y: Screen.FIRST_ITEM_TOP+Screen.TEXTFIELD_HEIGHT, width: Screen.Width * 0.6, height: Screen.TEXTFIELD_HEIGHT)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(showExtensionPicker))
        self.selectedExtensionTemplField.addGestureRecognizer(gesture2)
        self.selectedExtensionTemplField.backgroundColor = UIColor.orange
        self.selectedExtensionTemplField.isUserInteractionEnabled = false
        self.selectedExtensionTemplField.isHidden = true
        self.selectedExtensionTemplField.textAlignment = .center
        self.view.addSubview(self.selectedExtensionTemplField)
        
        self.tableView.register(Gear6TemplateViewCell.self, forCellReuseIdentifier: Cell.TemplateBuilderView.rawValue)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = false
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        // TODO: Darken View and center text field
        
        self.rangeField.frame = textField.frame
        self.rangeField.backgroundColor = UIColor.clear
        self.rangeField.delegate = self
        self.view.addSubview(self.rangeField)
        self.rangeField.isHidden = true
        
        self.view.bringSubview(toFront: self.pickerView)
        self.update()
    }
    
    func update() {
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            if ((category.subcategories?.count)! > 0) {
                if (self.currentSelectedGroup == 0) {
                    self.setDefaultGroup()
                }
                else {
                    self.selectedExtensionTemplField.isHidden = false
                    self.selectedExtensionTemplField.isUserInteractionEnabled = true
                }
            }
            else {
                self.currentSelectedGroup = 0
                self.selectedExtensionTemplField.isHidden = true
                self.selectedExtensionTemplField.isUserInteractionEnabled = false
            }
        }
        
        self.tableView.reloadData()
        self.pickerView.reloadAllComponents()
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        completion(true)
        self.pickerState = .MainTemplate
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            if (category.subcategories?.allObjects.count)! > 0 {
                self.currentSelectedGroup = 0
            }
        }
        self.update()
        self.setActiveTemplate()
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        self.pickerView.isHidden = true
        self.textField.isHidden = true
        self.rangeField.isHidden = true
        completion!(true)
    }
}

extension Gear6TemplateBuilderViewController {
    private func resetEditedDataBlock() {
        self.currentEditedDatatype.reset()
        for i in 0..<self.pickerView.numberOfComponents {
            self.pickerView.selectRow(0, inComponent: i, animated: false)
        }
    }
    
    private func setActiveTemplate() {
        if self.currentSelectedGroup > 0 {
            if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                self.activeTemplate = Int(group.template)
            }
        }
        else {
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                self.activeTemplate = Int(category.template)
            }
        }
    }
    
    func validation() -> Bool {
        for template in Gear6Elements.templates {
            if let dataArray = (template.data?.allObjects as? [DataBlock]) {
                if dataArray.count > 1 {
                    for i in 0..<dataArray.count-1 {
                        // Cat & Desc match check
                        for j in i+1..<dataArray.count {
                            if dataArray[i].categoryID == dataArray[j].categoryID &&
                                dataArray[i].groupID == dataArray[j].groupID &&
                                dataArray[i].desc == dataArray[j].desc {
                                return false
                            }
                        }
                    }
                }
            }
        }
        
        self.textField.text = ""
        self.textField.isHidden = true
        self.rangeField.reset()
        self.rangeField.isHidden = true
        self.pickerView.isHidden = true
        self.resetEditedDataBlock()
        
        return true
    }
}

@objc extension Gear6TemplateBuilderViewController: TempCellDelegate {
    
    func showPicker() {
        self.pickerView.isHidden = false
        self.pickerView.isUserInteractionEnabled = true
        self.pickerView.reloadAllComponents()
    }
    
    func showMainPicker() {
        guard self.textField.isHidden, self.rangeField.isHidden else { return }
        self.pickerState = .MainTemplate
        if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
            let index = Gear6Elements.categories.index(of: category)
            self.pickerView.selectRow(index!, inComponent: 0, animated: false)
        }
        self.showPicker()
    }
    func showExtensionPicker() {
        guard self.textField.isHidden, self.rangeField.isHidden else { return }
        self.pickerState = .ExtensionTemplate
        if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted(by: {$0.id < $1.id})
                let index = groupArray.index(of: group)!
                self.pickerView.selectRow(index+1, inComponent: 0, animated: false)
            }
        }
        self.showPicker()
    }
    
    func addNewDataBlock() {
        guard self.textField.isHidden, self.rangeField.isHidden else { return }
        
        self.pickerState = .AddData
        if self.currentSelectedGroup > 0 {
            if let group = Gear6Elements.getGroup(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup) {
                self.activeTemplate = Int(group.template)
                self.currentEditedDatatype.dataType = Gear6DataType.Extension.rawValue
            }
        }
        else {
            if let category = Gear6Elements.getCategory(id: self.currentSelectedCategory) {
                self.activeTemplate = Int(category.template)
                self.currentEditedDatatype.dataType = Gear6DataType.Category.rawValue
            }
        }
        self.currentEditedDatatype.categoryID = Gear6Elements.categories[0].id
        self.showPicker()
    }
    
    func editCategory() {
        guard self.textField.isHidden, self.rangeField.isHidden, self.currentEditedDatatype.id > 0 else { return }
        
        self.pickerState = .EditData
        if let dataBlock = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
            Gear6Elements.updateDataBlockStruct(lhs: &self.currentEditedDatatype, with: dataBlock)
            if self.currentEditedDatatype.objID > 0 {
                if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    let catIndex = Gear6Elements.categories.index(of: category)
                    if let obj = Gear6Elements.getObject(catID: Int(self.currentEditedDatatype.categoryID), objID: Int(self.currentEditedDatatype.objID)) {
                        self.pickerView.reloadComponent(0)
                        self.pickerView.selectRow(catIndex!, inComponent: 0, animated: false)
                        self.pickerView(self.pickerView, didSelectRow: catIndex!, inComponent: 0)
                        let objIndex = ((category.objects?.allObjects as! [Obj]).sorted{$0.id < $1.id}).index(of: obj)
                        if self.pickerView.numberOfComponents == 3 {
                            self.pickerView.selectRow(0, inComponent: 1, animated: false)
                            self.pickerView.selectRow(objIndex!+1, inComponent: 2, animated:  false)
                            self.pickerView(self.pickerView, didSelectRow: objIndex!+1, inComponent: 2)
                        }
                        else {
                            self.pickerView.selectRow(objIndex!+1, inComponent: 1, animated:  false)
                            self.pickerView(self.pickerView, didSelectRow: objIndex!+1, inComponent: 1)
                        }
                    }
                }
            }
            else if self.currentEditedDatatype.groupID > 0 {
                if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    let catIndex = Gear6Elements.categories.index(of: category)
                    if let group = Gear6Elements.getGroup(catID: Int(self.currentEditedDatatype.categoryID), groupID: Int(self.currentEditedDatatype.groupID)) {
                        let groupArray = (category.subcategories?.allObjects as! [SubCategory]).sorted{$0.id < $1.id}
                        guard groupArray.count > 0 else { return }
                        let index = groupArray.index(of: group)!
                        
                        self.pickerView.reloadComponent(0)
                        self.pickerView.selectRow(catIndex!, inComponent: 0, animated: false)
                        self.pickerView(self.pickerView, didSelectRow: catIndex!, inComponent: 0)
                        if self.pickerView.numberOfComponents > 2 {
                            self.pickerView.selectRow(0, inComponent: 2, animated: false)
                        }
                        if self.pickerView.numberOfComponents > 1 {
                            self.pickerView.selectRow(index+1, inComponent: 1, animated: false)
                            self.pickerView(self.pickerView, didSelectRow: index+1, inComponent: 1)
                        }
                        
                    }
                }
            }
            else {
                if let category = Gear6Elements.getCategory(id: Int(self.currentEditedDatatype.categoryID)) {
                    let index = Gear6Elements.categories.index(of: category)
                    self.pickerView.selectRow(index!, inComponent: 0, animated: false)
                }
                else {
                    self.pickerView.selectRow(Int(self.currentEditedDatatype.categoryID) + Gear6Elements.categories.count-1, inComponent: 0, animated: false)
                }
            }
        }
        
        self.setActiveTemplate()
        self.showPicker()
    }
    
    func descPickerPress() {
        guard self.pickerView.isHidden, self.rangeField.isHidden else { return }
        self.pickerState = .EditDesc
        self.textField.text = ""
        if let DataBlock = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
            Gear6Elements.updateDataBlockStruct(lhs: &self.currentEditedDatatype, with: DataBlock)
            switch self.currentEditedDatatype.categoryID {
            case Int32(Gear6ContentType.Text.rawValue):
                if let text = Gear6Elements.getText(id: Int(self.currentEditedDatatype.contentID)) {
                    self.textField.text = text.desc
                }
            case Int32(Gear6ContentType.Value.rawValue), Int32(Gear6ContentType.Range.rawValue):
                if let value = Gear6Elements.getValue(id: Int(self.currentEditedDatatype.contentID)) {
                    self.textField.text = value.desc
                }
            case Int32(Gear6ContentType.Image.rawValue):
                if let image = Gear6Elements.getImage(id: Int(self.currentEditedDatatype.contentID)) {
                    self.textField.text = image.desc
                }
            default: break
            }
        }
        self.setActiveTemplate()
        self.textField.isHidden = false
        self.textField.becomeFirstResponder()
    }
    func rangePickerPress() {
        guard self.pickerView.isHidden, self.textField.isHidden else { return }
        self.pickerState = .Content
        if let DataBlock = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
            Gear6Elements.updateDataBlockStruct(lhs: &self.currentEditedDatatype, with: DataBlock)
            if let value = Gear6Elements.getValue(id: Int(self.currentEditedDatatype.contentID)) {
                if (value.value != 0 && value.maxValue != 0) {
                    self.rangeField.minRangeField.text = "\(value.value)"
                    self.rangeField.maxRangeField.text = "\(value.maxValue)"
                    self.rangeField.minValue = Int(value.value)
                    self.rangeField.maxValue = Int(value.maxValue)
                    self.textField.text = value.desc
                }
            }
        }
        self.setActiveTemplate()
        self.rangeField.isHidden = false
        self.rangeField.becomeFirstResponder()
    }
    
    func quantityPickerPress() {
        guard self.textField.isHidden, self.rangeField.isHidden else { return }
        
        self.pickerState = .Quantity
        if let DataBlock = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
            Gear6Elements.updateDataBlockStruct(lhs: &self.currentEditedDatatype, with: DataBlock)
        }
        
        self.pickerView.reloadComponent(0)
        if (self.currentEditedDatatype.quantityType > 0) {
            let quantityRow = Int(self.currentEditedDatatype.quantityType) - 1
            self.pickerView.selectRow(quantityRow, inComponent: 0, animated: false)
            self.pickerView(self.pickerView, didSelectRow: quantityRow, inComponent: 0)
        }
        else {
            self.pickerView.selectRow(0, inComponent: 0, animated: false)
            self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 0)
        }
        
        self.setActiveTemplate()
        self.showPicker()
    }
    func extTypePickerPress() {
        guard self.textField.isHidden, self.rangeField.isHidden else { return }
        
        self.pickerState = .Expansion
        if let DataBlock = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
            Gear6Elements.updateDataBlockStruct(lhs: &self.currentEditedDatatype, with: DataBlock)
        }
        self.pickerView.reloadComponent(0)
        if self.currentEditedDatatype.expansionType > 0 {
            let extRow = Int(self.currentEditedDatatype.expansionType) - 1
            self.pickerView.selectRow(extRow, inComponent: 0, animated: false)
            self.pickerView(self.pickerView, didSelectRow: extRow, inComponent: 0)
        }
        else {
            self.pickerView.selectRow(0, inComponent: 0, animated: false)
            self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 0)
        }
        
        self.setActiveTemplate()
        self.showPicker()
    }
    
    func hidePicker() {
        self.pickerView.isHidden = true
        self.pickerView.isUserInteractionEnabled = false
        
        self.resetEditedDataBlock()
        self.tableView.reloadData()
        let currentDataCount = (self.currentSelectedGroup > 0) ? Gear6Elements.getExtensionTemplate(catID: self.currentSelectedCategory, groupID: self.currentSelectedGroup)?.data?.count : Gear6Elements.getMainTemplate(catID: self.currentSelectedCategory)?.data?.count
        
        let indexPath = IndexPath(item: currentDataCount!, section: 0)
        if (self.tableView.visibleCells.count < currentDataCount!+1) {
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    override func dismissAction() {
        self.view.endEditing(true)
        
        switch (self.pickerState) {
        case .MainTemplate, .ExtensionTemplate:
            self.hidePicker()
            return
        case .AddData, .EditData, .Quantity, .Expansion:
            guard self.pickerView.isHidden == false else { return }
            if self.pickerState == .AddData {
                self.currentEditedDatatype.id = Gear6Elements.getNewDataBlockID()
            }
        case .EditDesc, .Content:
            guard self.textField.isHidden, self.rangeField.isHidden else { return }
        }
        
        var exists = false
        if (self.pickerState == .AddData || self.pickerState == .EditData || self.pickerState == .EditDesc) {
            if let template = Gear6Elements.getTemplate(id: self.activeTemplate) {
                var dataArray = template.data?.allObjects as! [DataBlock]
                dataArray = dataArray.filter { $0.categoryID == self.currentEditedDatatype.categoryID }
                if dataArray.count > 0 {
                    if self.currentEditedDatatype.categoryID < Gear6ContentType.allCases.count {
                        dataArray = dataArray.filter { $0.contentID == self.currentEditedDatatype.contentID }
                        if dataArray.count > 0 {
                            dataArray = dataArray.filter { $0.desc == self.currentEditedDatatype.desc }
                            if dataArray.count > 0 {
                                exists = true
                            }
                        }
                    }
                    else {
                        dataArray = dataArray.filter { $0.groupID == self.currentEditedDatatype.groupID }
                        if dataArray.count > 0 {
                            dataArray = dataArray.filter { $0.objID == self.currentEditedDatatype.objID }
                            if dataArray.count > 0 {
                                exists = true
                            }
                        }
                    }
                }
            }
        } // else content -> static range
        
        guard !exists else {
            print("Already Exists!")
            self.hidePicker()
            return // TODO: Exists Message
        }
        
        // Generate Content for DataBlock
        // If problems then attach Content to Gear so filter different from object content
        if self.pickerState == .AddData || self.pickerState == .EditData {
            
            // If Edit and catID is default remove previous content
            if self.pickerState == .EditData && self.currentEditedDatatype.contentID > 0 {
                if let prevDataTemp = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id)) {
                    switch prevDataTemp.categoryID {
                    case Int32(Gear6ContentType.Text.rawValue):
                        Gear6Elements.removeText(id: Int(self.currentEditedDatatype.contentID))
                    case Int32(Gear6ContentType.Value.rawValue), Int32(Gear6ContentType.Range.rawValue):
                        Gear6Elements.removeValue(id: Int(self.currentEditedDatatype.contentID))
                    case Int32(Gear6ContentType.Image.rawValue):
                        Gear6Elements.removeImage(id: Int(self.currentEditedDatatype.contentID))
                    default: break
                    }
                }
            }
            
            switch self.currentEditedDatatype.categoryID {
            case Int32(Gear6ContentType.Text.rawValue):
                self.currentEditedDatatype.contentID = Int32(Gear6Elements.getNewTextID())
                let newText = Gear6Elements.getText(id: Int(self.currentEditedDatatype.contentID)) ?? CoreDataManager<Text>.getNewObject()
                newText.id = self.currentEditedDatatype.contentID
                Gear6Elements.texts.append(newText)
                Gear6Elements.texts = Gear6Elements.texts.sorted { $0.id < $1.id }
            case Int32(Gear6ContentType.Value.rawValue), Int32(Gear6ContentType.Range.rawValue):
                self.currentEditedDatatype.contentID = Int32(Gear6Elements.getNewValueID())
                let newValue = Gear6Elements.getValue(id: Int(self.currentEditedDatatype.contentID)) ?? CoreDataManager<Value>.getNewObject()
                newValue.id = self.currentEditedDatatype.contentID
                Gear6Elements.values.append(newValue)
                Gear6Elements.values = Gear6Elements.values.sorted { $0.id < $1.id }
            case Int32(Gear6ContentType.Image.rawValue):
                self.currentEditedDatatype.contentID = Int32(Gear6Elements.getNewImageID())
                let newImage = Gear6Elements.getImage(id: Int(self.currentEditedDatatype.contentID)) ?? CoreDataManager<Image>.getNewObject()
                newImage.id = self.currentEditedDatatype.contentID
                Gear6Elements.images.append(newImage)
                Gear6Elements.images = Gear6Elements.images.sorted { $0.id < $1.id }
            default:
                self.currentEditedDatatype.contentID = 0
            }
        }
        else if self.pickerState == .EditDesc || self.pickerState == .Content {
            switch self.currentEditedDatatype.categoryID {
            case Int32(Gear6ContentType.Text.rawValue):
                if let text = Gear6Elements.getText(id: Int(self.currentEditedDatatype.contentID)) {
                    if (Gear6Elements.texts.filter {$0.desc == self.currentEditedDatatype.desc }).count == 0 {
                        text.desc = self.currentEditedDatatype.desc
                    }
                    else {
                        self.currentEditedDatatype.desc = ""
                    }
                }
            case Int32(Gear6ContentType.Value.rawValue), Int32(Gear6ContentType.Range.rawValue):
                if let value = Gear6Elements.getValue(id: Int(self.currentEditedDatatype.contentID)) {
                    if (Gear6Elements.values.filter {$0.desc == self.currentEditedDatatype.desc }).count == 0 {
                        value.desc = self.currentEditedDatatype.desc
                    }
                    else {
                        self.currentEditedDatatype.desc = ""
                    }
                    if self.pickerState == .Content {
                        value.value = Int32(self.rangeField.minValue!)
                        value.maxValue = Int32(self.rangeField.maxValue!)
                        self.rangeField.minValue = nil
                        self.rangeField.maxValue = nil
                    }
                }
            case Int32(Gear6ContentType.Image.rawValue):
                if let image = Gear6Elements.getImage(id: Int(self.currentEditedDatatype.contentID)) {
                    if (Gear6Elements.images.filter {$0.desc == self.currentEditedDatatype.desc && $0.content == nil }).count == 0 { // TODO: unowned by content filter
                        image.desc = self.currentEditedDatatype.desc
                    } // TODO: else warning message
                    else {
                        self.currentEditedDatatype.desc = ""
                    }
                }
            default: break
            }
        }
        
        // Add EditedObject to Template
        
        if let template = Gear6Elements.getTemplate(id: Int(self.activeTemplate)) {
            
            var foundData = Gear6Elements.getDataBlock(id: Int(self.currentEditedDatatype.id))
            if foundData != nil {
                Gear6Elements.updateDataBlock(lhs: &foundData!, with: self.currentEditedDatatype)
            }
            else {
                var dataToAdd = CoreDataManager<DataBlock>.getNewObject()
                Gear6Elements.updateDataBlock(lhs: &dataToAdd, with: self.currentEditedDatatype)
                template.addToData(dataToAdd)
                Gear6Elements.dataBlocks.append(dataToAdd)
                Gear6Elements.dataBlocks = Gear6Elements.dataBlocks.sorted{$0.id < $1.id}
            }
        }
        self.hidePicker()
    }
}
