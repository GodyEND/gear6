//
//  Gear6BaseViewController.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

class Gear6BaseViewController: UIViewController {
    
    static var motionManager = CMMotionManager()
    let tableView = UITableView()
    let pickerView = UIPickerView()
    let textField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Gear6NavigationController.shared.presentScreen()
        //self.setupSmokeView()
        
        // Start Acellerometer
        Gear6BaseViewController.motionManager.gyroUpdateInterval = 1.0/30.0
        Gear6BaseViewController.motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            if let gyroscope = data {
                let subViews = self.view.subviews.filter{$0 is Gear6PropertyView} as! [Gear6PropertyView]
                for subview in subViews {
                    subview.gyroUpdate(source: CGSize(width: gyroscope.rotationRate.y, height: gyroscope.rotationRate.x))
                }
            }
        }
    }
}
