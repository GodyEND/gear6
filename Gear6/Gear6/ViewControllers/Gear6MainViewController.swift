//
//  Gear6MainViewController.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6MainViewController: Gear6BaseViewController {
    
    static let shared = Gear6MainViewController()
    
    override func setupView() {
        super.setupView()
        
        // Load Persistent Data // TODO: Move to Hub
        Gear6Elements.dataBlocks = CoreDataManager<DataBlock>.fetchData()
        Gear6Elements.templates = CoreDataManager<Template>.fetchData()
        
        Gear6Elements.images = CoreDataManager<Image>.fetchData()
        Gear6Elements.values = CoreDataManager<Value>.fetchData()
        Gear6Elements.texts = CoreDataManager<Text>.fetchData()
        _ = CoreDataManager<Colour>.fetchData()
        Gear6Elements.contents = CoreDataManager<Content>.fetchData()
        
        Gear6Elements.objects = CoreDataManager<Obj>.fetchData()
        Gear6Elements.groupings = CoreDataManager<SubCategory>.fetchData()
        Gear6Elements.categories = CoreDataManager<Category>.fetchData()
        Gear6Elements.categories = Gear6Elements.categories.filter { $0.id >= Gear6ContentType.allCases.count }
        Gear6Elements.defaultCategories = CoreDataManager<Category>.fetchDataWithIDs([1,2,3,4])

        Gear6Elements.gear = CoreDataManager<Gear>.fetchDataWithID(1).first(where: {$0.id == 1}) ?? CoreDataManager<Gear>.getNewObject()
        Gear6Elements.gear?.id = 1 // TODO: get Gear ID From Network Database
        Gear6Elements.gear?.name = "Pokemon X"

        if (Gear6Elements.defaultCategories.count == 0) {
            for i in 1...Gear6ContentType.allCases.count-1 {
                let defaultCategory: Category = CoreDataManager<Category>.getNewObject()
                defaultCategory.name = Gear6ContentType(rawValue: i)?.stringValue
                defaultCategory.id = Int32(i)
                Gear6Elements.defaultCategories.append(defaultCategory)
            }
        }
        
        Gear6Elements.initializeReuseIDsFromDefaults()
        
        switch (Gear6Defaults.gear6Mode) {
        case .Builder:
            // Load Local Btns
            break
        case .User:
            // NOTE: Dummy Data
            var dictionaryArray = [Dictionary<String, Any>]()
            dictionaryArray.append(Dictionary<String, Any>(dictionaryLiteral: ("title", "MAPPER"), ("pageID", Gear6View.Mapper.rawValue), ("buttonImage", "https://pre00.deviantart.net/8f6b/th/pre/i/2016/189/4/e/pokemon_go_globe_icon_by_exovedate-da97h16.png"), ("borderColor", [128, 200, 25, 255])))
            dictionaryArray.append(Dictionary<String, Any>(dictionaryLiteral: ("title", "TEMPLATE"), ("pageID", Gear6View.TemplateBuilder.rawValue), ("buttonImage", "https://pre00.deviantart.net/8f6b/th/pre/i/2016/189/4/e/pokemon_go_globe_icon_by_exovedate-da97h16.png"), ("borderColor", [28, 200, 25, 255])))
            dictionaryArray.append(Dictionary<String, Any>(dictionaryLiteral: ("title", "DATABASE"), ("pageID", Gear6View.Database.rawValue), ("buttonImage", "https://pre00.deviantart.net/8f6b/th/pre/i/2016/189/4/e/pokemon_go_globe_icon_by_exovedate-da97h16.png"), ("borderColor", [128, 200, 125, 255])))
            dictionaryArray.append(Dictionary<String, Any>(dictionaryLiteral: ("title", "ASSISTANT"), ("pageID", Gear6View.AssistantBuilder.rawValue), ("buttonImage", "https://pre00.deviantart.net/8f6b/th/pre/i/2016/189/4/e/pokemon_go_globe_icon_by_exovedate-da97h16.png"), ("borderColor", [28, 200, 125, 255])))
            var counter =  0
            for dictionary in dictionaryArray {
                // TODO: Main Menu Button Position Manager
                var dx = 70
                if (counter % 2 == 0) {
                    dx = 240
                }
                let collectionButton = Gear6MainMenuButton(dictionary, position: CGVector(dx: dx , dy: 140 + counter * 100))
                self.view.addSubview(collectionButton)
                counter += 1
            }
            
//            // Second
//            let collectionButton2 = Gear6MainMenuButton(x: 240, y: 240, title: "COLLECTION", image: #imageLiteral(resourceName: "Gear6BackButton"), borderColor: Gear6Colors.green)
//            collectionButton2.isUserInteractionEnabled = true
//            let tapGestureRecognizer2 = UITapGestureRecognizer(target: Gear6NavigationController.shared, action: #selector(Gear6NavigationController.collectionAction))
//            collectionButton2.addGestureRecognizer(tapGestureRecognizer2)
//            DispatchQueue.main.async {
//                collectionButton2.setNeedsLayout()
//            }
//            self.view.addSubview(collectionButton2)
//
//            // Third
//            let collectionButton3 = Gear6MainMenuButton(x: 70, y: 340, title: "SEARCH", image: UIImage(), borderColor: Gear6Colors.blue)
//            collectionButton3.isUserInteractionEnabled = true
//            //let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToTrain))
//            //collectionButton.addGestureRecognizer(tapGestureRecognizer)
//            DispatchQueue.main.async {
//                collectionButton3.setNeedsLayout()
//            }
//            self.view.addSubview(collectionButton3)
        }
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Save Context
        Gear6Elements.saveToCoreData()
        let mainButtons = (self.view.subviews.filter{$0 is Gear6MainMenuButton} as! [Gear6MainMenuButton]).sorted(by: { $0.frame.origin.y < $1.frame.origin.y })
        var counter:CGFloat = 0.0
        var completionCounter = 0
        for btn in mainButtons {
            btn.showAnimation(TimeInterval(counter * 0.25), completion:{ _ in
                completionCounter += 1
                if completionCounter == 3 {
                    completion(true)
                }
            })
            counter += 1.0
        }
    }
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {

        let mainButtons = (self.view.subviews.filter{$0 is Gear6MainMenuButton} as! [Gear6MainMenuButton]).sorted(by: { $0.frame.origin.y > $1.frame.origin.y })
        var counter:CGFloat = 0.0
        var completionCounter = 0
        for btn in mainButtons {
            btn.clearAnimation(TimeInterval(counter * 0.25), completion: { success in
                if success {
                    completionCounter += 1
                    if completionCounter == 3 {
                        completion!(true)
                    }
                }
            })
            counter += 1.0
        }
    }
    override func standbyAnimation() {
        let mainButtons = (self.view.subviews.filter{$0 is Gear6MainMenuButton} as! [Gear6MainMenuButton]).sorted(by: { $0.frame.origin.y < $1.frame.origin.y })
        for btn in mainButtons {
            btn.standbyAnimation()
        }
    }
}
