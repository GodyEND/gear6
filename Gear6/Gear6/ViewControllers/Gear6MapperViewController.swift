//
//  Gear6MapperViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6MapperViewController: Gear6BaseViewController {
    
    static let shared = Gear6MapperViewController()
    var viewState = MapperViewState.None
    
    var currentCategory: Int = 0
    var currentGroup: Int = 0
    var editID: Int = 0
    
    override func setupView() {
        super.setupView()
        
        self.textField.frame = CGRect(x: 75, y: Screen.FIRST_ITEM_TOP, width: Screen.TEXTFIELD_WIDTH, height: Screen.TEXTFIELD_HEIGHT)
        self.textField.placeholder = Placeholder.ADD_CAT
        self.textField.isHidden = false
        
        self.tableView.register(Gear6MapperViewCell.self, forCellReuseIdentifier: Cell.MapperView.rawValue)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = false
        self.tableView.reloadData()
        
        self.view.bringSubview(toFront: self.pickerView)
        // Load published map from webservice into core data
        // Load map from core data
        
        // Leaving Page to get to main will ask for saving map changes if changes were detected.
        // If true save to Core Data
        // TODO: When uploading and update i.d is below current server version then show user list of changes
        // and ask if they want to still publish their version?
        // TODO: clicking on group allows user to asign colour / pattern // delete group // edit name /
    }
    
    func update() {
        switch (self.viewState) {
        case .Category:
            self.textField.text = ""
            self.textField.placeholder = Placeholder.ADD_CAT
            self.tableView.reloadData()
        case .Grouping:
            self.textField.text = ""
            self.textField.placeholder = Placeholder.ADD_GROUP
            self.tableView.reloadData()
        case .EditCategory:
            if let category = Gear6Elements.getCategory(id: self.editID) {
                self.textField.text = category.name
                self.textField.placeholder = Placeholder.EDIT_CAT
                self.textField.becomeFirstResponder()
            }
        case .EditGroup:
            if let group = Gear6Elements.getGroup(catID: self.currentCategory, groupID: self.editID) {
                self.textField.text = group.name
                self.textField.placeholder = Placeholder.EDIT_GROUP
                self.textField.becomeFirstResponder()
            }
        default: break
        }
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}

