//
//  Gear6CollectionViewController.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit

class Gear6CollectionViewController: Gear6BaseViewController {
    
    static let shared = Gear6CollectionViewController()
    
    override func setupView() {
        super.setupView()
        // Filter Collectable Categories
        // For Result get userCollectableObjects // TODO: Stored on phone only? or to user cloud acc
        // Get User Defaults for Collection View // Wether or not missing objects are displayed
        
        // Setup CollectionView Style
        // Setup Scroll Indicator Style // Gear6 Scroller
        // Using Gear6 Cells // if section even then cell background coler uses secondary color
        
        // Get list of user object types Object class
        // Get info view of currently active user object type
        
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Void)? = nil) {
        
    }
}



