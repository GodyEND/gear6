//
//  Gear6LibraryViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6LibraryViewController: Gear6BaseViewController {
    
    static let shared = Gear6LibraryViewController()
    
    override func setupView() {
        super.setupView()
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Void)? = nil) {
        completion!(true)
    }
}
