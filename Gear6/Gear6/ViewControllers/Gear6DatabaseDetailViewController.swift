//
//  Gear6DatabaseDetailViewController.swift
//  Gear6
//
//  Created by Gody on 09/03/2019.
//  Copyright © 2019 Gear6. All rights reserved.
//

import UIKit

class Gear6DatabaseDetailViewController : Gear6BaseViewController {
    
    static var shared = Gear6DatabaseDetailViewController()
    
    var selectedObject: Obj?
    var possibleData: [DataBlock] = [] // tag = 0 for possible
    var existingData: [DataBlock] = [] // tag = 1 for existing
    var existingDataTableView = UITableView()
    
    
    override func setupView() {
        // hide back buton present modal back
        // First table view has possible datablock selection
        // second table view displays existing data
        self.tableView.tag = 0
        self.existingDataTableView.tag = 1
        // TODO register cell
    }
    
    func update() {
        
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        self.title = selectedObject?.name
        self.existingData = selectedObject?.data?.allObjects as! [DataBlock]
    }
    override func standbyAnimation() {
        // Do nothing
    }
    
    override func clearScreen(completion: ((Bool) -> Void)?) {
        Gear6NavigationController.shared.popViewController(animated: true)
        // clear animation
        // show default back button
    }
    
    // MARK: Swipe Gestures
    override func swipeAction(_ sender: UISwipeGestureRecognizer) {
        guard self.textField.isHidden, self.pickerView.isHidden else { return }
        
        if sender.direction == .left {
            self.pickerView.reloadAllComponents()
            if self.selectedObject != Gear6Elements.objects[0] {
                // Get current Cat index
                let currentIndex = Gear6Elements.objects.firstIndex(of: self.selectedObject!)!
                let index = Gear6Elements.objects.startIndex.distance(to: currentIndex) - 1
                self.selectedObject = Gear6Elements.objects[index]
                self.pickerView.selectRow(index, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: index, inComponent: 0)
            }
            else {
                self.selectedObject = Gear6Elements.objects.last!
                self.pickerView.selectRow((Gear6Elements.objects.count-1), inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: (Gear6Elements.objects.count-1), inComponent: 0)
            }
            //self.selectedObject = Gear6Elements.objects.first!
            self.update()
        }
        else if sender.direction == .right {
            self.pickerView.reloadAllComponents()
            if self.selectedObject != Gear6Elements.objects.last! {
                // Get current Cat index
                let currentIndex = Gear6Elements.objects.firstIndex(of: self.selectedObject!)!
                let index = Gear6Elements.objects.startIndex.distance(to: currentIndex) + 1
                self.selectedObject = Gear6Elements.objects[index]
                self.pickerView.selectRow(index, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: index, inComponent: 0)
            }
            else {
                self.selectedObject = Gear6Elements.objects.first!
                self.pickerView.selectRow(0, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 0)
            }
            //self.selectedObject = 0
            self.update()
        }
    }
}

extension Gear6DatabaseDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0: return 1
        default: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        switch tableView.tag {
        case 0: break
        default: break
        }
        return cell
    }
}


extension Gear6DatabaseDetailViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // nothing
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ""
    }
}
