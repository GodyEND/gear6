//
//  Gear6BuilderMenuViewController.swift
//  Gear6
//
//  Created by Gody on 27/05/2018.
//  Copyright © 2018 Gear6. All rights reserved.
//

import UIKit

class Gear6BuilderMenuViewController: Gear6BaseViewController {
    
    static let shared = Gear6BuilderMenuViewController()
    
    override func setupView() {
        super.setupView()
        // check existing projects
        // create new project only if non existing
        // existing will be added if user is part of a team and the team has projects assigned to them
        // edit will switch app state to builder and will take user to main
    }
    
    override func presentScreen(completion: @escaping (Bool) -> Void) {
        // Animate Gear6 Views
        
    }
    
    override func clearScreen(completion: ((Bool) -> Swift.Void)? = nil) {
        completion!(true)
    }
}
