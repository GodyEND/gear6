//
//  CoreDataManager.swift
//  Gear6
//
//  Created by Gody on 29/10/2017.
//  Copyright © 2017 Gear6. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager<T : NSManagedObject> {
    
    class func saveData() {
        do {
            try CoreData.sharedContext().save()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    class func delete(_ entity: T) {
        CoreData.sharedContext().delete(entity)
    }
    
    class func deleteAllData() {
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try CoreData.sharedContext().execute(deleteRequest)
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    class func deleteDataWithID(_ id: Int) {
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        let predicate = NSPredicate(format: "id == %@", NSNumber(value: id))
        fetchRequest.predicate = predicate
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        do {
            try CoreData.sharedContext().execute(deleteRequest)
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    class func fetchData() -> [T] {
        var entityArray = [T]()
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        let descriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [descriptor]
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            entityArray = try CoreData.sharedContext().fetch(fetchRequest)
        }
        catch {
            print(error.localizedDescription)
        }
        return entityArray
    }
    
    class func fetchDataWithID(_ id: Int) -> [T] {
        var entityArray = [T]()
        let fetchRequest: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        let predicate = NSPredicate(format: "id == %@", NSNumber(value: id))
        let descriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [descriptor]
        fetchRequest.predicate = predicate
        fetchRequest.returnsObjectsAsFaults = false
        do {
            entityArray = try CoreData.sharedContext().fetch(fetchRequest)
        }
        catch {
            print(error.localizedDescription)
        }
        return entityArray
    }
    
    class func fetchDataWithIDs(_ ids: [Int]) -> [T] {
        var entityArray = [T]()
        for id in ids {
            let fetchRequest: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
            let predicate = NSPredicate(format: "id == %@", NSNumber(value: id))
            let descriptor = NSSortDescriptor(key: "id", ascending: true)
            fetchRequest.sortDescriptors = [descriptor]
            fetchRequest.predicate = predicate
            fetchRequest.returnsObjectsAsFaults = false
            do {
                let entity = try CoreData.sharedContext().fetch(fetchRequest)
                entityArray.append(contentsOf: entity)
            }
            catch {
                print(error.localizedDescription)
            }
        }
        return entityArray
    }
    
    class func fetchDataWithName(_ searchString: String) -> [T] {
        var entityArray = [T]()
        let fetchRequest: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        let predicate = NSPredicate(format: "name contains[c] %@", searchString)
        let descriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [descriptor]
        fetchRequest.predicate = predicate
        fetchRequest.returnsObjectsAsFaults = false
        do {
            entityArray = try CoreData.sharedContext().fetch(fetchRequest)
        }
        catch {
            print(error.localizedDescription)
        }
        return entityArray
    }
    
    class func getNewObject() -> T {
        return NSEntityDescription.insertNewObject(forEntityName: String(describing: T.self), into: CoreData.sharedContext()) as! T
    }
}


// MARK: Data Parsing

extension CoreDataManager {
    
    class func parseJSON(jsonArray: [[String:Any]]) -> [T] {
        var entityArray = [T]()
        for jsonObject in jsonArray {
            let model = NSEntityDescription.insertNewObject(forEntityName: String(describing: T.self), into: CoreData.sharedContext()) as! T
            model.setValuesForKeys(jsonObject)
            entityArray.append(model)
        }
        CoreDataManager<T>.saveData()
        return entityArray
    }
}
